from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, QueryDict
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta

# from user.models import User
from administration.models import User, Call, Price

@user_passes_test(lambda u: u.is_superuser)
@csrf_exempt
def user_information(request):
    if request.method == 'GET':
        html = ''
        count = User.objects.using('user_info').all().count();
        digit = 0
        while count > 0:
            digit += 1
            count //= 10
        for user in User.objects.using('user_info').all():
            html += '<tr>'
            html += '   <td>(' + user.phone_num[:3] + ')' + user.phone_num[3:6] + '-' + user.phone_num[6:] + '</td>'
            html += '   <td>' + str(user.ip) + '</td>'
            html += '   <td>' + str(user.first_name) + '</td>'
            html += '   <td>' + str(user.last_name) + '</td>'
            html += '   <td>' + str(user.email) + '</td>'
            html += '   <td><div id="user_account_status_' + user.phone_num + '">' + ('ACTIVE' if user.active_status == b'\x01' else 'INACTIVE') + '</div></td>'
            html += '   <td>' + '<button class="button dark outline" id="change_user_account_status_' + user.phone_num + '" onclick="onClickChangeUserAccountStatus(' + user.phone_num + ')">' + ('DISABLE' if user.active_status == b'\x01' else 'ENABLE') + '</button>' + '</td>'
            html += '   <td>' + '<button class="button dark outline" id="delete_user_account_"' + user.phone_num + '" onclick="onClickDeleteUserAccount(' + user.phone_num + ')">DELETE</button>' + '</td>'
            html += '</tr>'
        return render(request, 'admin/user_information.html', {'user_information': html})
    elif request.method == 'POST':
        user_phone_number = request.POST.get('userPhoneNumber')
        user_active_status = request.POST.get('userActiveStatus')
        if User.objects.using('user_info').all().filter(phone_num=user_phone_number).exists():
            user = User.objects.using('user_info').all().get(phone_num=user_phone_number)
            user.active_status = (True if user_active_status == 'ACTIVE' else False)
            user.save()
            return JsonResponse({'userPhoneNumber': user_phone_number, 'userActiveStatus': user_active_status})
        return HttpResponse(status=400)
    elif request.method == 'DELETE':
        user_phone_number = QueryDict(request.body).get('userPhoneNumber')
        if User.objects.using('user_info').all().filter(phone_num=user_phone_number).exists():
            User.objects.using('user_info').get(phone_num=user_phone_number).delete()
            return HttpResponse(status=200)
        return HttpResponse(status=400)

@user_passes_test(lambda u: u.is_superuser)
def billing_information(request):
    if request.method == 'GET':
        html = ''
        phone_number_prefix = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        for prefix in phone_number_prefix:
            users = User.objects.using('user_info').all().filter(phone_num__startswith=prefix).order_by('phone_num')
            if users.count() > 0:
                html += '<li>'
                html += '   <input data-role="checkbox" data-caption="' + prefix + 'xx" title="">'
                html += '   <ul>'
                for user in users:
                    html += '       <li><input data-role="checkbox" data-caption="(' + user.phone_num[:3] + ')' + user.phone_num[3:6] + '-' + user.phone_num[6:] + '" title=""></li>'
                html += '   </ul>'
                html += '</li>'
        return render(request, 'admin/billing_information.html', {'phone_number_list': html})
    elif request.method == 'POST':
        start_date = datetime.strptime(request.POST.get('startDate'), '%a %b %d %Y')
        end_date = datetime.strptime(request.POST.get('endDate'), '%a %b %d %Y') + timedelta(days=1)
        html = ''
        html += '<table class="table striped table-border mt-4"\
                    data-role="table"\
                    data-cls-table-top="row flex-nowrap"\
                    data-cls-search="cell-md-8"\
                    data-cls-rows-count="cell-md-4"\
                    data-rows="10"\
                    data-rows-steps="10, 20, 30, 40, 50, 60, 70, 80, 90, 100">'
        html += '   <thead> '
        html += '       <tr>'
        html += '           <th class="sortable-column sort-asc">Call ID</th>'
        html += '           <th class="sortable-column">Caller Phone Number</th>'
        html += '           <th class="sortable-column">Caller IP</th> '
        html += '           <th class="sortable-column">Callee Phone Number</th>'
        html += '           <th class="sortable-column">Callee IP</th> '
        html += '           <th class="sortable-column">Accepted Time</th>'
        html += '           <th class="sortable-column">End Time</th> '
        html += '           <th class="sortable-column">Talktime(sec)</th>'
        html += '       </tr> '
        html += '   </thead>'
        html += '   <tbody>'
        if Call.objects.using('user_info').all().filter(end_yn='Y').filter(accepted_time__gte=start_date).filter(end_time__lt=end_date).exists():
            for call in Call.objects.using('user_info').all().filter(end_yn='Y').filter(accepted_time__gte=start_date).filter(end_time__lt=end_date):
                html += '       <tr>'
                html += '           <td>' + str(call.call_id).zfill(10) + '</td>'
                html += '           <td>(' + call.from_phone_num[:3] + ')' + call.from_phone_num[3:6] + '-' + call.from_phone_num[6:] + '</td>'
                html += '           <td>' + call.from_ip + '</td>'
                html += '           <td>(' + call.to_phone_num[:3] + ')' + call.to_phone_num[3:6] + '-' + call.to_phone_num[6:] + '</td>'
                html += '           <td>' + call.to_ip + '</td>'
                html += '           <td>' + str(call.accepted_time) + '</td>'
                html += '           <td>' + str(call.end_time) + '</td>'
                html += '           <td>' + str(call.talktime) + '</td>'
                html += '       </tr>'
        html += '   </tbody>'
        html += '   <tfoot>'
        html += '       <tr>'
        html += '           <th>Call ID</th>'
        html += '           <th>Caller Phone Number</th>'
        html += '           <th>Caller IP</th>'
        html += '           <th>Callee Phone Number</th>'
        html += '           <th>Callee IP</th>'
        html += '           <th>Accepted Time</th>'
        html += '           <th>End Time</th>'
        html += '           <th>Talktime(sec)</th>'
        html += '       </tr>'
        html += '   </tfoot>'
        html += '</table>'
        return JsonResponse({'billingInformation': html})

@user_passes_test(lambda u: u.is_superuser)
def price_policy(request):
    if request.method == 'GET':
        html = ''
        for price in Price.objects.using('user_info').all():
            html += '<tr>'
            if price.use_yn == 1:
                html += '   <td><b>' + str(price.apply_time) + '</b></td>'
                html += '   <td><b>' + str(price.call_price) + '</b></td>'
                html += '   <td><b>' + str(price.text_price) + '</b></td>'
            else:
                html += '   <td><p style="color:lightGray">' + str(price.apply_time) + '</p></td>'
                html += '   <td><p style="color:lightGray">' + str(price.call_price) + '</p></td>'
                html += '   <td><p style="color:lightGray">' + str(price.text_price) + '</p></td>'
            html += "</tr>"
        return render(request, 'admin/price_policy.html', {'price_policy_history': html})
    elif request.method == 'POST':
        applyTime = datetime.strptime(request.POST.get('applyDate') + ' ' + request.POST.get('applyTime'), '%Y-%m-%d %H:%M:%S')
        callPrice = request.POST.get('callPrice')
        textPrice = request.POST.get('textPrice')
        if Price.objects.using('user_info').all().filter(use_yn=1).exists():
            price = Price.objects.using('user_info').all().get(use_yn=1)
            price.use_yn = 0
            price.save()
        Price.objects.using('user_info').create(apply_time=applyTime, call_price=callPrice, text_price=textPrice, use_yn=1)
        return HttpResponse(status=200)
