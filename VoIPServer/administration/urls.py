from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    re_path(
        r'^password/$',
        auth_views.password_change,
        {
            'template_name': 'admin/change_password.html'
        },
        name='password_change',
    ),
    re_path(
        r'^password/done/$',
        auth_views.password_change_done,
        {
            'template_name': 'admin/change_password_done.html'
        },
        name='password_change_done'
    ),
    re_path(
        r'^user_information/$',
        views.user_information
    ),
    re_path(
        r'^billing_information/$',
        views.billing_information
    ),
    re_path(
        r'^price_policy/$',
        views.price_policy
    ),
    path(
        'logout',
        auth_views.logout,
        {
            'next_page': '/',
        },
        name='admin_logout'
    ),
]