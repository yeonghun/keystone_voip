from django.db import models

class User(models.Model):
    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100, default='')
    email = models.CharField(max_length=256, default='')
    password = models.CharField(max_length=50, default='')
    phone_num = models.CharField(primary_key=True, max_length=10, default='')
    ip = models.CharField(max_length=15, default='')
    active_status = models.BooleanField(default=True)

    class Meta:
        db_table = 'user'
        # db_table = 'user_userinfo'

class Call(models.Model):
    call_id = models.BigIntegerField(primary_key=True, default=0)
    received_yn = models.CharField(max_length=255, default='')
    from_ip = models.CharField(max_length=255, default='')
    from_phone_num = models.CharField(max_length=255, default='')
    to_ip = models.CharField(max_length=255, default='')
    to_phone_num = models.CharField(max_length=255, default='')
    end_yn = models.CharField(max_length=255, default='')
    create_time = models.DateTimeField(null=True)
    last_update_time = models.DateTimeField(null=True)
    accepted_time = models.DateTimeField(null=True)
    date = models.CharField(max_length=255, default='')
    end_time = models.DateTimeField(null=True)
    talktime = models.IntegerField(default=0)
    timeout_yn = models.CharField(max_length=255, default='')

    class Meta:
        db_table = 'call1'

class Price(models.Model):
    apply_time = models.DateTimeField(primary_key=True)
    call_price = models.FloatField(default=0)
    text_price  = models.FloatField(default=0)
    use_yn = models.IntegerField(default=0)

    class Meta:
        db_table = 'billconfig'
