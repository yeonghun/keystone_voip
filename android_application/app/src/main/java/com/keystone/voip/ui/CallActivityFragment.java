package com.keystone.voip.ui;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.keystone.voip.R;
import com.keystone.voip.service.CallService;
import com.keystone.voip.state.CallState;

/**
 * A placeholder fragment containing a simple view.
 */
public class CallActivityFragment extends BaseFragment implements View.OnClickListener  {
    private final String TAG = "CallActivityFragment";

    private TextView mCallStateTv;
    private TextView mContactNameTv;
    private TextView mPhoneNumberTv;

    private ImageButton mRingerButton;
    private ImageButton mVibrateButton;

    private ImageButton mSpeakerButton;
    private ImageButton mMicButton;
    private ImageButton mBluetoothButton;

    private ImageButton mAcceptButton;
    private ImageButton mEndButton;
    private ImageButton mRejectButton;
    private PowerManager.WakeLock proximityWakeLock;
    private MediaPlayer mRingTonePlayer;
    private AudioManager mAudioManager;
    private Vibrator mVibrator;
    private int mPreviousAudioManagerMode = 0;
    private final long[] mVibratorPattern = {0, 200, 800};

    public CallActivityFragment() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent callServiceStopIntent = new Intent(this.getContext(), CallService.class);
        getActivity().stopService(callServiceStopIntent);
        CallState.getInstance().clearAllState();
        disableProximityWakeLock();
        endRinger();
        stopVibrate();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragView = inflater.inflate(R.layout.fragment_call, container, false);

        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }

        mCallStateTv = (TextView) fragView.findViewById(R.id.callstate);
        mCallStateTv.setOnClickListener(this);

        mContactNameTv = (TextView) fragView.findViewById(R.id.contactname);
        mContactNameTv.setOnClickListener(this);

        mPhoneNumberTv = (TextView) fragView.findViewById(R.id.phonenumber);
        mPhoneNumberTv.setOnClickListener(this);

        mRingerButton = fragView.findViewById(R.id.ringer);
        mRingerButton.setOnClickListener(this);
        mRingerButton.setSelected(true);

        mVibrateButton = fragView.findViewById(R.id.vibrate);
        mVibrateButton.setOnClickListener(this);
        mVibrateButton.setSelected(true);

        mSpeakerButton = (ImageButton) fragView.findViewById(R.id.speaker);
        mSpeakerButton.setOnClickListener(this);
        mSpeakerButton.setSelected(false);

        mMicButton = (ImageButton) fragView.findViewById(R.id.mic);
        mMicButton.setOnClickListener(this);
        mMicButton.setSelected(true);

        mBluetoothButton = (ImageButton) fragView.findViewById(R.id.bluetooth);
        mBluetoothButton.setOnClickListener(this);
        mBluetoothButton.setSelected(false);

        mAcceptButton = (ImageButton) fragView.findViewById(R.id.acceptcallBtn);
        mAcceptButton.setOnClickListener(this);

        mEndButton = (ImageButton) fragView.findViewById(R.id.endcallbtn);
        mEndButton.setOnClickListener(this);

        mRejectButton = (ImageButton) fragView.findViewById(R.id.rejectcallbtn);
        mRejectButton.setOnClickListener(this);

        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        return fragView;
    }

    @Override
    public void onClick(View v) {

        if (v == null) {
            Log.e(TAG, "Onclick view = null");
            return;
        }

        if (mRingerButton == v) {
            mRingerButton.setSelected(!mRingerButton.isSelected());
            if (mRingerButton.isSelected()) startRinger();
            else endRinger();
            return;
        }
        if (mVibrateButton == v) {
            mVibrateButton.setSelected(!mVibrateButton.isSelected());
            if (mVibrateButton.isSelected()) startVibrate();
            else stopVibrate();
            return;
        }

        if (mSpeakerButton == v) {
            mSpeakerButton.setSelected(!mSpeakerButton.isSelected());
            changeAudioOut();
            return;
        }
        if (mMicButton == v) {
            mMicButton.setSelected(!mMicButton.isSelected());
            toggleMicOut(mMicButton.isSelected());
            return;
        }
        if (mBluetoothButton == v) {
            mBluetoothButton.setSelected(!mBluetoothButton.isSelected());
            changeAudioOut();
            return;
        }

        if (mAcceptButton == v) {

        //   if (CallState.getInstance().getCallState() == CallState.Call_State.INCOMING) {
               Intent callServiceIntent = new Intent(this.getContext(), CallService.class);
               callServiceIntent.putExtra("action", Utils.ACTION.ACT_ACCEPT_CALL);
               callServiceIntent.putExtra(Utils.CALL_ID, CallState.getInstance().getCallID());
               this.getActivity().startService(callServiceIntent);
               return;
          // }
        }
        if (mEndButton == v) {
           //ToDO end call
            Intent callServiceIntent = new Intent(this.getContext(), CallService.class);
            callServiceIntent.putExtra("action", Utils.ACTION.ACT_END_CALL);
            callServiceIntent.putExtra(Utils.CALL_ID, CallState.getInstance().getCallID());
            this.getActivity().startService(callServiceIntent);
            return;
        }
        if (mRejectButton == v) {
            //ToDO reject call. Don't misunderstand that caller or callee can reject call
            Intent callServiceIntent = new Intent(this.getContext(), CallService.class);
            callServiceIntent.putExtra("action", Utils.ACTION.ACT_REJECT_CALL);
            callServiceIntent.putExtra(Utils.CALL_ID, CallState.getInstance().getCallID());
            this.getActivity().startService(callServiceIntent);
            return;
        }
        return;
    }

    private void changeAudioOut() {

        Log.d(TAG, "changeAudioOut - BT(" + mBluetoothButton.isSelected() + "), SP(" + mSpeakerButton.isSelected() + ")");
        int mode = 0;
        if (mBluetoothButton.isSelected()) {
            mode = Utils.AUDIO_OUT_MODE.BLUETOOTH;
        } else if (mSpeakerButton.isSelected()) {
            mode = Utils.AUDIO_OUT_MODE.SPEAKER;
        } else {
            mode = Utils.AUDIO_OUT_MODE.EARPIECE;
        }

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this.getContext());
        Intent localIntent = new Intent(Utils.ACTION.toString(Utils.ACTION.ACT_CHANGE_AUDIO_MODE));
        localIntent.putExtra("OUT", mode);
        localBroadcastManager.sendBroadcast(localIntent);
    }

    private void toggleMicOut(boolean enable) {
        Log.d(TAG, "toggleMicOut - " + enable);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this.getContext());
        Intent localIntent = new Intent(Utils.ACTION.toString(Utils.ACTION.ACT_CHANGE_AUDIO_MODE));
        localIntent.putExtra("MIC", enable);
        localBroadcastManager.sendBroadcast(localIntent);
    }

    public void updateGUI() {
        Log.d(TAG, "updateGUI - " + CallState.getInstance().getCallState());
        //TODO update UI accordingly.
        if (CallState.getInstance().getCallState() == CallState.Call_State.CONNECTING) {
            enableProximityWakeLock();
            mCallStateTv.setText("Connecting...");
            mContactNameTv.setText(getArguments().getString(Utils.CONTACT_NAME, ""));
            mPhoneNumberTv.setText(CallState.getInstance().getRemotePhoneNumber());

            mAcceptButton.setVisibility(View.INVISIBLE);
            mRejectButton.setVisibility(View.INVISIBLE);
            mEndButton.setVisibility(View.VISIBLE);
        }
        else if (CallState.getInstance().getCallState() == CallState.Call_State.CALLING) {
            disableProximityWakeLock();
            mCallStateTv.setText("Ringing...");
            mPhoneNumberTv.setText(CallState.getInstance().getRemotePhoneNumber());

            mRingerButton.setVisibility(View.VISIBLE);
            mVibrateButton.setVisibility(View.INVISIBLE);

            mAcceptButton.setVisibility(View.INVISIBLE);
            mRejectButton.setVisibility(View.VISIBLE);
            mEndButton.setVisibility(View.INVISIBLE);


        } else if (CallState.getInstance().getCallState() == CallState.Call_State.CONNECTION_FAIL)  {
            mCallStateTv.setText("Connection failed");
            mPhoneNumberTv.setText(CallState.getInstance().getRemotePhoneNumber());

            mRingerButton.setVisibility(View.INVISIBLE);
            mVibrateButton.setVisibility(View.INVISIBLE);

            mAcceptButton.setVisibility(View.INVISIBLE);
            mRejectButton.setVisibility(View.INVISIBLE);
            mEndButton.setVisibility(View.INVISIBLE);

//            CallState.getInstance().setCallState(CallState.Call_State.IDLE);
            CallState.getInstance().clearAllState();
        }else if (CallState.getInstance().getCallState() == CallState.Call_State.INCOMING) {
            enableProximityWakeLock();
            mRingerButton.setVisibility(View.VISIBLE);
            mVibrateButton.setVisibility(View.VISIBLE);

            if (mRingerButton.isSelected()) startRinger();
            if (mVibrateButton.isSelected()) startVibrate();
            mCallStateTv.setText("Incoming Call...");
            mPhoneNumberTv.setText(CallState.getInstance().getRemotePhoneNumber());
            mAcceptButton.setVisibility(View.VISIBLE);
            mRejectButton.setVisibility(View.VISIBLE);
            mEndButton.setVisibility(View.INVISIBLE);
            String contactName = null;
            DBHelper db = new DBHelper(getContext());
            contactName = db.getNameFromNumber(CallState.getInstance().getRemotePhoneNumber());
            if (contactName.isEmpty()) {
                contactName = "Unknown Contact";
            }
            mContactNameTv.setText(contactName);

        } else if (CallState.getInstance().getCallState() == CallState.Call_State.INCALL) {
            enableProximityWakeLock();
            mRingerButton.setVisibility(View.INVISIBLE);
            mVibrateButton.setVisibility(View.INVISIBLE);
            endRinger();
            stopVibrate();
            mCallStateTv.setText("In Call...");
            mPhoneNumberTv.setText(CallState.getInstance().getRemotePhoneNumber());

            mAcceptButton.setVisibility(View.INVISIBLE);
            mRejectButton.setVisibility(View.INVISIBLE);
            mEndButton.setVisibility(View.VISIBLE);

        } else if (CallState.getInstance().getCallState() == CallState.Call_State.CALLREJECTED) {
            disableProximityWakeLock();
            mRingerButton.setVisibility(View.INVISIBLE);
            mVibrateButton.setVisibility(View.INVISIBLE);
            endRinger();
            stopVibrate();
            mCallStateTv.setText("Call Rejected");
            mPhoneNumberTv.setText(CallState.getInstance().getRemotePhoneNumber());

            mAcceptButton.setVisibility(View.INVISIBLE);
            mRejectButton.setVisibility(View.INVISIBLE);
            mEndButton.setVisibility(View.INVISIBLE);

//            CallState.getInstance().setCallState(CallState.Call_State.IDLE);
            CallState.getInstance().clearAllState();
        }  else if (CallState.getInstance().getCallState() == CallState.Call_State.CALLEND) {
            disableProximityWakeLock();
            mRingerButton.setVisibility(View.INVISIBLE);
            mVibrateButton.setVisibility(View.INVISIBLE);
            endRinger();
            stopVibrate();
            mCallStateTv.setText("Call Ended");
            mPhoneNumberTv.setText(CallState.getInstance().getRemotePhoneNumber());

            mAcceptButton.setVisibility(View.INVISIBLE);
            mRejectButton.setVisibility(View.INVISIBLE);
            mEndButton.setVisibility(View.INVISIBLE);

//            CallState.getInstance().setCallState(CallState.Call_State.IDLE);
            CallState.getInstance().clearAllState();
        }

    }

    public boolean enableProximityWakeLock() {
        if (proximityWakeLock != null) {
            return true;
        }
        if (getActivity() != null) {
            PowerManager powerManager = (PowerManager)
                    this.getActivity().getApplicationContext().getSystemService(Context.POWER_SERVICE);
            if (powerManager != null) {
                proximityWakeLock = powerManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "keystone:proximityWaklock");
                proximityWakeLock.acquire();
            }
        }
        return false;
    }

    public void disableProximityWakeLock() {
        if (proximityWakeLock != null) {
            proximityWakeLock.release();
            proximityWakeLock = null;
        }
    }

    private void startRinger() {
        Log.d(TAG, "startRinger, bluetooth(" + mBluetoothButton.isSelected() + "), speaker(" + mSpeakerButton.isSelected() + ")");
        if (mRingTonePlayer == null) {
            if (mAudioManager == null) {
                mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            }
            mPreviousAudioManagerMode = mAudioManager.getMode();
            mAudioManager.setMode(AudioManager.MODE_NORMAL);

            mRingTonePlayer = MediaPlayer.create(getContext(), R.raw.ring);
            mRingTonePlayer.setLooping(true);
            mRingTonePlayer.start();

        }
    }
    private void endRinger() {
        Log.d(TAG, "endRinger");
        if (mRingTonePlayer != null) {
            mRingTonePlayer.stop();
            mRingTonePlayer.release();
            mRingTonePlayer = null;
            mAudioManager.setMode(mPreviousAudioManagerMode);
        }
    }

    private void startVibrate() {
        if (mVibrator == null) {
            mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        }
        mVibrator.vibrate(mVibratorPattern, 0);
    }

    private void stopVibrate() {
        if (mVibrator != null) {
            mVibrator.cancel();
        }
    }

}
