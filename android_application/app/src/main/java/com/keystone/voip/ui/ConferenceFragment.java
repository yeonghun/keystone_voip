package com.keystone.voip.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.keystone.voip.R;
import com.keystone.voip.service.ConferenceCallService;
import com.keystone.voip.state.ConferenceCallState;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ConferenceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConferenceFragment extends BaseFragment implements View.OnClickListener, SettingsActivity.newDateTimeListner , Observer {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private final String TAG = "ConferenceFragment";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private boolean mIsTimeSet = false;
    private boolean mIsDateSet = false;

    private Button mDateBtn = null;
    private Button mTimeBtn = null;
    private Button mSendInvitiesBtn = null;
    private EditText mEditTxtParticipant1 = null;
    private EditText mEditTxtParticipant2 = null;
    private EditText mEditTxtParticipant3 = null;
    private EditText mEditTxtParticipant4 = null;

    private DatePickerFragment mDatePickerFrag;
    private TimePickerFragment mTimePickerFrag;
    private Calendar mConfCallCal = Calendar.getInstance();

    public ConferenceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConferenceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConferenceFragment newInstance(String param1, String param2) {
        ConferenceFragment fragment = new ConferenceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mDatePickerFrag = new DatePickerFragment();
        mDatePickerFrag.setListner((SettingsActivity.newDateTimeListner)this);

        mTimePickerFrag = new TimePickerFragment();
        mTimePickerFrag.setListner((SettingsActivity.newDateTimeListner)this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.fragment_conference, container, false);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
         mDateBtn = (Button) fragView.findViewById(R.id.dateBtn);
         mDateBtn.setOnClickListener(this);

         mTimeBtn = (Button) fragView.findViewById(R.id.timebtn);
         mTimeBtn.setOnClickListener(this);

         mSendInvitiesBtn = (Button) fragView.findViewById(R.id.sendInvitebtn);
         mSendInvitiesBtn.setOnClickListener(this);

         mEditTxtParticipant1 = (EditText) fragView.findViewById(R.id.editTxt_parti1);
         mEditTxtParticipant2 = (EditText) fragView.findViewById(R.id.editTxt_parti2);
         mEditTxtParticipant3 = (EditText) fragView.findViewById(R.id.editTxt_parti3);
         mEditTxtParticipant4 = (EditText) fragView.findViewById(R.id.editTxt_parti4);
        return fragView;
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            return;
        }
        if (mDateBtn == v) {
            mDatePickerFrag.setCalender(mConfCallCal);
            mDatePickerFrag.show(getFragmentManager(),"datepicker");
        } else if (mTimeBtn == v) {
            mTimePickerFrag.setCalender(mConfCallCal);
            mTimePickerFrag.show(getFragmentManager(),"Timepicker");
        } else if (mSendInvitiesBtn == v) {
            if (canSendInvite() == false) {
                return;
            }
            if (isVaildNumbers() == false) {
                return;
            }
            ArrayList<String> numbers = new ArrayList<>();
            if (mEditTxtParticipant1.getText().toString().isEmpty() == false) {
                numbers.add(mEditTxtParticipant1.getText().toString());
            }
            if (mEditTxtParticipant2.getText().toString().isEmpty() == false) {
                numbers.add(mEditTxtParticipant2.getText().toString());
            }
            if (mEditTxtParticipant3.getText().toString().isEmpty() == false) {
                numbers.add(mEditTxtParticipant3.getText().toString());
            }
            if (mEditTxtParticipant4.getText().toString().isEmpty() == false) {
                numbers.add(mEditTxtParticipant4.getText().toString());
            }

            Date d = mConfCallCal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            String timeString = sdf.format(d);
            Intent i = new Intent(getContext(), ConferenceCallService.class);
            i.putExtra("action", Utils.ACTION.ACT_CC_CREATE);
            i.putExtra(Utils.CC_DATE_TIME, timeString);
            i.putStringArrayListExtra(Utils.CC_ATTENDANTS, numbers);
            getActivity().startService(i);
            Toast.makeText(getContext(),"Invites sent successfully",Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
    }
    private boolean isVaildNumbers () {

        if (mEditTxtParticipant1.getText().toString().isEmpty() == false) {
            if (mEditTxtParticipant1.getText().toString().length() != 10 ) {
                Toast.makeText(getContext(),"Please entry vaild number for Participant 1",Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (mEditTxtParticipant2.getText().toString().isEmpty() == false) {
            if (mEditTxtParticipant2.getText().toString().length() != 10 ) {
                Toast.makeText(getContext(),"Please entry vaild number for Participant 2",Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (mEditTxtParticipant3.getText().toString().isEmpty() == false) {
            if (mEditTxtParticipant3.getText().toString().length() != 10 ) {
                Toast.makeText(getContext(),"Please entry vaild number for Participant 3",Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (mEditTxtParticipant4.getText().toString().isEmpty() == false) {
            if (mEditTxtParticipant1.getText().toString().length() != 10 ) {
                Toast.makeText(getContext(),"Please entry vaild number for Participant 4",Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return true;
    }
    @Override
    public void onNewDate(int year, int month, int dayofmonth) {
        mConfCallCal.set(Calendar.YEAR, year);
        mConfCallCal.set(Calendar.MONTH, month);
        mConfCallCal.set(Calendar.DAY_OF_MONTH, dayofmonth);
        mDateBtn.setText(mConfCallCal.get(Calendar.DAY_OF_MONTH) +  mConfCallCal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault()));
        mIsDateSet = true;
    }

    @Override
    public void onNewTime(int hourOfDay, int minute) {
        mConfCallCal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mConfCallCal.set(Calendar.MINUTE, minute);
        mIsTimeSet = true;
      //  mTimeBtn.setText(mConfCallCal.get(Calendar.HOUR_OF_DAY)+":"+mConfCallCal.get(Calendar.MINUTE));
        SimpleDateFormat simpleDF = new SimpleDateFormat("hh:mm a");
        mTimeBtn.setText(simpleDF.format(mConfCallCal.getTime()));

        }
        private boolean canSendInvite()
        {
          if (mIsDateSet == false) {
              Toast.makeText(getContext(), "Please set the date", Toast.LENGTH_LONG).show();
              return false;
          }
            if (mIsTimeSet == false) {
                Toast.makeText(getContext(), "Please set the time", Toast.LENGTH_LONG).show();
                return false;
            }
          if (mEditTxtParticipant1.getText().toString().isEmpty() &&
              mEditTxtParticipant2.getText().toString().isEmpty() &&
              mEditTxtParticipant3.getText().toString().isEmpty() &&
              mEditTxtParticipant4.getText().toString().isEmpty()) {
              Toast.makeText(getContext(), "Please set input participants number", Toast.LENGTH_LONG).show();
              return false;
          }
          return true;
        }

    @Override
    public void update(Observable o, Object arg) {
        if (ConferenceCallState.getInstance().getCcState() == ConferenceCallState.CcState.END) {
            ConferenceCallState.getInstance().clearAllState();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ConferenceCallState.getInstance().deleteObserver(this);
    }
}
