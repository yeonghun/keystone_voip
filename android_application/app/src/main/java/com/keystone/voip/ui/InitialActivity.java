package com.keystone.voip.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.keystone.voip.R;
import com.keystone.voip.account.Account;
import com.keystone.voip.config.ConfigStore;
import com.keystone.voip.state.CallState;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

public class InitialActivity extends AppCompatActivity implements OnFragmentInteractionListener {
    // Requesting permission to RECORD_AUDIO
    private static final String LOG_TAG = "InitialActivity";
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_initial);

        if (checkSelfPermission(android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            permissionToRecordAccepted = true;
            Log.e(LOG_TAG, "Permission To Record Audio Granted");
        } else {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (permissionToRecordAccepted)
            goToNextStep();
    }

    private void goToNextStep() {
        int delayMillis = 2000;
        if (!doesAccountExists()) {
            delayMillis = 1500;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent accActivity = new Intent(InitialActivity.this, AccountActivity.class);
                    startActivity(accActivity);
                    finish();
                }
            }, delayMillis);
        } else {
            delayMillis = 1500;
            CallState.getInstance().setLocalIP(Utils.getLocalIP(getApplicationContext()));
            CallState.getInstance().setLocalPhoneNumber(Account.getInstance().getPhoneNumber());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    new UpdateIpRegTask().execute();
                }
            }, delayMillis);
        }
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onActionPerformed(int fragmentName, int action, Bundle args) {
        if (action == 0) {
            Intent accActivity = new Intent(this, AccountActivity.class);
            startActivity(accActivity);
            finish();
        } else if (action == 1) {
            Intent accActivity = new Intent(this, ContentsActivity.class);
            startActivity(accActivity);
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                if (grantResults.length > 0 && permissions.length == grantResults.length)
                    permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                Log.e(LOG_TAG, "Request for Permission To Record Audio Granted");
                break;
        }
        if (!permissionToRecordAccepted) {
            Log.e(LOG_TAG, "Request for Permission To Record Audio Not Granted");
            finish();
        }
    }
    private boolean doesAccountExists () {
        Account.getInstance().retrieveSavedAccount(this);
        if (Account.getInstance().getPassword().isEmpty() || Account.getInstance().getPhoneNumber().isEmpty()) {
            return false;
        } else {
            Account.getInstance().setLocalIP(Utils.getLocalIP(this));
            Account.getInstance().saveAccount(this);
        }
        return true;
    }
    private void launchContentActivity ()
    {
        Intent accActivity = new Intent(InitialActivity.this, ContentsActivity.class);
        startActivity(accActivity);
        finish();
    }
    private class UpdateIpRegTask extends AsyncTask<String, String, String>
    {
        private final String FAIL = "fail";
        private final String PASS = "pass";
        private HttpURLConnection myConnection = null;

        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;
            try {

               URL serverURL = new URL(ConfigStore.Server.getAddress(getApplicationContext()) + "/user/update?" +
                        Utils.PHONENUMBER + "=" + Account.getInstance().getPhoneNumber() + "&" +
                        Utils.PASSWORD + "=" + Account.getInstance().getPassword());

                myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");
                myConnection.setConnectTimeout(2000);
                myConnection.setReadTimeout(3000);

                //Build Json for USER data
                JSONObject jsonUserData = new JSONObject();
                jsonUserData.put(Utils.FIRST_NAME, Account.getInstance().getFirstName());
                jsonUserData.put(Utils.LAST_NAME, Account.getInstance().getLastName());
                jsonUserData.put(Utils.EMAIL, Account.getInstance().getEmailID());
                jsonUserData.put(Utils.IP, Account.getInstance().getLocalIP());
                jsonUserData.put(Utils.REGID, FirebaseInstanceId.getInstance().getToken());
                jsonUserData.put(Utils.PHONENUMBER, Account.getInstance().getPhoneNumber());
                jsonUserData.put(Utils.PASSWORD, Account.getInstance().getPassword());

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonUserData.toString().getBytes());  // Write the data

                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    result = PASS;
                } else if (responseCode == 400) {
                    result = FAIL;
                }

            } catch (Exception e){
                result = FAIL;
                e.printStackTrace();

            } finally {
                if (myConnection != null) {
                    myConnection.disconnect();
                }
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals(FAIL))
            {
                //Toast.makeText(InitialActivity.this.getApplicationContext(),"Temp Msg: RegId & IP update to server failed ", Toast.LENGTH_LONG).show();
                Intent accActivity = new Intent(InitialActivity.this, AccountActivity.class);
                startActivity(accActivity);
                finish();
                return;
            } else {
               // Toast.makeText(InitialActivity.this.getApplicationContext(),getResources().getString(R.string.login_pass), Toast.LENGTH_LONG).show();
                launchContentActivity();
            }

        }

    }
}
