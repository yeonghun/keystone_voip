package com.keystone.voip.ui;

import com.keystone.voip.state.CallState;

public class ChatMessage {
    public String msg, sender, receiver;
    public int isMine;

    public ChatMessage(String Sender, String Receiver, String messageString, int isM) {
        msg = messageString;
        sender = Sender;
        receiver = Receiver;
        isMine = isM;
    }
}