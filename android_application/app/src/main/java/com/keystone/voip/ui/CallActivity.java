package com.keystone.voip.ui;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import com.keystone.voip.R;
import com.keystone.voip.service.CallService;
import com.keystone.voip.state.CallState;

import java.util.Observable;
import java.util.Observer;



public class CallActivity extends AppCompatActivity implements OnFragmentInteractionListener,Observer {
    private static final String TAG = "CallActivity";
    CallActivityFragment mCallFragment = null;
    boolean first = true;


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        int action = 0;
        action = getIntent().getIntExtra("action", 0);
        Log.d(TAG, "onNewIntent - " + Utils.ACTION.toString(action));

        if (action == Utils.ACTION.ACT_REQUEST_CALL || action == Utils.ACTION.ACT_REQ_PORT_OPEN) {
            Intent callServiceIntent = new Intent(this, CallService.class);
            callServiceIntent.putExtra("action", action);
            callServiceIntent.putExtra(Utils.CALL_ID,getIntent().getExtras().getString(Utils.CALL_ID,""));
            callServiceIntent.putExtra(Utils.CALLER_IP,getIntent().getExtras().getString(Utils.CALLER_IP,""));
            startService(callServiceIntent);
        }

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onPostCreate");
        super.onPostCreate(savedInstanceState);
        int action = 0;
        if (getIntent() != null) {
            action = getIntent().getIntExtra("action", 0);

            if (action == Utils.ACTION.ACT_REQUEST_CALL || action == Utils.ACTION.ACT_REQ_PORT_OPEN) {
                String calleePhoneNum = getIntent().getExtras().getString(Utils.TO_PHONE_NUMBER,"");
                String callID = getIntent().getExtras().getString(Utils.CALL_ID,"");

                Intent callServiceIntent = new Intent(this, CallService.class);
                callServiceIntent.putExtra("action", action);
                callServiceIntent.putExtra(Utils.TO_PHONE_NUMBER, calleePhoneNum);
                callServiceIntent.putExtra(Utils.FROM_PHONE_NUMBER, CallState.getInstance().getLocalPhoneNumber());
                callServiceIntent.putExtra(Utils.CALL_ID, callID);

                if (calleePhoneNum.isEmpty() == false) {
                    CallState.getInstance().setRemotePhoneNumber(calleePhoneNum);
                }
                if (callID.isEmpty() == false) {
                    CallState.getInstance().setCallID(callID);
                }

                startService(callServiceIntent);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CallState.getInstance().addObserver(this);
        if (findViewById(R.id.call_act_fragment_container) != null) {

            if (savedInstanceState != null) {
                return;
            }
            mCallFragment = new CallActivityFragment();
            mCallFragment.setArguments(getIntent().getExtras());
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.call_act_fragment_container, mCallFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (blockBackForCall() == false) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
           // Turn on screen and lockscreen
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onActionPerformed(int fragmentName, int action, Bundle args) {

    }

    @Override
    public void update(Observable o, Object arg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateGUI();
            }
        });
    }
    private void updateGUI() {
         if (mCallFragment != null) {
             mCallFragment.updateGUI();
         }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CallState.getInstance().deleteObserver(this);
        if (mCallFragment != null) {
            mCallFragment.disableProximityWakeLock();
        }
    }

    private boolean blockBackForCall() {
        if (CallState.getInstance().getCallState() == CallState.Call_State.IDLE ||
            CallState.getInstance().getCallState() == CallState.Call_State.CALLEND ||
            CallState.getInstance().getCallState() == CallState.Call_State.CALLREJECTED ||
                CallState.getInstance().getCallState() == CallState.Call_State.CONNECTION_FAIL) {
            return false;
        }
        return true;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
