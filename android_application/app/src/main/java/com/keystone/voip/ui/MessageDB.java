package com.keystone.voip.ui;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class MessageDB extends SQLiteOpenHelper{

    public static final String CONTACTS_COLUMN_SENDER = "sender";
    public static final String CONTACTS_COLUMN_RECEVER = "receiver";
    public static final String CONTACTS_COLUMN_MESSAGE = "msg";
    public static final String CONTACTS_COLUMN_ISMINE = "isMine";


    public MessageDB(Context context, String name, CursorFactory factory, int version)
    {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db){
        String sql = "create table savedmsgs (" +
                "_id integer primary key autoincrement," +
                "sender text," +
                "receiver text," +
                "msg text, " +
                "isMine integer);";

        db.execSQL(sql);
    }

    public void onUpgrade(SQLiteDatabase db, int oldversion, int newversion){
        db.execSQL("DROP TABLE IF EXISTS savedmsgs");
        onCreate(db);
    }

    public boolean insertMsg (String sender, String receiver, String msg, int isMine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_SENDER, sender);
        contentValues.put(CONTACTS_COLUMN_RECEVER, receiver);
        contentValues.put(CONTACTS_COLUMN_MESSAGE, msg);
        contentValues.put(CONTACTS_COLUMN_ISMINE, isMine);

        db.insert("savedmsgs", null, contentValues);

        return true;
    }

    public ArrayList<ChatMessage> getAllmsg() {
        ArrayList<ChatMessage> array_list = new ArrayList<ChatMessage>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from savedmsgs", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            ChatMessage cm = new ChatMessage(res.getString(res.getColumnIndex(CONTACTS_COLUMN_SENDER)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_RECEVER)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_MESSAGE)),
                    res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ISMINE)));

            //array_list.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_FIRST_NAME))+" " + res.getString(res.getColumnIndex(CONTACTS_COLUMN_LAST_NAME)) );
            array_list.add(cm);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<ChatMessage> getSeletedReceivermsg(String phoneNum) {
        ArrayList<ChatMessage> array_list = new ArrayList<ChatMessage>();

        String query = "select * from savedmsgs where sender = '" + phoneNum + "' or receiver = '" + phoneNum + "'";

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( query, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            ChatMessage cm = new ChatMessage(res.getString(res.getColumnIndex(CONTACTS_COLUMN_SENDER)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_RECEVER)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_MESSAGE)),
                    res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ISMINE)));

            Log.d(Utils.DEBUG_MSG_LJB, "is Mine =  : " + cm.isMine);

            array_list.add(cm);
            res.moveToNext();
        }
        return array_list;
    }
}
