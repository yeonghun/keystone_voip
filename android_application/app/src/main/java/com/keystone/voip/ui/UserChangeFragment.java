package com.keystone.voip.ui;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.keystone.voip.R;
import com.keystone.voip.account.Account;
import com.keystone.voip.config.ConfigStore;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserChangeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserChangeFragment extends BaseFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private final String TAG = "UserChangeFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private TextInputEditText mFirstName_Etv;
    private TextInputEditText mLastName_Etv;
    private TextInputEditText mEmail_Etv;
    private TextInputEditText mPassword_Etv;
    private TextInputEditText mAddress_Etv;
    private TextInputEditText mCard_Etv;

    private Button mUpdateInfo;


    public UserChangeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserChangeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserChangeFragment newInstance(String param1, String param2) {
        UserChangeFragment fragment = new UserChangeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.fragment_user_change, container, false);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
        mFirstName_Etv = (TextInputEditText) fragView.findViewById(R.id.uc_first_etv);
        mFirstName_Etv.setText(Account.getInstance().getFirstName());


        mLastName_Etv = (TextInputEditText) fragView.findViewById(R.id.uc_last_etv);
        mLastName_Etv.setText(Account.getInstance().getLastName());

        mEmail_Etv = (TextInputEditText) fragView.findViewById(R.id.uc_email_id_edittv);
        mEmail_Etv.setText(Account.getInstance().getEmailID());

        mPassword_Etv = (TextInputEditText) fragView.findViewById(R.id.uc_new_pwd_edittv);
        mPassword_Etv.setText(Account.getInstance().getPassword());

        mAddress_Etv = (TextInputEditText) fragView.findViewById(R.id.uc_address_edittv);
        mAddress_Etv.setText(Account.getInstance().getAddress());

        mCard_Etv = (TextInputEditText) fragView.findViewById(R.id.uc_card_edittv);
        mCard_Etv.setText(Account.getInstance().getCardInfo());

        mUpdateInfo = (Button) fragView.findViewById(R.id.uc_update_btn);
        mUpdateInfo.setOnClickListener(this);
        return fragView;
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            Log.e(TAG, " onClick : v = null");
            return;
        }
        if (v == mUpdateInfo) {
            if (validateInputFields() == false) {
                return;
            }
            new UpdateInfoTask().execute();
        }
    }

    private boolean validateInputFields() {

        if ((mFirstName_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.first_name), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mLastName_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.last_name), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mEmail_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.email), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mPassword_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.password), Toast.LENGTH_LONG).show();
            return false;
        }


        return true;
    }


    private class UpdateInfoTask extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(UserChangeFragment.this.getContext());
        private final String FAIL = "fail";
        private final String PASS = "pass";
        private HttpURLConnection myConnection = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pdLoading != null) {
                pdLoading.setMessage(getResources().getString(R.string.processing_msg));
                pdLoading.show();
            }
        }
        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;
            try {

                URL serverURL = new URL(ConfigStore.Server.getAddress(UserChangeFragment.this.getContext()) + "/user/update?" +
                        Utils.PHONENUMBER + "=" + Account.getInstance().getPhoneNumber() + "&" +
                        Utils.PASSWORD + "=" + Account.getInstance().getPassword());

                myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");
                myConnection.setConnectTimeout(2000);
                myConnection.setReadTimeout(3000);

                //Build Json for USER data
                JSONObject jsonUserData = new JSONObject();
                jsonUserData.put(Utils.FIRST_NAME, mFirstName_Etv.getText().toString());
                jsonUserData.put(Utils.LAST_NAME, mLastName_Etv.getText().toString());
                jsonUserData.put(Utils.EMAIL, mEmail_Etv.getText());
                jsonUserData.put(Utils.IP, Account.getInstance().getLocalIP());
                jsonUserData.put(Utils.REGID, FirebaseInstanceId.getInstance().getToken());
                jsonUserData.put(Utils.PHONENUMBER, Account.getInstance().getPhoneNumber());
                jsonUserData.put(Utils.PASSWORD, mPassword_Etv.getText());
                //Todo uncomment below code once server chagnes are ready.
                /*jsonUserData.put(Utils.ADDRESS,  mAddress_Etv.getText());
                jsonUserData.put(Utils.CARDINFO, mCard_Etv.getText());*/

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonUserData.toString().getBytes());  // Write the data

                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    result = PASS;
                    Account.getInstance().setFirstName(mFirstName_Etv.getText().toString());
                    Account.getInstance().setLastName(mLastName_Etv.getText().toString());
                    Account.getInstance().setEmailID(mEmail_Etv.getText().toString());
                    Account.getInstance().setPassword(mPassword_Etv.getText().toString());
                    Account.getInstance().saveAccount(UserChangeFragment.this.getContext());
                } else if (responseCode == 400) {
                    result = FAIL;
                }

            } catch (Exception e){
                result = FAIL;
                e.printStackTrace();

            } finally {
                if (myConnection != null) {
                    myConnection.disconnect();
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pdLoading != null) {
                pdLoading.dismiss();
            }
            if (result.equals(FAIL))
            {
                Toast.makeText(UserChangeFragment.this.getContext(),getResources().getString(R.string.update_info_fail), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(UserChangeFragment.this.getContext(),getResources().getString(R.string.update_info_success), Toast.LENGTH_LONG).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().finish();
                    }
                }, 500);
            }
        }

    }
}
