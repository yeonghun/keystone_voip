package com.keystone.voip.service;

import android.content.Context;
import android.util.Log;

import com.keystone.voip.config.ConfigStore;
import com.keystone.voip.state.CallState;
import com.keystone.voip.state.ConferenceCallState;
import com.keystone.voip.ui.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class CallSessionHandler {
    private static final String TAG = "CallSessionHandler";
    private Context mContext;
    private CallSessionListener mListener;
    private class Response {
        int code;
        String msg;
        Response(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        @Override
        public String toString() {
            return "Response: " + code + "(" + msg + ")";
        }
    }

    public CallSessionHandler(Context context) {
        this(context, null);
    }

    public CallSessionHandler(Context context, CallSessionListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void requestCall(final String callerPhoneNumber, final String calleePhoneNumber) {
        Log.i(TAG, "requestCall~!: ");
        CallState.getInstance().setLocalIP(Utils.getLocalIP(mContext));
        Thread callReqThread = new Thread(new Runnable(){
            @Override
            public void run()
            {
                boolean passResult = false;
                HttpURLConnection myConnection = null;
                try {
                    URL serverURL = new URL(ConfigStore.Server.getAddress(mContext) +
                            "/call/request");
                    myConnection = (HttpURLConnection) serverURL.openConnection();
                    myConnection.setRequestMethod("POST");
                    myConnection.setRequestProperty("Content-Type", "application/json");
                    myConnection.setRequestProperty("User-Agent", "keystone");


                    JSONObject jsonLoginData = new JSONObject();
                    jsonLoginData.put(Utils.FROM_PHONE_NUMBER, callerPhoneNumber);
                    jsonLoginData.put(Utils.TO_PHONE_NUMBER, calleePhoneNumber);

                    myConnection.setDoOutput(true);  // Enable writing
                    myConnection.getOutputStream().write(jsonLoginData.toString().getBytes());

                    InputStream responseBody;
                    String responseString = "";
                    String callIdString = "";
                    String calleeIpString = "";
                    JSONObject jsonReader = null;

                    int responseCode = myConnection.getResponseCode();
                    if (responseCode == 200) {
                        responseBody = myConnection.getInputStream();
                        responseString = Utils.getStringFromInputStream(responseBody);

                        jsonReader = new JSONObject(responseString);
                        callIdString = jsonReader.getString(Utils.CALL_ID);
                        calleeIpString = jsonReader.getString(Utils.CALLEE_IP);
                        Log.e(TAG, "callIdString: " + callIdString + ", calleeIpString: " + calleeIpString);
                        CallState.getInstance().setCallID(callIdString);
                        CallState.getInstance().setRemoteIP(calleeIpString);
                        passResult = true;
                    } else {
                        passResult = false;
                    }

                } catch (Exception e) {
                    passResult = false;
                } finally {
                    if (myConnection != null) {
                        myConnection.disconnect();
                    }
                    if (mListener != null) {
                        mListener.onCallRequested(passResult);
                    }
                }
            }
        });
        callReqThread.start();
    }

    public void acceptCall(final String callID) {
        Log.i(TAG, "acceptCall~!: ");
        Thread callAcceptThread = new Thread(new Runnable(){
            @Override
            public void run() {
                Response response = new Response(200, "");
                try {
                    JSONObject jsonUserData = new JSONObject();
                    jsonUserData.put(Utils.CALL_ID, callID);
                    response = sendMessage("/call/accept", jsonUserData);
                } catch (Exception e) {
                } finally {
                    Log.w(TAG, "acceptCall, response: " + response);
                }
            }
        });
        callAcceptThread.start();
        return;
    };

    public void rejectCall(final String callID, final String myNumber) {
        Log.i(TAG, "rejectCall~!: ");
        Thread callRejectThread = new Thread(new Runnable(){
            @Override
            public void run() {
                Response response = new Response(200, "");
                try {
                    JSONObject jsonUserData = new JSONObject();
                    jsonUserData.put(Utils.CALL_ID, callID);
                    jsonUserData.put(Utils.FROM_PHONE_NUMBER, myNumber);
                    response = sendMessage("/call/reject", jsonUserData);
                } catch (Exception e) {
                } finally {
                    Log.w(TAG, "rejectCall, response: " + response);
                }
            }
        });
        callRejectThread.start();
        return;
    }

    public void endCall(final String callID) {
        Thread endCallThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = new Response(200, "");
                try {
                    JSONObject jsonUserData = new JSONObject();
                    jsonUserData.put(Utils.CALL_ID, callID);
                    response = sendMessage("/call/end", jsonUserData);
                } catch (Exception e) {
                } finally {
                    Log.w(TAG, "endCall, response: " + response);
                }
            }
        });
        endCallThread.start();
    }

    public void sessionTimeout(final String callID) {
        Thread sessionTimeoutThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Response responseCode = new Response(200, "");
                try {
                    JSONObject jsonUserData = new JSONObject();
                    jsonUserData.put(Utils.CALL_ID, callID);
                    responseCode = sendMessage("/call/timeout", jsonUserData);
                } catch (Exception e) {
                } finally {
                    Log.w(TAG, "sessionTimeout, responseCode: " + responseCode);
                }
            }
        });
        sessionTimeoutThread.start();
    }

    public void billingHeartbeat(final String callID) {
        Thread billingHeartbeatThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = new Response(200, "");
                try {
                    JSONObject jsonUserData = new JSONObject();
                    jsonUserData.put(Utils.CALL_ID, callID);
                    response = sendMessage("/call/heartbeat", jsonUserData);
                } catch (Exception e) {
                } finally {
                    Log.w(TAG, "sessionTimout, response: " + response);
                }
            }
        });
        billingHeartbeatThread.start();
    }


    private Response sendMessage(String page, JSONObject jsonUserData) throws Exception {
        HttpURLConnection myConnection = null;
        URL serverURL = new URL(ConfigStore.Server.getAddress(mContext) +
                page);
        myConnection = (HttpURLConnection) serverURL.openConnection();
        myConnection.setRequestMethod("POST");
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestProperty("User-Agent", "keystone");
        //Build Json for USER data
        Log.d(TAG, "sendMessage: " + serverURL.toString() + " " + jsonUserData.toString());
        myConnection.setDoOutput(true);
        myConnection.getOutputStream().write(jsonUserData.toString().getBytes());// Write the data

        int code = myConnection.getResponseCode();
        InputStream responseBody = myConnection.getInputStream();
        String msg = Utils.getStringFromInputStream(responseBody);
        if (myConnection != null) {
            myConnection.disconnect();
        }
        return new Response(code, msg);
    }
}
