package com.keystone.voip.account;


import android.content.Context;
import android.content.SharedPreferences;

public class Account
{
    private String mFirstName = "";
    private String mLastName = "";
    private String mEmailID = "";
    private String mPassword = "";
    private String mLocalIP = "";
    private String mPhoneNumber = "";
    private String mAddress = "";
    private String mCardInfo = "";

    private static Account sAccountInstance = new Account();

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getEmailID() {
        return mEmailID;
    }

    public void setEmailID(String emailID) {
        mEmailID = emailID;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getLocalIP() {
        return mLocalIP;
    }

    public void setLocalIP(String localIP) {
        mLocalIP = localIP;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getAddress() { return mAddress; }

    public void setAddress(String address) { this.mAddress = address; }

    public String getCardInfo() { return mCardInfo; }

    public void setCardInfo(String cardInfo) { this.mCardInfo = cardInfo; }

    public static final Account getInstance() {
        if (sAccountInstance == null) {
            sAccountInstance = new Account();
        }
        return sAccountInstance;
    }

    // Temp : till DB is implemented.
    public void saveAccount (Context context) {
        SharedPreferences pref = context.getSharedPreferences("KeyStonePreference", 0);
        SharedPreferences.Editor prefsEditor = pref.edit();
        prefsEditor .putString("FirstName",mFirstName);
        prefsEditor .putString("LastName", mLastName );
        prefsEditor .putString("EmailID", mEmailID);
        prefsEditor .putString("Password", mPassword);
        prefsEditor .putString("LocalIP", mLocalIP);
        prefsEditor .putString("PhoneNumber", mPhoneNumber);

        prefsEditor.commit();
    }

    public void retrieveSavedAccount (Context context) {
        SharedPreferences pref = context.getSharedPreferences("KeyStonePreference", 0);
        if (pref!= null) {
            mFirstName = pref.getString("FirstName", "");
            mLastName = pref.getString("LastName", "");
            mEmailID = pref.getString("EmailID", "");
            mPassword = pref.getString("Password", "");
            mLocalIP = pref.getString("LocalIP", "");
            mPhoneNumber = pref.getString("PhoneNumber", "");
        }
    }

    public void clearSavedAccount (Context context) {
        SharedPreferences pref = context.getSharedPreferences("KeyStonePreference", 0);
        SharedPreferences.Editor prefsEditor = pref.edit();

        prefsEditor .putString("FirstName", "");
        prefsEditor .putString("LastName", "");
        prefsEditor .putString("EmailID", "");
        prefsEditor .putString("Password", "");
        prefsEditor .putString("LocalIP", "");
        prefsEditor .putString("PhoneNumber", "");

        mFirstName = "";
        mLastName = "";
        mEmailID = "";
        mPassword = "";
        mLocalIP = "";
        mPhoneNumber = "";

        prefsEditor.commit();
    }
}
