package com.keystone.voip.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.keystone.voip.audio.VoIPAudioIo;
import com.keystone.voip.audio.VoIPAudioListener;
import com.keystone.voip.state.CallState;
import com.keystone.voip.ui.Utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static com.keystone.voip.ui.Utils.ACTION.ACT_ACCEPT_CALL;
import static com.keystone.voip.ui.Utils.ACTION.ACT_END_CALL;
import static com.keystone.voip.ui.Utils.ACTION.ACT_PEER_READY;
import static com.keystone.voip.ui.Utils.ACTION.ACT_REJECT_CALL;
import static com.keystone.voip.ui.Utils.ACTION.ACT_REJECT_CALL_RECEIVED;
import static com.keystone.voip.ui.Utils.ACTION.ACT_REQUEST_CALL;
import static com.keystone.voip.ui.Utils.ACTION.ACT_REQ_PORT_OPEN;

public class CallService extends Service {
    private static final String TAG = "CallService";
    private CallSessionHandler mCallSessionHandler;
    private VoIPAudioIo mVoIPAudio;
    private Handler mHandler;

    private class CallSessionListenerImpl implements CallSessionListener {
        @Override
        public void onCallRequested(boolean passResult) {
            if (passResult) {
                updateAndNotifyCallState(CallState.Call_State.CALLING);
                // start session timeout thread
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        new SessionTimeout(CallState.getInstance().getCallID()).start();
                    }
                });
            } else {
                updateAndNotifyCallState(CallState.Call_State.CONNECTION_FAIL);
            }
        }
    }

    private class VoIPAudioListenerImpl implements VoIPAudioListener {
        @Override
        public void onCallTerminated(int result) {
            // do handle here and update Call State and notify it
            Log.i(TAG, "onCallTerminated~! result: " + result +
                    ", CallState: " + CallState.getInstance().getCallState());

            if (result == CALL_TERMINATED_BY_EXCEPTION &&
                    (CallState.getInstance().getCallState() == CallState.Call_State.INCOMING ||
                            CallState.getInstance().getCallState() == CallState.Call_State.INCALL)) {
                String callId = CallState.getInstance().getCallID();
                String remoteIp = CallState.getInstance().getRemoteIP();
                Log.d(TAG, "It might be happened due to remoteIp end call, " +
                        "callId: " + callId + ", remoteIp" + remoteIp);

                // 1. send end call msg to server
                mCallSessionHandler.endCall(callId);

                // 2. end audio session
                mVoIPAudio.endAudio();

                // 3. update call state
                updateAndNotifyCallState(CallState.Call_State.CALLEND);
            } else {
                Log.i(TAG, "invalid state to handle onCallTerminated");
            }
        }
    }

    private class SessionTimeout extends CountDownTimer {
        private String callId;

        SessionTimeout(String id) {
            this(45 * 1000, 5 * 1000);
            callId = id;
        }

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public SessionTimeout(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            Log.d(TAG, "SessionTimeout, seconds remaining: " + millisUntilFinished / 1000);
        }

        @Override
        public void onFinish() {
            Log.d(TAG, "SessionTimeout, callId: " + callId + ", " + CallState.getInstance().dump());
            if (callId == null) {
                return;
            }
            if (!callId.equals(CallState.getInstance().getCallID())) {
                Log.w(TAG, "SessionTimeout, mismatch call id so ignore it");
                return; // mismatch callid
            }
            if (CallState.getInstance().getCallState() != CallState.Call_State.CALLING &&
                    CallState.getInstance().getCallState() != CallState.Call_State.INCOMING) {
                Log.w(TAG, "SessionTimeout, invalid state so ignore it");
                return; // invalid state
            }
            // now, timeout so clear our state for next call and then send a msg to server
            // 1. send timeout msg to server
            mCallSessionHandler.sessionTimeout(CallState.getInstance().getCallID());
            // 2. clear our state
            updateAndNotifyCallState(CallState.Call_State.CALLEND);
        }
    }

    private class BillingHeartBeat implements Runnable {
        private static final int INTERVAL = 60 * 1000;
        private String callId;

        BillingHeartBeat(String id) {
            callId = id;
        }

        @Override
        public void run() {
            Log.d(TAG, "BillingHeartBeat~! callId: " + callId + ", " + CallState.getInstance().dump());
            if (!callId.equals(CallState.getInstance().getCallID())) {
                Log.w(TAG, "BillingHeartBeat, call id mismatch, so stop it");
                return;
            }
            if (CallState.getInstance().getCallState() != CallState.Call_State.INCALL) {
                Log.w(TAG, "BillingHeartBeat, invalid state, so stop it");
                return;
            }
            // enable when server is ready
            mCallSessionHandler.billingHeartbeat(callId);
            mHandler.postDelayed(this, INTERVAL);
        }
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent ) {
            if (intent.hasExtra("OUT")) {
                int out_mode = intent.getIntExtra("OUT", 100);
                mVoIPAudio.changeAudioOut(out_mode);
            }

            if (intent.hasExtra("MIC")) {
                boolean micEnable = intent.getBooleanExtra("MIC", true);
                mVoIPAudio.toggleMicIn(micEnable);
            }

        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Utils.ACTION.toString(Utils.ACTION.ACT_CHANGE_AUDIO_MODE));
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(mBroadcastReceiver, filter);
        mCallSessionHandler = new CallSessionHandler(this, new CallSessionListenerImpl());
        mVoIPAudio = new VoIPAudioIo(this, new VoIPAudioListenerImpl());
        mHandler = new Handler();
        CallState.getInstance().setCallState(CallState.Call_State.IDLE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
         super.onStartCommand(intent, flags, startId);
         if (intent == null) {
             Log.w(TAG, "our process was automatically restarted with null intent");
             return START_NOT_STICKY;
         }
         Bundle extraBundle = intent.getExtras();
         int action = extraBundle.getInt("action");
         Log.i(TAG, "onStartCommand~! action: " + Utils.ACTION.toString(action) +
                 " " + CallState.getInstance().dump());

         switch (action) {
             case ACT_REQUEST_CALL:
                 if (CallState.getInstance().getCallState() == CallState.Call_State.IDLE) {
                     updateAndNotifyCallState(CallState.Call_State.CONNECTING);
                     // send request msg to server
                     mCallSessionHandler.requestCall(CallState.getInstance().getLocalPhoneNumber(),
                             CallState.getInstance().getRemotePhoneNumber());
                 } else {
                     Log.w(TAG, "invalid state to handle ACT_REQUEST_CALL");
                 }
                 break;

             case ACT_REQ_PORT_OPEN:
                 if (CallState.getInstance().getCallState() == CallState.Call_State.IDLE) {
                     // update call state so let ui know incoming call received
                     updateAndNotifyCallState(CallState.Call_State.INCOMING);
                     // start session timeout thread
                     new SessionTimeout(CallState.getInstance().getCallID()).start();
                 } else {
                     Log.w(TAG, "invalid state to handle ACT_REQ_PORT_OPEN");
                 }
                 break;

             case ACT_ACCEPT_CALL:
                 if (CallState.getInstance().getCallState() == CallState.Call_State.INCOMING) {
                     String callID = CallState.getInstance().getCallID();
                     String callerIp = CallState.getInstance().getRemoteIP();

                     // 1. update call state
                     updateAndNotifyCallState(CallState.Call_State.INCALL);

                     // 2. send msg to server with callID
                     mCallSessionHandler.acceptCall(callID);

                     // 3. let's voice call with caller ip~!
                     try {
                         InetAddress address = InetAddress.getByName(callerIp);
                         mVoIPAudio.startAudio(address, 0);
                     } catch (UnknownHostException e) {
                         e.printStackTrace();
                     }
                 } else {
                     Log.w(TAG, "invalid state to handle ACT_ACCEPT_CALL");
                 }
                 break;

             case ACT_REJECT_CALL:
                 if (CallState.getInstance().getCallState() == CallState.Call_State.CALLING ||
                         CallState.getInstance().getCallState() == CallState.Call_State.INCOMING) {
                     // 1. send reject msg to server
                     String callID = CallState.getInstance().getCallID();
                     String myNumber = CallState.getInstance().getLocalPhoneNumber();
                     mCallSessionHandler.rejectCall(callID, myNumber);

                     // 2. update call state
                     updateAndNotifyCallState(CallState.Call_State.CALLREJECTED);
                 } else {
                     Log.w(TAG, "invalid state to handle ACT_REJECT_CALL");
                 }
                 break;

             case ACT_REJECT_CALL_RECEIVED:
                 if (CallState.getInstance().getCallState() == CallState.Call_State.CALLING ||
                         CallState.getInstance().getCallState() == CallState.Call_State.INCOMING) {
                     // 1. end audio session
                     mVoIPAudio.endAudio();

                     // 2. update call state
                     updateAndNotifyCallState(CallState.Call_State.CALLEND);
                 }
                 break;

             case ACT_PEER_READY:
                 if (CallState.getInstance().getCallState() != CallState.Call_State.CALLING) {
                     Log.w(TAG, "invalid state to handle ACT_PEER_READY");
                     break;
                 }

                 // 1. update call state
                 updateAndNotifyCallState(CallState.Call_State.INCALL);

                 // 2. let's start voip call with callee
                 try {
                     InetAddress address = InetAddress.getByName(CallState.getInstance().getRemoteIP());
                     mVoIPAudio.startAudio(address, 0);
                 } catch (UnknownHostException e) {
                     e.printStackTrace();
                 }

                 // 3. send a periodic msg to server during calling
                 mHandler.postDelayed(new BillingHeartBeat(CallState.getInstance().getCallID()),
                         BillingHeartBeat.INTERVAL);
                 break;

             case ACT_END_CALL:
                 if (CallState.getInstance().getCallState() == CallState.Call_State.INCOMING ||
                         CallState.getInstance().getCallState() == CallState.Call_State.CALLING ||
                         CallState.getInstance().getCallState() == CallState.Call_State.INCALL) {

                     // 1. send end call msg to server
                     mCallSessionHandler.endCall(CallState.getInstance().getCallID());

                     // 2. end audio session
                     mVoIPAudio.endAudio();

                     // 3. update call state
                     updateAndNotifyCallState(CallState.Call_State.CALLEND);
                 } else {
                     Log.i(TAG, "invalid state to handle ACT_END_CALL");
                 }
                 break;

             default:
                 CallState.getInstance().setCallState(CallState.Call_State.IDLE);
                 break;
         }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        LocalBroadcastManager.getInstance(this.getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
        if (mVoIPAudio != null) {
            mVoIPAudio.endAudio();
        }
    }


    private void updateAndNotifyCallState(CallState.Call_State state) {
        CallState.getInstance().setCallState(state);
        CallState.getInstance().notifyUpdate();
    }

}
