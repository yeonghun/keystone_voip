package com.keystone.voip.ui;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.keystone.voip.R;

public class SettingsActivity extends AppCompatActivity implements OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (findViewById(R.id.fragment_container_settings) != null) {

            if (savedInstanceState != null) {
                return;
            }
            if (getIntent().getAction().equals("userinfo")) {
                UserChangeFragment usrChngeFragment = UserChangeFragment.newInstance("", "");
                usrChngeFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container_settings, usrChngeFragment).commit();
            } else if (getIntent().getAction().equals("billinghistory")) {
               BillingHistoryFragment billingFrag = BillingHistoryFragment.newInstance("","");
                billingFrag.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container_settings, billingFrag).commit();
            } else if (getIntent().getAction().equals("callhistory")) {
                CallHistoryFragment callHistoryFrag = CallHistoryFragment.newInstance("","");
                callHistoryFrag.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container_settings, callHistoryFrag).commit();
            } else if (getIntent().getAction().equals("settings")) {
              
             getFragmentManager().beginTransaction().replace(R.id.fragment_container_settings,
                        SettingsPreferenceFragment.newInstance("test1","test2")).commit();
            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onActionPerformed(int fragmentName, int action, Bundle args) {

    }
    public interface newDateTimeListner {
        void onNewDate(int year, int month, int dayofmonth);
        void onNewTime(int hourOfDay, int minute);
    }
}
