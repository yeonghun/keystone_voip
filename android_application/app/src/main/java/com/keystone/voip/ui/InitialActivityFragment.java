package com.keystone.voip.ui;

import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.firebase.iid.FirebaseInstanceId;
import com.keystone.voip.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitialActivityFragment extends BaseFragment implements View.OnClickListener{
    private final String TAG = "InitialActivityFragment";

    public InitialActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragView = inflater.inflate(R.layout.fragment_initial, container, false);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
        //TODO remove below code later...


        return fragView;
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            Log.e(TAG, " onClick : v = null");
            return;
        }

        int action = -1;

        return;
    }
}
