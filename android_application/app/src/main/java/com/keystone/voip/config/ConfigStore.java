package com.keystone.voip.config;

import android.content.Context;

import com.keystone.voip.R;
import com.keystone.voip.ui.Utils;

public class ConfigStore {
    public static class Server {
        public static String getAddress(Context context) {
            String prefVal = Utils.getDataFromPreference(context, "server_address");
            if (prefVal != null && prefVal.length() > 0) {
                return prefVal;
            }
            return context.getResources().getString(R.string.default_server_address);
        }

        public static void updateAddress(Context context, String val) {
            Utils.saveDataToPreference(context, "server_address", val);
        }
    }
}
