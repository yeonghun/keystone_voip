package com.keystone.voip.audio;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.net.InetSocketAddress;
import java.io.InputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.util.Log;
import android.os.Process;

import com.keystone.voip.R;
import com.keystone.voip.ui.Utils;

import static android.support.constraint.Constraints.TAG;
import static com.keystone.voip.audio.VoIPAudioListener.CALL_TERMINATED_BY_END_CALL;
import static com.keystone.voip.audio.VoIPAudioListener.CALL_TERMINATED_BY_EXCEPTION;

public class VoIPAudioIo {

    private static final String LOG_TAG = "VoIPAudioIo";
    private static final int MILLISECONDS_IN_A_SECOND = 1000;
    private static final int SAMPLE_RATE = 8000; // Hertz
    private static final int SAMPLE_INTERVAL = 20;   // Milliseconds
    private static final int BYTES_PER_SAMPLE = 2;    // Bytes Per Sampl;e
    private static final int RAW_BUFFER_SIZE = SAMPLE_RATE / (MILLISECONDS_IN_A_SECOND / SAMPLE_INTERVAL) * BYTES_PER_SAMPLE;
    private static final int GSM_BUFFER_SIZE = 33;
    private static final int VOIP_DATA_UDP_PORT = 5124;
    private static final int SOCKET_TIME_OUT = 2 * 1000;
    private static final int RTP_PACKET_SIZE = GSM_BUFFER_SIZE + 12;

    private ConcurrentLinkedQueue<byte[]> mIncommingpacketQueue;
    private Context mContext;
    private InetAddress mRemoteIp;                   // Address to call
    private Thread mAudioRecorder;
    private Thread mPacketListener;
    private VoIPAudioListener mVoIPAudioListener;
    private ScheduledExecutorService mExecutor;
    private AudioOutputTask mAudioOutputTask;
    private Object mLock = new Object();

    private int mSimVoice;
    private boolean mIsRunning = false;
    private boolean mAudioRecorderThreadRun = false;
    private boolean mListenerThreadRun = false;

    private com.keystone.voip.audio.DeJitterBuffer mJitterBuffer;

    private class AudioOutputTask implements Runnable {
        final AudioTrack outputTrack;
        public byte[] jbuf;
        public byte[] rawbuf;
        AudioOutputTask() {
            outputTrack = new AudioTrack.Builder()
                    .setAudioAttributes(new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                            //	.setFlags(AudioAttributes.FLAG_LOW_LATENCY) //This is Nougat+ only (API 25) comment if you have lower
                            .build())
                    .setAudioFormat(new AudioFormat.Builder()
                            .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                            .setSampleRate(SAMPLE_RATE)
                            .setChannelMask(AudioFormat.CHANNEL_OUT_MONO).build())
                    .setBufferSizeInBytes(RAW_BUFFER_SIZE)
                    .setTransferMode(AudioTrack.MODE_STREAM)
                    //.setPerformanceMode(AudioTrack.PERFORMANCE_MODE_LOW_LATENCY) //Not until Api 26
                    //.setSessionId(recorder.getAudioSessionId()
                    .build();
            outputTrack.play();
            jbuf = new byte[GSM_BUFFER_SIZE];
            rawbuf = new byte[RAW_BUFFER_SIZE];
        }

        @Override
        public void run() {
            int bufferstate;
            //Log.d(LOG_TAG, "AudioOutputTask, run: " + mIncommingpacketQueue.size());
            if(mJitterBuffer.getPacketCount() < 15) {
                synchronized (mLock) {
                    bufferstate = mJitterBuffer.read(jbuf);
                }
                if (bufferstate != com.keystone.voip.audio.DeJitterBuffer.EMPTY) {
                    AudioNative.JniGsmDecodeB(jbuf, rawbuf);
                    outputTrack.write(rawbuf, 0, RAW_BUFFER_SIZE);

                }
            }else {
                /* Read multiple packet */
                for(int cnt  = 0 ; cnt < 5 ; cnt++){
                    synchronized (mLock) {
                        bufferstate = mJitterBuffer.read(jbuf);
                    }
                    if (bufferstate != com.keystone.voip.audio.DeJitterBuffer.EMPTY) {
                        AudioNative.JniGsmDecodeB(jbuf, rawbuf);
                        outputTrack.write(rawbuf, 0, RAW_BUFFER_SIZE);
                    }
                }
            }
        }

        public void end() {
            outputTrack.stop();
            outputTrack.flush();
            outputTrack.release();
        }
    }

    public VoIPAudioIo(Context context, VoIPAudioListener listener) {
        mContext = context;
        mVoIPAudioListener = listener;
        mJitterBuffer = new com.keystone.voip.audio.DeJitterBuffer();
    }

    public synchronized boolean startAudio(InetAddress remoteIp, int simVoice) {
        if (mIsRunning) return (true);
        if (AudioNative.JniGsmOpen() == 0)
            Log.i(LOG_TAG, "JniGsmOpen() Success");
        mIncommingpacketQueue = new ConcurrentLinkedQueue<>();
        mSimVoice = simVoice;
        mRemoteIp = remoteIp;
        startAudioRecorderThread();
        startReceiveDataThread();
        startAudioOutputThread();
        mIsRunning = true;
        return (false);
    }

    public synchronized boolean endAudio() {
        Log.i(LOG_TAG, "endAudio~! mIsRunning: " + mIsRunning
                + ", mPacketListener: " + mPacketListener + ", mAudioRecorder: " + mAudioRecorder);
        if (!mIsRunning) return (true);
        Log.i(LOG_TAG, "Ending VoIP Audio");
        // stop audio output
        if (!mExecutor.isShutdown()) {
            mExecutor.shutdown(); // stop periodic execution
            mAudioOutputTask.end(); // for audio track
        }
        // stop packet listener
        if (mPacketListener != null && mPacketListener.isAlive()) {
            mListenerThreadRun = false;
            Log.i(LOG_TAG, "mPacketListener Join started");
            try {
                mPacketListener.join();
            } catch (InterruptedException e) {
                Log.i(LOG_TAG, "mPacketListener Join interrupted");
            }
            Log.i(LOG_TAG, "mPacketListener Join success");
        }
        // stop audio recorder
        if (mAudioRecorder != null && mAudioRecorder.isAlive()) {
            mAudioRecorderThreadRun = false;
            Log.i(LOG_TAG, "mAudioRecorder Join started");

            try {
                mAudioRecorder.join();
            } catch (InterruptedException e) {
                Log.i(LOG_TAG, "mAudioRecorder Join interrupted");
            }
            Log.i(LOG_TAG, "mAudioRecorder Join success");
        }
        mAudioRecorder = null;
        mPacketListener = null;
        mExecutor = null;
        mAudioOutputTask = null;
        mIncommingpacketQueue = null;
        AudioNative.JniGsmClose();
        mIsRunning = false;
        mJitterBuffer.flush();
        return (false);
    }

    private InputStream OpenSimVoice(int SimVoice) {
        InputStream VoiceFile = null;
        switch (SimVoice) {
            case 0:
                break;
            case 1:
                VoiceFile = mContext.getResources().openRawResource(R.raw.t18k16bit);
                break;
            case 2:
                VoiceFile = mContext.getResources().openRawResource(R.raw.t28k16bit);
                break;
            case 3:
                VoiceFile = mContext.getResources().openRawResource(R.raw.t38k16bit);
                break;
            case 4:
                VoiceFile = mContext.getResources().openRawResource(R.raw.t48k16bit);
                break;
            default:
                break;
        }
        return VoiceFile;
    }

    private void startAudioRecorderThread() {
        // Creates the thread for capturing and transmitting audio
        mAudioRecorderThreadRun = true;
        mAudioRecorder = new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO);
                AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
                int prevAudioMode = 0;
                if (audioManager != null) {
                    prevAudioMode = audioManager.getMode();
                    audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION); //Enable AEC
                }

                // Create an instance of the AudioRecord class
                InputStream inputPlayFile = OpenSimVoice(mSimVoice);
                AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE,
                        AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT,
                        AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT));
                com.keystone.voip.audio.RTPPacket genRtpPacket = new com.keystone.voip.audio.RTPPacket(0, 0x1234);

                int bytesRead;
                long rtpTimestamp = 0x2000;
                byte[] rawbuf = new byte[RAW_BUFFER_SIZE];
                byte[] gsmbuf = new byte[GSM_BUFFER_SIZE];
                byte[] rtpbuf;
                try {
                    // Create a socket and start recording
                    Log.i(LOG_TAG, "Packet destination: " + mRemoteIp.toString());
                    DatagramSocket socket = new DatagramSocket();
                    socket.setSoTimeout(SOCKET_TIME_OUT);
                    recorder.startRecording();
                    while (mAudioRecorderThreadRun) {
                        // Capture audio from microphone and send
                        bytesRead = recorder.read(rawbuf, 0, RAW_BUFFER_SIZE);
                        if (inputPlayFile != null) {
                            bytesRead = inputPlayFile.read(rawbuf, 0, RAW_BUFFER_SIZE);
                            if (bytesRead != RAW_BUFFER_SIZE) {
                                inputPlayFile.close();
                                inputPlayFile = OpenSimVoice(mSimVoice);
                                bytesRead = inputPlayFile.read(rawbuf, 0, RAW_BUFFER_SIZE);
                            }
                        }
                        rtpTimestamp = (rtpTimestamp + 160) & 0xffffffff;
                        if (bytesRead == RAW_BUFFER_SIZE) {
                            AudioNative.JniGsmEncodeB(rawbuf, gsmbuf);

                            rtpbuf = genRtpPacket.genRTPPacket(gsmbuf, rtpTimestamp, GSM_BUFFER_SIZE);
                            DatagramPacket packet = new DatagramPacket(rtpbuf, RTP_PACKET_SIZE, mRemoteIp, VOIP_DATA_UDP_PORT);
                            socket.send(packet);
                        }
                    }
                    // Stop Audio Thread);
                    socket.disconnect();
                    socket.close();
                    if (inputPlayFile != null) inputPlayFile.close();
                    if (audioManager != null) audioManager.setMode(prevAudioMode);
                    Log.i(LOG_TAG, "mAudioRecorder Stopped");
                } catch (Exception e) {
                    // I know this is not good practice handle in Exception but we did -_-;;
                    mAudioRecorderThreadRun = false;
                    Log.e(LOG_TAG, "Exception: " + e.toString());
                } finally {
                    // TODO: do something?
                    recorder.stop();
                    recorder.release();
                }
            }
        });
        mAudioRecorder.start();
    }

    private void startReceiveDataThread() {
        // Create thread for receiving audio data
        mListenerThreadRun = true;
        mPacketListener = new Thread(new Runnable() {
            @Override
            public void run() {
                // Create an instance of AudioTrack, used for playing back audio
                Log.i(LOG_TAG, "mPacketListener Started. Thread id: " + Thread.currentThread().getId());
                final int[] ret = {CALL_TERMINATED_BY_END_CALL};
                DatagramSocket socket = null;
                try {
                    // Setup socket to receive the audio data
                    socket = new DatagramSocket(null);
                    socket.setReuseAddress(true);
                    socket.setSoTimeout(SOCKET_TIME_OUT);
                    socket.bind(new InetSocketAddress(VOIP_DATA_UDP_PORT));

                    while (mListenerThreadRun) {

                        byte[] rtpbuf = new byte[RTP_PACKET_SIZE];
                        DatagramPacket packet = new DatagramPacket(rtpbuf, RTP_PACKET_SIZE);
                        socket.receive(packet);
                        if(packet.getLength() == RTP_PACKET_SIZE){

                            synchronized (mLock) {
                                mJitterBuffer.write(packet.getData());
                                //Log.i(LOG_TAG, "Buffer write");

                            }
//                           Log.i(LOG_TAG, "Packet received: " + packet.getLength());
                        } else
                            Log.i(LOG_TAG, "Invalid Packet LengthReceived: " + packet.getLength());

                    }
                    // close socket
                    socket.disconnect();
                    socket.close();
                    Log.i(LOG_TAG, "mPacketListener Stopped");
                } catch (Exception e) {
                    mListenerThreadRun = false;
                    ret[0] = CALL_TERMINATED_BY_EXCEPTION;
                    Log.e(LOG_TAG, "Exception: " + e.toString());
                    e.printStackTrace();
                } finally {
                    if (!socket.isConnected()) socket.disconnect();
                    if (!socket.isClosed()) socket.close();
                    if (ret[0] == CALL_TERMINATED_BY_EXCEPTION) {
                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                mVoIPAudioListener.onCallTerminated(ret[0]);
                            }
                        };
                        new Thread(r).start();
                    }

                }
            }
        });
        mPacketListener.start();
    }

    private void startAudioOutputThread() {
        mExecutor = Executors.newScheduledThreadPool(5);
        mAudioOutputTask = new AudioOutputTask();
        long delay = (long)SAMPLE_INTERVAL;
        mExecutor.scheduleAtFixedRate(mAudioOutputTask, delay, delay, TimeUnit.MILLISECONDS);
    }

    public void changeAudioOut(int audioOutputTarget) {
        AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        if (audioOutputTarget == Utils.AUDIO_OUT_MODE.SPEAKER) {
            audioManager.setBluetoothScoOn(false);
            audioManager.stopBluetoothSco();
            audioManager.setSpeakerphoneOn(true);
        } else if (audioOutputTarget == Utils.AUDIO_OUT_MODE.EARPIECE) {
            audioManager.setBluetoothScoOn(false);
            audioManager.stopBluetoothSco();
            audioManager.setSpeakerphoneOn(false);
        } else if (audioOutputTarget == Utils.AUDIO_OUT_MODE.BLUETOOTH) {
            audioManager.setSpeakerphoneOn(false);
            audioManager.setBluetoothScoOn(true);
            audioManager.startBluetoothSco();
        }
    }

    public void toggleMicIn(boolean enable) {
        AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        if (enable) {
            audioManager.setMicrophoneMute(false);
        } else {
            audioManager.setMicrophoneMute(true);
        }
    }

    static {
        System.loadLibrary("native-lib");
    }

//    public static native int JniGsmOpen();
//    // Not Used uncomment to enable
//    //public static native int JniGsmDecode(byte encoded[], short lin[]);
//    // Not Used uncomment to enable
//    //public static native int JniGsmEncode(short lin[], byte encoded[]);
//
//    public static native int JniGsmDecodeB(byte encoded[], byte lin[]);
//
//    public static native int JniGsmEncodeB(byte lin[], byte encoded[]);
//
//    public static native void JniGsmClose();
}


