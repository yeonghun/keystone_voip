package com.keystone.voip.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AbsListView;
import android.widget.Toast;
import android.app.AlertDialog;

import java.util.ArrayList;

import com.keystone.voip.R;
import com.keystone.voip.service.MessageService;
import com.keystone.voip.state.CallState;
import com.keystone.voip.state.MessageState;

import static com.keystone.voip.ui.Utils.ACTION.ACT_SEND_MSG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment implements View.OnClickListener {

    private TextView mSendText;
    private Button mSendButton;
    private Button mRecvNumBtn;
    private ListView listView;
    private boolean side = false;
    private View fragView;
    public static ArrayList<ChatMessage> msglist;
    public static MessageAdapter MsgAdapter;
    public static MessageDB helper;
    public static TextView mRecvNum;

    public ChatFragment() {
        // Required empty public constructor
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        fragView =  inflater.inflate(R.layout.fragment_chat, container,false);

        if (fragView == null)
        {
            Log.e(Utils.DEBUG_MSG_LJB, "fragView = null");
            return null;
        }

        mSendText = (TextView) fragView.findViewById(R.id.message);

        mRecvNum = (TextView) fragView.findViewById(R.id.receive_number);
        mRecvNum.setOnClickListener(this);
        mRecvNum.setKeyListener(null);
        mRecvNum.setText("503863");

        mSendButton = (Button) fragView.findViewById(R.id.send_button);
        mSendButton.setOnClickListener(this);

        listView = (ListView) fragView.findViewById(R.id.list_msg);
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setStackFromBottom(true);

        msglist = new ArrayList<ChatMessage>();
        MsgAdapter = new MessageAdapter(getActivity(), msglist);

        listView.setAdapter(MsgAdapter);

        helper = new MessageDB(getActivity(), "savedmsgs.db", null, 1);

        Log.d(Utils.DEBUG_MSG_LJB, "Last sender = " + MessageState.getInstance().getReceiver());

        return fragView;
    }

    @Override
    public void onClick(View v) {

        String msg = mSendText.getText().toString();
        String recvPhoneNum = mRecvNum.getText().toString();

        if (v == null) {
            Log.d(Utils.DEBUG_MSG_LJB, " onClick : v = null");
            return;
        }

        if (v == mSendButton) {

            final ChatMessage chatMessage = new ChatMessage(CallState.getInstance().getLocalPhoneNumber(), recvPhoneNum, msg, Utils.ISMINE.MSG_ISMINE);

            if (recvPhoneNum.length() != 10) {
                Toast.makeText(getActivity(), "Phone number must be 10 digits", Toast.LENGTH_LONG).show();
            } else if (msg.length() == 0) {
                Toast.makeText(getActivity(), "Please enter a message.", Toast.LENGTH_LONG).show();
            } else {
                mSendText.setText("");

                MsgAdapter.add(chatMessage);
                MsgAdapter.notifyDataSetChanged();
                Intent SendMsgIntent = new Intent(getActivity(), MessageService.class);
                SendMsgIntent.putExtra("action", ACT_SEND_MSG);
                SendMsgIntent.putExtra(Utils.MSG_TO_PHONE_NUM, recvPhoneNum);
                SendMsgIntent.putExtra(Utils.MESSAGE, msg);
                Log.d(Utils.DEBUG_MSG_LJB, "Message = " + msg);
                this.getActivity().startService(SendMsgIntent);

                boolean results = helper.insertMsg(CallState.getInstance().getLocalPhoneNumber(), recvPhoneNum, msg, Utils.ISMINE.MSG_ISMINE);

                Log.d(Utils.DEBUG_MSG_LJB, " insert result = " + results);
            }
        }
        else if (v == mRecvNumBtn) {
            /*
            ArrayList<ChatMessage> cMessage = helper.getSeletedReceivermsg(mRecvNum.getText().toString());

            for (int index = 0; index < cMessage.size(); index++) {
                Log.d(Utils.DEBUG_MSG_LJB,  "msg = " + cMessage.get(index).msg + ", isMind = " + cMessage.get(index).isMine);
                MsgAdapter.add(cMessage.get(index));
                MsgAdapter.notifyDataSetChanged();
            }
            */
        }

        else if (v == mRecvNum) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

            alert.setTitle(" Enter recipient number");
            alert.setMessage(" enter a 10-digit number without \"-\"");

            final EditText inputPN = new EditText(getActivity());
            inputPN.setText("503863");
            inputPN.setSelection(inputPN.getText().length());
            alert.setView(inputPN);

            alert.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String pn = inputPN.getText().toString();

                    if (pn.length() != 10) {
                        Toast.makeText(getActivity(), "Phone number must be 10 digits", Toast.LENGTH_LONG).show();
                    }
                    else {

                        String pastPN = mRecvNum.getText().toString();

                        if (pastPN != pn) {
                            MsgAdapter.chatMessageList.clear();
                            MsgAdapter.notifyDataSetChanged();
                            mRecvNum.setText(pn);
                            ArrayList<ChatMessage> cMessage = helper.getSeletedReceivermsg(pn);

                            for (int index = 0; index < cMessage.size(); index++) {
                                MsgAdapter.add(cMessage.get(index));
                                MsgAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            });

            alert.setNegativeButton("no",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                }
            });

            alert.show();
        }
    }


}