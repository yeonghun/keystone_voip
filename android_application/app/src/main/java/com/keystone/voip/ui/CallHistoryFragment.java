package com.keystone.voip.ui;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.keystone.voip.R;
import com.keystone.voip.account.Account;
import com.keystone.voip.config.ConfigStore;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CallHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CallHistoryFragment extends BaseFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private final String TAG = "CallHistoryFragment";


    ArrayList<CallInfo> mCallLogList = null;

    CallLogRecyclerViewAdapter mRecycleAdapter = null;
    RecyclerView mRecyclerView = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public class CallType {
            static public final int INCOMING = 0;
            static public final int OUTGOING = 1;
            static public final int MISSED = 2;
    }
    public class CallInfo {
        String name;
        String time;
        int callType;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public int getCallType() {
            return callType;
        }

        public void setCallType(int callType) {
            this.callType = callType;
        }

        public CallInfo(String name, String time, int callType) {

            this.name = name;
            this.time = time;
            this.callType = callType;
        }
    }

    public CallHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CallHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CallHistoryFragment newInstance(String param1, String param2) {
        CallHistoryFragment fragment = new CallHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.fragment_call_history, container, false);;
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }

        mCallLogList = new ArrayList<CallInfo>();
        /*CallInfo c = new CallInfo("Kaushik", "8:30 PM", 0);
        CallInfo c1 = new CallInfo("GUN", "8:20 PM", 1);
        CallInfo c2 = new CallInfo("Yeonghyun", "4:30 PM", 2);
        mCallLogList.add(c);
        mCallLogList.add(c1);
        mCallLogList.add(c2);*/

        mRecyclerView = fragView.findViewById(R.id.callloglist);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mRecycleAdapter = new CallLogRecyclerViewAdapter(this.getContext(), mCallLogList);
        mRecyclerView.setAdapter(mRecycleAdapter);

        return fragView;
    }
    @Override
    public void onResume() {

        super.onResume();
        new CallLogTask().execute();
    }
    @Override
    public void onClick(View v) {

    }

    private class CallLogTask extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(CallHistoryFragment.this.getContext());
        private final String FAIL = "fail";
        private final String PASS = "pass";
        ArrayList<CallInfo> mTempCallLogList = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pdLoading != null) {
                pdLoading.setMessage(getResources().getString(R.string.processing_msg));
                pdLoading.show();
            }
        }
        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;
            try {

                URL serverURL = new URL(  ConfigStore.Server.getAddress(getContext()) +
                        "/bill/callhistory");
                HttpURLConnection myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");

                //Build Json for Login data
                JSONObject jsonCallData = new JSONObject();

                jsonCallData.put(Utils.PHONENUMBER, Account.getInstance().getPhoneNumber());
                jsonCallData.put(Utils.PASSWORD, Account.getInstance().getPassword());
                jsonCallData.put(Utils.IP, Utils.getLocalIP(CallHistoryFragment.this.getContext()));
                jsonCallData.put(Utils.REGID, FirebaseInstanceId.getInstance().getToken());

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonCallData.toString().getBytes());

                InputStream responseBody;
                String responseString = "";
                String number = "";
                String timeStamp ="";
                String callStatus = "";

                //
                String name = "";
                String time = "";
                int cType = -1;


                JSONObject jsonObjReader = null;
                JSONArray jsonArrayReader = null;
                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    DBHelper db = new DBHelper(CallHistoryFragment.this.getContext());
                    mTempCallLogList = new ArrayList<CallInfo>();
                    result = PASS;
                    responseBody = myConnection.getInputStream();
                    responseString = Utils.getStringFromInputStream(responseBody);
                    jsonArrayReader = new JSONArray(responseString);

                    for(int i=0; i < jsonArrayReader.length(); i++){

                        jsonObjReader = jsonArrayReader.getJSONObject(i);
                        timeStamp = jsonObjReader.getString("createTime");
                        callStatus = jsonObjReader.getString("status");
                        if (callStatus.equalsIgnoreCase("INCOMING") || callStatus.equalsIgnoreCase("MISSED")) {
                            number = jsonObjReader.getString("fromPhoneNum");
                        } else {
                            number = jsonObjReader.getString("toPhoneNum");
                        }

                       // "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                        Date nDate = simpleDateformat.parse(timeStamp);

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(nDate);

                        CallInfo c = new CallInfo(getNameFromNumber(number,db), calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE) ,getCallType(callStatus));
                        mTempCallLogList.add(c);
                    }
                } else {
                    result = FAIL;
                }

            } catch (Exception e){
                result = FAIL;
                e.printStackTrace();
            }
            return result;
        }
        private int getCallType (String callStatus) {
            if (callStatus == null ||callStatus.isEmpty()) {
                return -1;
            }
            if (callStatus.equalsIgnoreCase("INCOMING")) {
                return CallType.INCOMING;
            } else if (callStatus.equalsIgnoreCase("OUTGOING"))  {
                return CallType.OUTGOING;
            } else if (callStatus.equalsIgnoreCase("MISSED")) {
                return CallType.MISSED;
            }
            return -1;
        }
        private String getNameFromNumber (String number , DBHelper db) {
            String name = "";
            if (db == null || number ==  null || number.isEmpty())
            {
                return number;
            } else {
                name =  db.getNameFromNumber(number);
                if (name.isEmpty() == false) {
                    return name;
                }
            }
            return number;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pdLoading != null) {
                pdLoading.dismiss();
            }
            if (result.equals(FAIL))
            {
                Toast.makeText(CallHistoryFragment.this.getContext(),"Fetching Call History Failed, Try Later...", Toast.LENGTH_LONG).show();
                return;
            } else {
                mCallLogList.clear();
               for (int i = 0; i < mTempCallLogList.size(); i++) {
                   mCallLogList.add(mTempCallLogList.get(i));
               }
                mRecycleAdapter.notifyDataSetChanged();
                mTempCallLogList.clear();
            }
        }

    }
}
