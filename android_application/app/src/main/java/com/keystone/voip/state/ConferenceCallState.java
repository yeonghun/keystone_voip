package com.keystone.voip.state;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class ConferenceCallState extends Observable {
    private static final String TAG = "ConCallState";
    private static ConferenceCallState sConferenceCallStateInstance;

    private String callId;
    private String datetime;
    private List<String> attendeeNumList; // it will be valuable only in initiator
    private List<String> attendeeIpList;

    public enum CcState {
        IDLE,
        INCALL,
        END
    };

    public CcState getCcState() {
        return mCcState;
    }

    public void setCcState(CcState mCcState) {
        this.mCcState = mCcState;
    }

    private CcState mCcState = CcState.IDLE;

    public static final ConferenceCallState getInstance() {
        if (sConferenceCallStateInstance == null) {
            sConferenceCallStateInstance = new ConferenceCallState();
        }
        return sConferenceCallStateInstance;
    }

    private ConferenceCallState() {
        callId = "";
        attendeeNumList = new ArrayList<>();
        attendeeIpList = new ArrayList<>();
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public List<String> getAttendeeNumList() {
        return attendeeNumList;
    }

    public void addAttendeeNumber(String attendeeNum) {
        this.attendeeNumList.add(attendeeNum);
    }

    public List<String> getAttendeeIpList() {
        return attendeeIpList;
    }

    public void addAttendeeIp(String remoteIp) {
        this.attendeeIpList.add(remoteIp);
    }
    public void removeAttendeeIp(String remoteIp) { this.attendeeIpList.remove(remoteIp); }
    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void clearAllState() {
        Log.d(TAG, "clearAllState~!");
        callId = "";
        mCcState = CcState.IDLE;
        attendeeNumList.clear();
        attendeeIpList.clear();
    }

    public String dump() {
        StringBuilder sb = new StringBuilder();
        sb.append("state: " + mCcState);
        sb.append("\n");
        sb.append("callId: " + callId);
        sb.append("\n");
        sb.append("datetime " + datetime);
        sb.append("\n");
        sb.append("attendees: " + attendeeNumList.toString());
        sb.append("\n");
        sb.append("attendees Ip: " + attendeeIpList.toString());
        return sb.toString();
    }

    public void notifyUpdate() {
        setChanged();
        notifyObservers();
    }
}
