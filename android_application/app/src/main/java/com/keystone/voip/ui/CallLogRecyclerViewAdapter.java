package com.keystone.voip.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.keystone.voip.R;

import java.util.List;

public class CallLogRecyclerViewAdapter extends RecyclerView.Adapter<CallLogRecyclerViewAdapter.ViewHolder> {
    private List<CallHistoryFragment.CallInfo> mData;
    private LayoutInflater mInflater;
    private Context mContext;


        // data is passed into the constructor
        CallLogRecyclerViewAdapter(Context context, List<CallHistoryFragment.CallInfo> data) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.callloglist_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CallHistoryFragment.CallInfo callInfo = mData.get(position);
        holder.nameTv.setText(callInfo.getName());
        holder.timeTv.setText(callInfo.getTime());


        if (callInfo.callType == CallHistoryFragment.CallType.INCOMING) {
            holder.callImage.setBackgroundResource(R.drawable.img_incoming);
        } else if (callInfo.callType == CallHistoryFragment.CallType.OUTGOING) {
            holder.callImage.setBackgroundResource(R.drawable.img_outgoing);
        } else if (callInfo.callType == CallHistoryFragment.CallType.MISSED) {
            holder.callImage.setBackgroundResource(R.drawable.img_missed);
        }

   }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameTv;
        ImageView callImage;
        TextView timeTv;

        ViewHolder(View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.nameView);
            callImage = itemView.findViewById(R.id.imageView);
            timeTv = itemView.findViewById(R.id.timeview);
        }
    }
}
