package com.keystone.voip.ui;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.keystone.voip.R;
import com.keystone.voip.account.Account;
import com.keystone.voip.config.ConfigStore;
import com.keystone.voip.state.CallState;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateAccountFragment extends BaseFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private final String TAG = "CreateAccountFragment";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextInputEditText mFirstName_Etv;
    private TextInputEditText mLastName_Etv;
    private TextInputEditText mEmail_Etv;
    private TextInputEditText mPassword_Etv;
    private TextInputEditText mConfirmPassword_Etv;
    private TextInputEditText mAddress_Etv;
    private TextInputEditText mCardInfo_Etv;

    private Button mCreate_Btn;



    public CreateAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateAccountFragment newInstance(String param1, String param2) {
        CreateAccountFragment fragment = new CreateAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.create_account_fragment, container, false);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
         mFirstName_Etv = (TextInputEditText) fragView.findViewById(R.id.firstname_edittv);
         mLastName_Etv = (TextInputEditText) fragView.findViewById(R.id.lastname_edittv);
         mEmail_Etv = (TextInputEditText) fragView.findViewById(R.id.id_edittv);
         mPassword_Etv = (TextInputEditText) fragView.findViewById(R.id.input_pwd_edittv);
         mConfirmPassword_Etv = (TextInputEditText) fragView.findViewById(R.id.cnf_pwd_edittv);
         mAddress_Etv = (TextInputEditText) fragView.findViewById(R.id.address_edittv);
         mCardInfo_Etv = (TextInputEditText) fragView.findViewById(R.id.cardinfo_edittv);

        mCreate_Btn = (Button) fragView.findViewById(R.id.create_btn);
        mCreate_Btn.setOnClickListener(this);

        return fragView;
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            Log.e(TAG, " onClick : v = null");
            return;
        }
        if (v == mCreate_Btn) {

           if (validateInputFields() == false) {
             return;
            }
            new CreateUserTask().execute();
        }
    }
    private boolean validateInputFields() {
        if ((mFirstName_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.first_name), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mLastName_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.last_name), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mEmail_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.email), Toast.LENGTH_LONG).show();
            return false;
        } else {
            if (Utils.isEmailValid(mEmail_Etv.getText().toString()) == false) {
                Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_email), Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if ((mPassword_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.password), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mConfirmPassword_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.confirm_password), Toast.LENGTH_LONG).show();
            return false;
        }
        if (mPassword_Etv.getText().toString().equals(mConfirmPassword_Etv.getText().toString()) == false)
        {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_password), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private class CreateUserTask extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(CreateAccountFragment.this.getContext());
        private final String FAIL = "fail";
        private final String PASS = "pass";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pdLoading != null) {
                pdLoading.setMessage(getResources().getString(R.string.processing_msg));
                pdLoading.show();
            }
        }
        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;
            try {

                URL serverURL = new URL(ConfigStore.Server.getAddress(getContext()) + "/user/create");
                HttpURLConnection myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");


                //Build Json for USER data
                JSONObject jsonUserData = new JSONObject();
                jsonUserData.put(Utils.FIRST_NAME, mFirstName_Etv.getText().toString());
                jsonUserData.put(Utils.LAST_NAME, mLastName_Etv.getText().toString());
                jsonUserData.put(Utils.EMAIL, mEmail_Etv.getText().toString());
                jsonUserData.put(Utils.PASSWORD, mPassword_Etv.getText().toString());
                jsonUserData.put(Utils.IP, Utils.getLocalIP(CreateAccountFragment.this.getContext()));
                jsonUserData.put(Utils.REGID, FirebaseInstanceId.getInstance().getToken());

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonUserData.toString().getBytes());  // Write the data

                InputStream responseBody;
                String responseString = "";
                String phoneNumberString = "";
                JSONObject jsonReader = null;

                int responseCode = myConnection.getResponseCode();
                Account newAccount = Account.getInstance();
                if (responseCode == 200) {
                    responseBody = myConnection.getInputStream();
                    responseString = Utils.getStringFromInputStream(responseBody);
                    jsonReader = new JSONObject(responseString);
                    phoneNumberString = jsonReader.getString(Utils.PHONENUMBER);
                    newAccount.setFirstName(mFirstName_Etv.getText().toString());
                    newAccount.setLastName(mLastName_Etv.getText().toString());
                    newAccount.setEmailID(mEmail_Etv.getText().toString());
                    newAccount.setPassword(mPassword_Etv.getText().toString());
                    newAccount.setLocalIP(Utils.getLocalIP(CreateAccountFragment.this.getContext()));
                    newAccount.setPhoneNumber(phoneNumberString);
                    newAccount.saveAccount(CreateAccountFragment.this.getContext());

                    CallState.getInstance().setLocalPhoneNumber(phoneNumberString);
                    CallState.getInstance().setLocalIP(Utils.getLocalIP(CreateAccountFragment.this.getContext()));
                    //Temp
                    Utils.saveDataToPreference(CreateAccountFragment.this.getContext(),Utils.PREF_KEY_PHONENUMBER,phoneNumberString);
                    result = PASS;
                }

            } catch (Exception e){
                result = FAIL;
                e.printStackTrace();

            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           if (pdLoading != null) {
               pdLoading.dismiss();
           }
           if (result.equals(FAIL))
           {
               Toast.makeText(CreateAccountFragment.this.getContext(),getResources().getString(R.string.account_create_fail), Toast.LENGTH_LONG).show();
           } else {
               Toast.makeText(CreateAccountFragment.this.getContext(),getResources().getString(R.string.account_create_pass), Toast.LENGTH_LONG).show();
               if (mListener == null) {
                   Log.e(TAG, " CreateUserTask onPostExecute : mListener = null");
                   return;
               }
               mListener.onActionPerformed(Utils.Fragment.CREATE_ACCOUNT,Utils.ACTION.ACT_LOGIN_SUCCESS,new Bundle());
           }

        }

    }
}

