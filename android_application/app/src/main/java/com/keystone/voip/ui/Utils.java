package com.keystone.voip.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.regex.Pattern;

import static android.content.Context.WIFI_SERVICE;

public class Utils {

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String PHONENUMBER = "phoneNum";
    public static final String ADDRESS = "address";
    public static final String CARDINFO = "cardInfo";
    public static final String IP = "ip";
    public static final String REGID = "regId";
    public static final String FROM_PHONE_NUMBER = "fromPhoneNum";
    public static final String TO_PHONE_NUMBER = "toPhoneNum";
    public static final String CALL_ID = "callId";
    public static final String CALLEE_IP = "toIp";
    public static final String CALLER_IP = "fromIp";
    public static final String CONTACT_NAME = "contactName";

    public static final String CC_INITIATOR = "initiator";
    public static final String CC_ID = "conferenceId";
    public static final String CC_DATE_TIME = "planTime";
    public static final String CC_ATTENDANTS = "attendants";
    public static final String CC_MY_IP = "con";
    public static final String CC_READY_ATTENDEE_IP = "ip";

    // Message
    public static final String MSG_ID = "msgId";
    public static final String MSG_FROM_PHONE_NUM = "fromPhoneNum";
    public static final String MSG_TO_PHONE_NUM = "toPhoneNum";
    public static final String MESSAGE = "msg";


    public static final String SETTINGS_SHARED_PREFERENCES_FILE_NAME = "settingpreference";
    public static final String PREF_KEY_PHONENUMBER = "key_phonenumber";
    public static final String PREF_KEY_IPADDRESS = "key_ipaddress";
    public static final String PREF_KEY_CALLID = "key_callid";
    public static final String PREF_KEY_CALLER_IPADDRESS = "key_caller_ipaddress";

    // LOG
    public static final String DEBUG_MSG_LJB = "JB LEE";

    public class Fragment {

        static public final int LOGIN = 0;
        static public final int CREATE_ACCOUNT = 1;
        static public final int FORGOT_PASSWORD = 2;
        static public final int CONTACTS = 3;
    }
    public class ServerMethods {
        public static final String LOGIN_CMD = "/user/login";
        public static final String CREATE_USER_CMD = "/user/create";
    }
    public class MSG {
        static public final int MSG_XXX = 100;
        static public final int MSG_YYY = 101;
    }

    public class ISMINE {
        static public final int MSG_ISMINE = 0;
        static public final int MSG_ISNOTMINE = 1;
    }

    public static class ACTION {
        static public final int ACT_LOGIN_SUCCESS= 200;
        static public final int ACT_REQUEST_CALL = 201; // caller request a call
        static public final int ACT_REQ_PORT_OPEN = 202; // callee receive a call
        static public final int ACT_ACCEPT_CALL = 203; // callee accept a call
        static public final int ACT_REJECT_CALL = 204; // callee reject a call
        static public final int ACT_PEER_READY = 205; // caller receive a peer ready
        static public final int ACT_END_CALL = 206;
        static public final int ACT_RESET_PWD_SUCCESS= 207;
        static public final int ACT_REJECT_CALL_RECEIVED = 208;
//        static public final int ACT_PEER_READY = 206;

        static public final int ACT_SEND_MSG = 300;
        static public final int ACT_RECV_MSG = 301;

        public static final int ACT_CC_CREATE = 400;
        public static final int ACT_CC_SETUP = 401;
        public static final int ACT_CC_START = 402;
        public static final int ACT_CC_END = 403;
        public static final int ACT_CC_CHANGE = 404;

        public static final int ACT_CHANGE_AUDIO_MODE = 500;
        public static final int ACT_CHANGE_AUDIO_MODE_CONFCALL = 501;

        public static String toString(int code) {
            switch (code) {
                case ACT_LOGIN_SUCCESS: return "ACT_LOGIN_SUCCESS";
                case ACT_REQUEST_CALL: return "ACT_REQUEST_CALL";
                case ACT_REQ_PORT_OPEN: return "ACT_REQ_PORT_OPEN";
                case ACT_ACCEPT_CALL: return "ACT_ACCEPT_CALL";
                case ACT_REJECT_CALL: return "ACT_REJECT_CALL";
                case ACT_PEER_READY: return "ACT_PEER_READY";
                case ACT_END_CALL: return "ACT_END_CALL";
                case ACT_RESET_PWD_SUCCESS: return "ACT_RESET_PWD_SUCCESS";
                case ACT_REJECT_CALL_RECEIVED: return "ACT_REJECT_CALL_RECEIVED";
                case ACT_SEND_MSG: return "ACT_SEND_MSG";
                case ACT_RECV_MSG: return "ACT_RECV_MSG";
                case ACT_CC_CREATE: return "ACT_CC_CREATE";
                case ACT_CC_SETUP: return "ACT_CC_SETUP";
                case ACT_CC_START: return "ACT_CC_START";
                case ACT_CC_END: return "ACT_CC_END";
                case ACT_CC_CHANGE: return "ACT_CC_CHANGE";
                case ACT_CHANGE_AUDIO_MODE: return "ACT_CHANGE_AUDIO_MODE";
                default: return String.valueOf(code);
            }
        }
    }

    public static class AUDIO_OUT_MODE {
        public static final int EARPIECE = 100;
        public static final int SPEAKER = 200;
        public static final int BLUETOOTH = 300;
    }

    public static boolean isEmailValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern emailPattern = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return emailPattern.matcher(email).matches();
    }

    public static  String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    public static String getLocalIP(Context context) {
        if (context == null) {
            return "";
        }
        String  localIp = "";
        WifiManager wifiMan = (WifiManager) context.getSystemService(WIFI_SERVICE);
        if (wifiMan != null) {
            WifiInfo wifiInfo = wifiMan.getConnectionInfo();
            int ip = wifiInfo.getIpAddress();
            return String.format(Locale.US, "%d.%d.%d.%d", (ip & 0xff), (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
        }
        return "";
    }


    public static void saveDataToPreference(Context context, String key, String value) {
        SharedPreferences pref = context.getSharedPreferences("KeyStonePreference", 0);
        SharedPreferences.Editor prefsEditor = pref.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public static String getDataFromPreference(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences("KeyStonePreference", 0);
        if (pref != null) {
            return pref.getString(key, "");
        }
        return "";
    }
    public static void setSettingsPreference(Context context, String key, boolean value) {
        SharedPreferences pref = context.getSharedPreferences(Utils.SETTINGS_SHARED_PREFERENCES_FILE_NAME, 0);
        SharedPreferences.Editor prefsEditor = pref.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.commit();
    }

    public static boolean getSettingsPreference(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences(Utils.SETTINGS_SHARED_PREFERENCES_FILE_NAME, 0);
        if (pref != null) {
            return pref.getBoolean(key,true);

        }
        return true;
    }
}
