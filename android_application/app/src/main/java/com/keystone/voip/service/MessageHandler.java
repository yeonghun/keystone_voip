package com.keystone.voip.service;

import android.content.Context;
import android.util.Log;
import android.os.AsyncTask;
import android.widget.ListView;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.keystone.voip.config.ConfigStore;
import com.keystone.voip.state.CallState;
import com.keystone.voip.ui.ChatFragment;
import com.keystone.voip.ui.MessageAdapter;
import com.keystone.voip.ui.Utils;
import com.keystone.voip.state.MessageState;

public class MessageHandler {
    private Context mContext;
    private MessageListner mListener;
    private MessageAdapter MsgAdapter;
    private ListView listView;

    public MessageHandler(Context context) {
        this(context, null);
    }

    public MessageHandler(Context context, MessageListner listener) {
        mContext = context;
        mListener = listener;
    }

    public void setListener(MessageListner listener) {
        this.mListener = mListener;
    }

    public void sendMessage(String receiver, String msg) {
        // send msg to server via thread
        new SendMsgTask().execute(receiver, msg);
    }

    public void recvMessage(String receiver, String msg, String msgId) {
        new RecvMsgTask().execute(receiver, msgId);
    }

    private class SendMsgTask extends AsyncTask<String, String, String> {
        private final String SUCCESS = "success";
        private final String INVALID = "invalid";
        private final String USER_NOT_FOUND = "notfound";
        private final String FAIL = "fail";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;

            try {
                URL serverURL = new URL(ConfigStore.Server.getAddress(mContext) +
                        "/message/request");
                HttpURLConnection myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");

                //Build Json for USER data
                JSONObject jsonUserData = new JSONObject();
                jsonUserData.put(Utils.MSG_FROM_PHONE_NUM, CallState.getInstance().getLocalPhoneNumber());
                jsonUserData.put(Utils.MSG_TO_PHONE_NUM, params[0]);
                jsonUserData.put(Utils.MESSAGE, params[1]);

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonUserData.toString().getBytes());  // Write the data

                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    result = SUCCESS;
                } else if (responseCode == 400) {
                    result = INVALID;
                } else if (responseCode == 404) {
                    result = USER_NOT_FOUND;
                }

            } catch (Exception e) {
                result = FAIL;
                e.printStackTrace();

            }

            Log.d(Utils.DEBUG_MSG_LJB, " Send Message Result =  " + result);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mListener.onMessageSent(result);

        }
    }

    private class RecvMsgTask extends AsyncTask<String, String, String> {
        private final String SUCCESS = "success";
        private final String FAIL = "fail";
        private String sender = "";
        private String receiver = "";
        private String msg = "";
        private String msgId = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;

            try {
                URL serverURL = new URL(ConfigStore.Server.getAddress(mContext) +
                        "/message/accept");
                HttpURLConnection myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");

                //Build Json for USER data
                JSONObject jsonUserData = new JSONObject();
                jsonUserData.put(Utils.MSG_ID, params[1]);

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonUserData.toString().getBytes());  // Write the data

                InputStream responseBody;
                String responseString = "";
                JSONObject jsonReader = null;

                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    result = SUCCESS;

                    responseBody = myConnection.getInputStream();
                    responseString = Utils.getStringFromInputStream(responseBody);

                    jsonReader = new JSONObject(responseString);
                    sender = jsonReader.getString(Utils.MSG_FROM_PHONE_NUM);
                    msg = jsonReader.getString(Utils.MESSAGE);
                    msgId = jsonReader.getString(Utils.MSG_ID);
                }

                Log.d(Utils.DEBUG_MSG_LJB, " Sender      =  " + sender);
                Log.d(Utils.DEBUG_MSG_LJB, " msg ID      =  " + msgId);
                Log.d(Utils.DEBUG_MSG_LJB, " msg         =  " + msg);

            } catch (Exception e) {
                result = FAIL;
                e.printStackTrace();

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(!MessageState.getInstance().isDuplicated(msgId)) {
                mListener.onMessageReceived(sender, receiver, msg);
                MessageState.getInstance().addReceiveMsg(msgId);
            }
            else
            {
                Log.d(Utils.DEBUG_MSG_LJB, "Duplicated Msg : " + msgId);
            }
        }
    }
}
