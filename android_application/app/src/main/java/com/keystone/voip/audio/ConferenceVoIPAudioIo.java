package com.keystone.voip.audio;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Process;
import android.util.Log;

import com.keystone.voip.R;
import com.keystone.voip.state.ConferenceCallState;
import com.keystone.voip.ui.Utils;

import java.io.InputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.keystone.voip.audio.VoIPAudioListener.CALL_TERMINATED_BY_END_CALL;
import static com.keystone.voip.audio.VoIPAudioListener.CALL_TERMINATED_BY_EXCEPTION;

public class ConferenceVoIPAudioIo {

    private static final String LOG_TAG = "VoIPAudioIo";
    private static final int MILLISECONDS_IN_A_SECOND = 1000;
    private static final int SAMPLE_RATE = 8000; // Hertz
    private static final int SAMPLE_INTERVAL = 20;   // Milliseconds
    private static final int BYTES_PER_SAMPLE = 2;    // Bytes Per Sampl;e
    private static final int RAW_BUFFER_SIZE = SAMPLE_RATE / (MILLISECONDS_IN_A_SECOND / SAMPLE_INTERVAL) * BYTES_PER_SAMPLE;
    private static final int GSM_BUFFER_SIZE = 33;
    private static final int VOIP_DATA_UDP_PORT = 5124;
    private static final int SOCKET_TIME_OUT = 10 * 1000;

//    private ConcurrentLinkedQueue<byte[]> mIncommingpacketQueue;
    private Context mContext;
    private Map<String, ConcurrentLinkedQueue<byte[]>> mIncomingPacketMap;
    private List<InetAddress> mRemoteAddress;
    private Thread mAudioRecorder;
    private Thread mPacketListener;
    private VoIPAudioListener mVoIPAudioListener;
    private ScheduledExecutorService mExecutor;
    private Object mLock = new Object();

    private int mSimVoice;
    private boolean mIsRunning = false;
    private boolean mAudioRecorderThreadRun = false;
    private boolean mListenerThreadRun = false;

    private class AudioOutputTask implements Runnable {
        final AudioTrack outputTrack;
        final String address;

        AudioOutputTask(String address) {
            outputTrack = new AudioTrack.Builder()
                    .setAudioAttributes(new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                            .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                            //	.setFlags(AudioAttributes.FLAG_LOW_LATENCY) //This is Nougat+ only (API 25) comment if you have lower
                            .build())
                    .setAudioFormat(new AudioFormat.Builder()
                            .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                            .setSampleRate(SAMPLE_RATE)
                            .setChannelMask(AudioFormat.CHANNEL_OUT_MONO).build())
                    .setBufferSizeInBytes(RAW_BUFFER_SIZE)
                    .setTransferMode(AudioTrack.MODE_STREAM)
                    //.setPerformanceMode(AudioTrack.PERFORMANCE_MODE_LOW_LATENCY) //Not until Api 26
                    //.setSessionId(recorder.getAudioSessionId()
                    .build();
            outputTrack.play();
            this.address = address;
        }

        @Override
        public void run() {
            Log.d(LOG_TAG, "AudioOutputTask, run: " + address);
            ConcurrentLinkedQueue<byte[]> queue = mIncomingPacketMap.get(address);
            synchronized (queue) {
                if (queue.size() > 0) {
                    byte[] buffer = queue.poll();
                    outputTrack.write(buffer, 0, RAW_BUFFER_SIZE);
                    mIncomingPacketMap.put(address, queue);
                }
            }
        }

        public void end() {
            outputTrack.stop();
            outputTrack.flush();
            outputTrack.release();
        }
    }

    public ConferenceVoIPAudioIo(Context context, VoIPAudioListener listener) {
        mContext = context;
        mVoIPAudioListener = listener;
        mRemoteAddress = new ArrayList<>();
        mIncomingPacketMap = new HashMap<>();
    }

    public synchronized boolean startAudio(int simVoice) {
        Log.i(LOG_TAG, "startAudio~!" + mIsRunning);
        if (mIsRunning) return (true);
        if (AudioNative.JniGsmOpen() == 0)
            Log.i(LOG_TAG, "JniGsmOpen() Success");
//        mIncommingpacketQueue = new ConcurrentLinkedQueue<>();
        mSimVoice = simVoice;
        startAudioRecorderThread();
        startReceiveDataThread();
        startAudioOutputThread();
        mIsRunning = true;
        return (false);
    }

    public synchronized boolean endAudio() {
        Log.i(LOG_TAG, "endAudio~! mIsRunning: " + mIsRunning
                + ", mPacketListener: " + mPacketListener + ", mAudioRecorder: " + mAudioRecorder);
        if (!mIsRunning) return (true);
        Log.i(LOG_TAG, "Ending VoIP Audio");
        // stop audio output
        if (!mExecutor.isShutdown()) {
            mExecutor.shutdown(); // stop periodic execution
        }
        // stop packet listener
        if (mPacketListener != null && mPacketListener.isAlive()) {
            mListenerThreadRun = false;
            Log.i(LOG_TAG, "mPacketListener Join started");
            try {
                mPacketListener.join();
            } catch (InterruptedException e) {
                Log.i(LOG_TAG, "mPacketListener Join interrupted");
            }
            Log.i(LOG_TAG, "mPacketListener Join success");
        }
        // stop audio recorder
        if (mAudioRecorder != null && mAudioRecorder.isAlive()) {
            mAudioRecorderThreadRun = false;
            Log.i(LOG_TAG, "mAudioRecorder Join started");

            try {
                mAudioRecorder.join();
            } catch (InterruptedException e) {
                Log.i(LOG_TAG, "mAudioRecorder Join interrupted");
            }
            Log.i(LOG_TAG, "mAudioRecorder Join success");
        }
        mAudioRecorder = null;
        mPacketListener = null;
        mExecutor.shutdown();
        mExecutor = null;
        mIncomingPacketMap.clear();
        mRemoteAddress.clear();
        AudioNative.JniGsmClose();
        mIsRunning = false;
        return (false);
    }

    public void addRemoteAddress(InetAddress address) {
        mRemoteAddress.add(address);
        // queue per address wil be created in receiver socket
//        mIncomingPacketMap.put(address.toString(), new ConcurrentLinkedQueue());
    }

    private InputStream OpenSimVoice(int SimVoice) {
        InputStream VoiceFile = null;
        switch (SimVoice) {
            case 0:
                break;
            case 1:
                VoiceFile = mContext.getResources().openRawResource(R.raw.t18k16bit);
                break;
            case 2:
                VoiceFile = mContext.getResources().openRawResource(R.raw.t28k16bit);
                break;
            case 3:
                VoiceFile = mContext.getResources().openRawResource(R.raw.t38k16bit);
                break;
            case 4:
                VoiceFile = mContext.getResources().openRawResource(R.raw.t48k16bit);
                break;
            default:
                break;
        }
        return VoiceFile;
    }

    private void startAudioRecorderThread() {
        // Creates the thread for capturing and transmitting audio
        mAudioRecorderThreadRun = true;
        mAudioRecorder = new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO);
                AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
                int prevAudioMode = 0;
                if (audioManager != null) {
                    prevAudioMode = audioManager.getMode();
                    audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION); //Enable AEC
                }

                // Create an instance of the AudioRecord class
                Log.i(LOG_TAG, "mAudioRecorder started. Thread id: " + Thread.currentThread().getId());
                InputStream inputPlayFile = OpenSimVoice(mSimVoice);
                AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE,
                        AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT,
                        AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT));

                int bytesRead;
                byte[] rawbuf = new byte[RAW_BUFFER_SIZE];
                byte[] gsmbuf = new byte[GSM_BUFFER_SIZE];
                try {
                    // Create a socket and start recording
                    DatagramSocket socket = new DatagramSocket();
                    socket.setSoTimeout(SOCKET_TIME_OUT);
                    recorder.startRecording();
                    while (mAudioRecorderThreadRun) {
                        // Capture audio from microphone and send
                        bytesRead = recorder.read(rawbuf, 0, RAW_BUFFER_SIZE);
                        if (inputPlayFile != null) {
                            bytesRead = inputPlayFile.read(rawbuf, 0, RAW_BUFFER_SIZE);
                            if (bytesRead != RAW_BUFFER_SIZE) {
                                inputPlayFile.close();
                                inputPlayFile = OpenSimVoice(mSimVoice);
                                bytesRead = inputPlayFile.read(rawbuf, 0, RAW_BUFFER_SIZE);
                            }
                        }
                        if (bytesRead == RAW_BUFFER_SIZE) {
                            AudioNative.JniGsmEncodeB(rawbuf, gsmbuf);
                            for (InetAddress address : mRemoteAddress) {
                                Log.d(LOG_TAG, "mAudioRecorder send to " + address.toString());
                                DatagramPacket packet = new DatagramPacket(gsmbuf, GSM_BUFFER_SIZE, address, VOIP_DATA_UDP_PORT);
                                socket.send(packet);
                            }
                        }
                    }
                    // Stop Audio Thread);
                    recorder.stop();
                    recorder.release();
                    socket.disconnect();
                    socket.close();
                    if (inputPlayFile != null) inputPlayFile.close();
                    if (audioManager != null) audioManager.setMode(prevAudioMode);
                    Log.i(LOG_TAG, "mAudioRecorder Stopped");
                } catch (Exception e) {
                    // I know this is not good practice handle in Exception but we did -_-;;
                    mAudioRecorderThreadRun = false;
                    Log.e(LOG_TAG, "Exception: " + e.toString());
                } finally {
                    // TODO: do something?
                }
            }
        });
        mAudioRecorder.start();
    }

    private void startReceiveDataThread() {
        // Create thread for receiving audio data
        mListenerThreadRun = true;
        mPacketListener = new Thread(new Runnable() {
            @Override
            public void run() {
                // Create an instance of AudioTrack, used for playing back audio
                Log.i(LOG_TAG, "mPacketListener Started. Thread id: " + Thread.currentThread().getId());
                final int[] ret = {CALL_TERMINATED_BY_END_CALL};
                DatagramSocket socket = null;
                try {
                    // Setup socket to receive the audio data
                    socket = new DatagramSocket(null);
                    socket.setReuseAddress(true);
                    socket.setSoTimeout(SOCKET_TIME_OUT);
                    socket.bind(new InetSocketAddress(VOIP_DATA_UDP_PORT));

                    while (mListenerThreadRun) {
                        byte[] rawbuf = new byte[RAW_BUFFER_SIZE];
                        byte[] gsmbuf = new byte[GSM_BUFFER_SIZE];
                        DatagramPacket packet = new DatagramPacket(gsmbuf, GSM_BUFFER_SIZE);
                        socket.receive(packet);
                        if (packet.getLength() == GSM_BUFFER_SIZE) {
                            AudioNative.JniGsmDecodeB(packet.getData(), rawbuf);
                            Log.d(LOG_TAG, "mPacketListener receive from " + packet.getAddress().toString());
                            ConcurrentLinkedQueue<byte[]> queue = mIncomingPacketMap.get(packet.getAddress().toString());
                            // packet will be received in earlier than added from
                            if (queue == null) {
                                queue = new ConcurrentLinkedQueue<>();
                                mExecutor.scheduleAtFixedRate(
                                        new AudioOutputTask(packet.getAddress().toString()),
                                        1 * 1000,
                                        (long)SAMPLE_INTERVAL,
                                        TimeUnit.MILLISECONDS);
                            }
                            synchronized (queue) {
                                queue.add(rawbuf);
                                mIncomingPacketMap.put(packet.getAddress().toString(), queue);
                            }
                        } else {
                            Log.i(LOG_TAG, "Invalid Packet LengthReceived: " + packet.getLength());
                        }

                    }
                    // close socket
                    socket.disconnect();
                    socket.close();
                    Log.i(LOG_TAG, "mPacketListener Stopped");
                } catch (Exception e) {
                    mListenerThreadRun = false;
                    ret[0] = CALL_TERMINATED_BY_EXCEPTION;
                    Log.e(LOG_TAG, "Exception: " + e.toString());
                    e.printStackTrace();
                } finally {
                    if (!socket.isConnected()) socket.disconnect();
                    if (!socket.isClosed()) socket.close();
                    if (ret[0] == CALL_TERMINATED_BY_EXCEPTION) {
                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                mVoIPAudioListener.onCallTerminated(ret[0]);
                            }
                        };
                        new Thread(r).start();
                    }

                }
            }
        });
        mPacketListener.start();
    }

    private void startAudioOutputThread() {
        int max = ConferenceCallState.getInstance().getAttendeeNumList().size();
        mExecutor = Executors.newScheduledThreadPool(max);
    }

    public void changeAudioOut(int audioOutputTarget) {
        AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
/*        if (audioOutputTarget == Utils.AUDIO_OUT_MODE.SPEAKER) {
            audioManager.setBluetoothScoOn(false);
            audioManager.stopBluetoothSco();
            audioManager.setSpeakerphoneOn(true);
        } else */if (audioOutputTarget == Utils.AUDIO_OUT_MODE.EARPIECE) {
            audioManager.setBluetoothScoOn(false);
            audioManager.stopBluetoothSco();
            audioManager.setSpeakerphoneOn(false);
        } else if (audioOutputTarget == Utils.AUDIO_OUT_MODE.BLUETOOTH) {
            audioManager.setSpeakerphoneOn(false);
            audioManager.setBluetoothScoOn(true);
            audioManager.startBluetoothSco();
        }
    }

    static {
        System.loadLibrary("native-lib");
    }

//    public static native int JniGsmOpen();
//    // Not Used uncomment to enable
//    //public static native int JniGsmDecode(byte encoded[], short lin[]);
//    // Not Used uncomment to enable
//    //public static native int JniGsmEncode(short lin[], byte encoded[]);
//
//    public static native int JniGsmDecodeB(byte encoded[], byte lin[]);
//
//    public static native int JniGsmEncodeB(byte lin[], byte encoded[]);
//
//    public static native void JniGsmClose();
}


