package com.keystone.voip.audio;

public class AudioNative {
    public static native int JniGsmOpen();
    // Not Used uncomment to enable
    //public static native int JniGsmDecode(byte encoded[], short lin[]);
    // Not Used uncomment to enable
    //public static native int JniGsmEncode(short lin[], byte encoded[]);

    public static native int JniGsmDecodeB(byte encoded[], byte lin[]);

    public static native int JniGsmEncodeB(byte lin[], byte encoded[]);

    public static native void JniGsmClose();
}
