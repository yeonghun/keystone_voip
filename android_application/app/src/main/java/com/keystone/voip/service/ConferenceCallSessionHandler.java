package com.keystone.voip.service;

import android.content.Context;
import android.util.Log;

import com.keystone.voip.config.ConfigStore;
import com.keystone.voip.state.ConferenceCallState;
import com.keystone.voip.ui.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ConferenceCallSessionHandler {
    private static final String TAG = "CCSessionHandler";
    private Context mContext;

    public ConferenceCallSessionHandler(Context context) {
        mContext = context;
    }

    private class Response {
        int code;
        String msg;
        Response(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        @Override
        public String toString() {
            return "Response: " + code + "(" + msg + ")";
        }
    }

    public void setupConferenceCall(final String myNum, final String datetime, final List<String> numbers) {
        Thread setupConferenceThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = new Response(200, "");
                try {
                    JSONObject jsonUserData = new JSONObject();

                    JSONObject conference = new JSONObject();
                    conference.put(Utils.CC_DATE_TIME, datetime);
                    jsonUserData.put("conference", conference);

                    JSONArray phoneNums = new JSONArray();
                    for (String num : numbers) {
                        JSONObject obj = new JSONObject();
                        obj.put("phoneNum", num);
                        phoneNums.put(obj);
                    }
                    jsonUserData.put("attendants", phoneNums);
                    response = sendMessage("/conference/setup", jsonUserData);
                    if (response.code == 200) {
                        // save cc info from firebase message
//                        JSONObject jsObj = new JSONObject(response.msg);
//                        JSONObject outConference = (JSONObject)jsObj.get("conference");
//                        String id = outConference.getString(Utils.CC_ID);
////                        String datetime = outConference.getString(Utils.CC_DATE_TIME);
//                        ConferenceCallState.getInstance().setCallId(id);
//                        Log.d(TAG, ConferenceCallState.getInstance().dump());
                    }
                } catch (Exception e) {
                } finally {
                    Log.w(TAG, "setupConferenceCall, response: " + response);
                }
            }
        });
        setupConferenceThread.start();
    }

    public void readyConferenceCall(final String myNum, final String conCallId) {
        Thread readyConferenceThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = new Response(200, "");
                try {
                    JSONObject jsonUserData = new JSONObject();
                    jsonUserData.put("phoneNum", myNum);
                    jsonUserData.put(Utils.CC_ID, conCallId);
                    response = sendMessage("/conference/ready", jsonUserData);
                } catch (Exception e) {
                } finally {
                    Log.w(TAG, "setupConferenceCall, response: " + response);
                }
            }
        });
        readyConferenceThread.start();
    }

    public void endConferenceCall(final String myNum, final String conCallId) {
        Thread readyConferenceThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = new Response(200, "");
                try {
                    JSONObject jsonUserData = new JSONObject();
                    jsonUserData.put("phoneNum", myNum);
                    jsonUserData.put(Utils.CC_ID, conCallId);
                    response = sendMessage("/conference/end", jsonUserData);
                } catch (Exception e) {
                } finally {
                    Log.w(TAG, "setupConferenceCall, response: " + response);
                }
            }
        });
        readyConferenceThread.start();
    }

    private ConferenceCallSessionHandler.Response sendMessage(String page, JSONObject jsonUserData) throws Exception {
        HttpURLConnection myConnection = null;
        URL serverURL = new URL(ConfigStore.Server.getAddress(mContext) +
                page);
        myConnection = (HttpURLConnection) serverURL.openConnection();
        myConnection.setRequestMethod("POST");
        myConnection.setRequestProperty("Content-Type", "application/json");
        myConnection.setRequestProperty("User-Agent", "keystone");
        //Build Json for USER data
        Log.d(TAG, "sendMessage: " + serverURL.toString() + " " + jsonUserData.toString());
        myConnection.setDoOutput(true);
        myConnection.getOutputStream().write(jsonUserData.toString().getBytes());// Write the data

        int code = myConnection.getResponseCode();
        InputStream responseBody = myConnection.getInputStream();
        String msg = Utils.getStringFromInputStream(responseBody);
        if (myConnection != null) {
            myConnection.disconnect();
        }
        return new Response(code, msg);
    }
}
