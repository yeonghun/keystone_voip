package com.keystone.voip.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;
import android.os.Bundle;

import com.keystone.voip.state.CallState;
import com.keystone.voip.state.MessageState;
import com.keystone.voip.ui.ChatFragment;
import com.keystone.voip.ui.ChatMessage;
import com.keystone.voip.ui.Utils;

import java.util.ArrayList;

import static com.keystone.voip.ui.Utils.ACTION.ACT_SEND_MSG;
import static com.keystone.voip.ui.Utils.ACTION.ACT_RECV_MSG;

public class MessageService extends Service implements MessageListner {
    private MessageHandler mMessageHandler;
    private ChatFragment fragment;

    private String sender = "";
    private String receiver = "";
    private String msg = "";
    private String msgId = "";

    @Override
    public void onCreate() {
        super.onCreate();
        mMessageHandler = new MessageHandler(this, this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
        Bundle extraBundle = intent.getExtras();
        int action = extraBundle.getInt("action");

        sender = extraBundle.getString(Utils.MSG_FROM_PHONE_NUM);
        receiver = extraBundle.getString(Utils.MSG_TO_PHONE_NUM);
        msg = extraBundle.getString(Utils.MESSAGE);
        msgId = extraBundle.getString(Utils.MSG_ID);

        switch (action) {
            case ACT_SEND_MSG:
                mMessageHandler.sendMessage(receiver, msg);
                break;

            case ACT_RECV_MSG:

                final ChatMessage chatMessage = new ChatMessage(sender, receiver, msg, Utils.ISMINE.MSG_ISNOTMINE);

                if(ChatFragment.mRecvNum.getText().toString() != sender) {
                    ChatFragment.mRecvNum.setText(sender);
                    ChatFragment.MsgAdapter.clear();

                    ArrayList<ChatMessage> cMessage = ChatFragment.helper.getSeletedReceivermsg(sender);

                    for (int index = 0; index < cMessage.size(); index++) {
                        ChatFragment.MsgAdapter.add(cMessage.get(index));
                        ChatFragment.MsgAdapter.notifyDataSetChanged();
                    }
                }

                ChatFragment.MsgAdapter.add(chatMessage);
                ChatFragment.MsgAdapter.notifyDataSetChanged();
                ChatFragment.helper.insertMsg(sender, receiver, msg, Utils.ISMINE.MSG_ISNOTMINE);

                mMessageHandler.recvMessage(receiver, msg, msgId);
                MessageState.getInstance().setReceiver(sender);
                break;
        }
        return super.onStartCommand(intent, flags, startId);
        } catch (Exception e) {
            e.printStackTrace();
            return Service.START_NOT_STICKY;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onMessageSent(String result) {
        // TODO: Implement: onMessageSent called when MessageHandler successfully sent message
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMessageReceived(String sender, String receiver, String msg) {
        // TODO: Implement: onMessageReceived called when MessageHandler receive message
        final ChatMessage chatMessage = new ChatMessage(sender, receiver, msg, Utils.ISMINE.MSG_ISNOTMINE);

        ChatFragment.MsgAdapter.add(chatMessage);
        ChatFragment.MsgAdapter.notifyDataSetChanged();
        ChatFragment.helper.insertMsg(sender, receiver, msg, Utils.ISMINE.MSG_ISNOTMINE);
    }
}
