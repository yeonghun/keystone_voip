package com.keystone.voip.ui;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.keystone.voip.R;
import com.keystone.voip.account.Account;
import com.keystone.voip.config.ConfigStore;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BillingHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BillingHistoryFragment extends BaseFragment implements SettingsActivity.newDateTimeListner, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "BillingHistoryFragment";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView mMonth_tv;
    private ImageView mArrowLeft;
    private ImageView mArrowRight;
    private TextView mCallCost;
    private TextView mCallDuration;
    private TextView mTextCost;
    private TextView mTextCount;
    private TextView mTotalCost;
    private DatePickerFragment mDf;
    private Calendar mBillCalendar = Calendar.getInstance();


    public BillingHistoryFragment() {
        // Required empty public constructor
    }

    public static BillingHistoryFragment newInstance(String param1, String param2) {
        BillingHistoryFragment fragment = new BillingHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mDf = new DatePickerFragment();
        mDf.setListner((SettingsActivity.newDateTimeListner)this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragView = inflater.inflate(R.layout.fragment_billing_history, container, false);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }

        mMonth_tv = (TextView) fragView.findViewById(R.id.monthtv);
        mMonth_tv.setOnClickListener(this);
        mMonth_tv.setText(getMonthYearString(mBillCalendar));

        mArrowLeft = (ImageView) fragView.findViewById(R.id.img_left_arrow);
        mArrowLeft.setOnClickListener(this);

        mArrowRight = (ImageView) fragView.findViewById(R.id.img_right_arrow);
        mArrowRight.setOnClickListener(this);

        mCallCost = (TextView) fragView.findViewById(R.id.cost_value);
        mCallDuration = (TextView) fragView.findViewById(R.id.call_duration);
        mTextCost = (TextView) fragView.findViewById(R.id.msg_cost_value);
        mTextCount = (TextView) fragView.findViewById(R.id.msg_count);
        mTotalCost = (TextView) fragView.findViewById(R.id.total_cost_value);
        return fragView;
    }

    @Override
    public void onResume() {
        super.onResume();
        new BillTask().execute();
    }

    @Override
    public void onClick(View v) {

        if (v == null) {
            Log.e(TAG, " onClick : v = null");
            return;
        }
        if (v == mMonth_tv ) {
            mDf.setCalender(mBillCalendar);
            mDf.show(getFragmentManager(),"datepicker");
        } else if (v == mArrowLeft) {
          mBillCalendar.add(Calendar.MONTH, -1);
          mMonth_tv.setText(getMonthYearString(mBillCalendar));
          //TODO fetch bill from server and update
            new BillTask().execute();

        } else if (v == mArrowRight) {
          mBillCalendar.add(Calendar.MONTH, +1);
          mMonth_tv.setText(getMonthYearString(mBillCalendar));
            //TODO fetch bill from server and update
            new BillTask().execute();
        }
    }

    @Override
    public void onNewDate(int year, int month, int dayofmonth) {
        mBillCalendar.set(Calendar.YEAR, year);
        mBillCalendar.set(Calendar.MONTH, month);
        mBillCalendar.set(Calendar.DAY_OF_MONTH, dayofmonth);

        if (mMonth_tv != null) {
            mMonth_tv.setText(getMonthYearString(mBillCalendar));
        }
        //TODO fetch bill from server and update
        new BillTask().execute();
        return;
    }

    @Override
    public void onNewTime(int hourOfDay, int minute) {

    }

    private String getMonthYearString(Calendar c) {

        String monthName =  c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        String yearString = Integer.toString(c.get(Calendar.YEAR)).toUpperCase(Locale.getDefault());
        return monthName + " " + yearString;
    }

    private class BillTask extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(BillingHistoryFragment.this.getContext());
        private final String FAIL = "fail";
        private final String PASS = "pass";

        private  Double callCost = 0.0;
        private  int callDuration = 0;

        private  Double textCost = 0.0;
        private  int textCount = 0;

        private  String totalCost = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pdLoading != null) {
                pdLoading.setMessage(getResources().getString(R.string.processing_msg));
                pdLoading.show();
            }
        }
        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;
            try {

                URL serverURL = new URL(  ConfigStore.Server.getAddress(getContext()) +"/bill/summary");
                HttpURLConnection myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");

                //Build Json for Login data
                JSONObject jsonBillData = new JSONObject();

                jsonBillData.put(Utils.PHONENUMBER, Account.getInstance().getPhoneNumber());
                jsonBillData.put("month", mBillCalendar.get(Calendar.YEAR)+""+ String.format("%02d", 1 + mBillCalendar.get(Calendar.MONTH)));
                jsonBillData.put(Utils.REGID, FirebaseInstanceId.getInstance().getToken());

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonBillData.toString().getBytes());

                InputStream responseBody;
                String responseString = "";

                JSONObject jsonObjReader = null;
                JSONArray jsonArrayReader = null;
                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    result = PASS;
                    responseBody = myConnection.getInputStream();
                    responseString = Utils.getStringFromInputStream(responseBody);
                    jsonArrayReader = new JSONArray(responseString);

                    for(int i=0; i < jsonArrayReader.length(); i++){

                        jsonObjReader = jsonArrayReader.getJSONObject(i);
                        String category = jsonObjReader.getString("category");
                        if (category.equalsIgnoreCase("Call")) {
                         callCost = jsonObjReader.getDouble("cost");
                         callDuration = jsonObjReader.getInt("duration");

                        } else if (category.equalsIgnoreCase("Text")) {
                         textCost = jsonObjReader.getDouble("cost");
                         textCount = jsonObjReader.getInt("duration");
                        }
                    }

                    }
                else {
                    result = FAIL;
                }
            } catch (Exception e){
                result = FAIL;
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pdLoading != null) {
                pdLoading.dismiss();
            }
            if (result.equals(FAIL))
            {
                Toast.makeText(BillingHistoryFragment.this.getContext(),"Fetching Billing info failed, Try later", Toast.LENGTH_LONG).show();
                return;
            } else {
                mCallCost.setText("$"+ String.format("%.2f",callCost));
                mCallDuration.setText("("+(callDuration/3600) +"h" + " " + ((callDuration%3600)/60)+"m)");
                mTextCount.setText("("+textCount+" texts)");
                mTextCost.setText("$"+ String.format("%.2f",textCost));
                double d = callCost+textCost;
                mTotalCost.setText("$"+ String.format("%.2f",d));
            }
        }

    }

}
