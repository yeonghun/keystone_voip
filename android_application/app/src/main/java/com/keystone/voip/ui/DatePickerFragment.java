package com.keystone.voip.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private SettingsActivity.newDateTimeListner mListner;
    private Calendar mLocalCalender;
    public void DatePickerFragment() {

    }
    public void setListner(SettingsActivity.newDateTimeListner listener) {
        mListner = listener;
    }
    public void setCalender(Calendar cal) {
        mLocalCalender = cal;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int year = mLocalCalender.get(Calendar.YEAR);
        int month = mLocalCalender.get(Calendar.MONTH);
        int day = mLocalCalender.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getContext(),this, year, month,day);
    }
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        mListner.onNewDate(year,month,dayOfMonth);
    }
}
