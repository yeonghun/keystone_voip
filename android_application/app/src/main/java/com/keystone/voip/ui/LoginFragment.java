package com.keystone.voip.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.keystone.voip.R;
import com.keystone.voip.account.Account;
import com.keystone.voip.config.ConfigStore;
import com.keystone.voip.state.CallState;

import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener {

    private final String TAG = "LoginFragment";

    private TextInputEditText mUserID_EditTv;
    private TextInputEditText mPassword_EditTv;

    private Button mLogin_Btn;
    private TextView mCreateAccount_Tv;
    private TextView mForgotPwd_Tv;


    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragView = inflater.inflate(R.layout.login_fragment, container, false);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
        mUserID_EditTv =  (TextInputEditText) fragView.findViewById(R.id.id_edittv);
        mUserID_EditTv.setText("503863");
        mUserID_EditTv.setSelection(mUserID_EditTv.length());
        mPassword_EditTv = (TextInputEditText) fragView.findViewById(R.id.pwd_edittv);

        mLogin_Btn = (Button) fragView.findViewById(R.id.login_btn);
        mLogin_Btn.setOnClickListener(this);

        mCreateAccount_Tv = (TextView) fragView.findViewById(R.id.createaccount_tv);
        mCreateAccount_Tv.setOnClickListener(this);

        mForgotPwd_Tv = (TextView) fragView.findViewById(R.id.forgetpwd_tv);
        mForgotPwd_Tv.setOnClickListener(this);

        return fragView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
      if (v == null) {
          Log.e(TAG, " onClick : v = null");
          return;
      }
      int fragmentName = 0;
      if (v == mLogin_Btn) {
          fragmentName = Utils.Fragment.LOGIN;
          if (validateInputFields() == false) {
              return;
          }
          new LoginTask().execute();
          return;
      } else if (v == mCreateAccount_Tv) {
          fragmentName = Utils.Fragment.CREATE_ACCOUNT;
      } else if (v == mForgotPwd_Tv) {
          fragmentName = Utils.Fragment.FORGOT_PASSWORD;
      }
      if (mListener == null) {
          Log.e(TAG, " onClick : mListener = null");
          return;
      }
      mListener.onActionPerformed(fragmentName,0,new Bundle());
    }
    private boolean validateInputFields() {

        if ((mUserID_EditTv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.user_id), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mPassword_EditTv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.password), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private class LoginTask extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(LoginFragment.this.getContext());
        private final String FAIL = "fail";
        private final String PASS = "pass";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pdLoading != null) {
                pdLoading.setMessage(getResources().getString(R.string.processing_msg));
                pdLoading.show();
            }
        }
        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;
            try {

                URL serverURL = new URL(  ConfigStore.Server.getAddress(getContext()) +
                        "/user/login");
                HttpURLConnection myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");

                //Build Json for Login data
                JSONObject jsonLoginData = new JSONObject();

                jsonLoginData.put(Utils.PHONENUMBER, mUserID_EditTv.getText().toString());
                jsonLoginData.put(Utils.PASSWORD, mPassword_EditTv.getText().toString());
                jsonLoginData.put(Utils.IP, Utils.getLocalIP(LoginFragment.this.getContext()));
                jsonLoginData.put(Utils.REGID, FirebaseInstanceId.getInstance().getToken());

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonLoginData.toString().getBytes());

                Account newAccount = Account.getInstance();

                InputStream responseBody;
                String responseString = "";
                String phoneNumberString = "";
                JSONObject jsonReader = null;

                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    result = PASS;
                    responseBody = myConnection.getInputStream();
                    responseString = Utils.getStringFromInputStream(responseBody);
                    jsonReader = new JSONObject(responseString);


                    newAccount.setFirstName(jsonReader.getString(Utils.FIRST_NAME));
                    newAccount.setLastName(jsonReader.getString(Utils.LAST_NAME));
                    newAccount.setEmailID(jsonReader.getString(Utils.EMAIL));
                    newAccount.setLocalIP(Utils.getLocalIP(LoginFragment.this.getContext()));
                    newAccount.setPhoneNumber(jsonReader.getString(Utils.PHONENUMBER));
                    newAccount.setPassword(mPassword_EditTv.getText().toString());
                    newAccount.saveAccount(LoginFragment.this.getContext());

                    CallState.getInstance().setLocalPhoneNumber(mUserID_EditTv.getText().toString());
                    CallState.getInstance().setLocalIP(newAccount.getLocalIP());
                } else {
                result = FAIL;
            }

            } catch (Exception e){
                result = FAIL;
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pdLoading != null) {
                pdLoading.dismiss();
            }
            if (result.equals(FAIL))
            {
                Toast.makeText(LoginFragment.this.getContext(),getResources().getString(R.string.login_fail), Toast.LENGTH_LONG).show();
                return;
            } else {
                Toast.makeText(LoginFragment.this.getContext(),getResources().getString(R.string.login_pass), Toast.LENGTH_LONG).show();
                if (mListener == null) {
                    Log.e(TAG, "onPostExecute : mListener = null");
                    return;
                }
                mListener.onActionPerformed(Utils.Fragment.LOGIN,0,new Bundle());
            }
        }

    }
}
