package com.keystone.voip.state;

import android.telecom.Call;
import android.util.Log;

import java.util.Observable;

public class CallState extends Observable {
    private static final String TAG = "CallState";

    private String mLocalIP;
    private String mRemoteIP;
    private String mCallID;
    private String mLocalPhoneNumber;
    private String mRemotePhoneNumber;

    public enum Call_State {
        IDLE,
        CONNECTING,
        CALLING,
        INCOMING,
        INCALL,
        CONNECTION_FAIL,
        CALLREJECTED,
        CALLEND
    };

    private Call_State  mCallState = Call_State.IDLE;

    private static CallState sCallStateInstance = new CallState();
    public static final CallState getInstance() {
        if (sCallStateInstance == null) {
            sCallStateInstance = new CallState();
        }
        return sCallStateInstance;
    }

    private CallState() { }

    public String getLocalPhoneNumber() {
        return mLocalPhoneNumber;
    }

    public void setLocalPhoneNumber(String localPhoneNumber) {
        mLocalPhoneNumber = localPhoneNumber;
    }

    public String getRemotePhoneNumber() {
        return mRemotePhoneNumber;
    }

    public void setRemotePhoneNumber(String remotePhoneNumber) {
        mRemotePhoneNumber = remotePhoneNumber;
    }

    public String getLocalIP() {
        return mLocalIP;
    }

    public void setLocalIP(String localIP) {
        mLocalIP = localIP;
    }

    public String getCallID() {
        return mCallID;
    }

    public void setCallID(String callID) {
        mCallID = callID;
    }

    public String getRemoteIP() {
        return mRemoteIP;
    }

    public void setRemoteIP(String remoteIP) {
        mRemoteIP = remoteIP;
    }
    public Call_State getCallState() {
        return mCallState;
    }

    public void setCallState(Call_State callState) {
        mCallState = callState;
    }

    public void notifyUpdate() {
        setChanged();
        notifyObservers();
    }

    public void clearAllState() {
        Log.d(TAG, "clearAllState()");
        setCallState(Call_State.IDLE);
        setCallID("");
        setRemoteIP("");
    }

    /*
    private String mLocalIP;
    private String mRemoteIP;
    private String mCallID;
    private String mLocalPhoneNumber;
    private String mRemotePhoneNumber;
    private Boolean mRingerEnabled;
    private Boolean mMicEnabled;
     */
    public String dump() {
        StringBuilder sb = new StringBuilder();
        sb.append("dump:\n");
        sb.append("mCallID: " + mCallID);
        sb.append("\n");
        sb.append("mLocalIP: " + mLocalIP);
        sb.append("\n");
        sb.append("mRemoteIP: " + mRemoteIP);
        sb.append("\n");
        sb.append("mLocalPhoneNumber: " + mLocalPhoneNumber);
        sb.append("\n");
        sb.append("mRemotePhoneNumber: " + mRemotePhoneNumber);
        sb.append("\n");
        return sb.toString();
    }
}
