package com.keystone.voip.ui;


import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.keystone.voip.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsPreferenceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsPreferenceFragment extends PreferenceFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "SettingsPreferenceFrag";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public SettingsPreferenceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingsPreferenceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsPreferenceFragment newInstance(String param1, String param2) {
        SettingsPreferenceFragment fragment = new SettingsPreferenceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        getPreferenceManager().setSharedPreferencesName(Utils.SETTINGS_SHARED_PREFERENCES_FILE_NAME);
        addPreferencesFromResource(R.xml.callpreference);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragView =super.onCreateView(inflater, container, savedInstanceState);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
        if(fragView != null) {
            ListView lv = (ListView) fragView.findViewById(android.R.id.list);
            lv.setPadding(10, 120, 10, 10);
        }

        return fragView;
    }
}
