package com.keystone.voip.state;
import java.util.Vector;

public class MessageState {
    Vector<String> msgDuplicatedChecker = new Vector<String>();
    private String mLastReceiver = "null";

    private MessageState(){}

    public static MessageState getInstance() {
        return LazyHolder.INSTANCE;
    }

    private static class LazyHolder {
        private static final MessageState INSTANCE = new MessageState();
    }

    public void addReceiveMsg(String msgId)
    {
        msgDuplicatedChecker.add(msgId);
    }

    public boolean isDuplicated(String msgId)
    {
        return msgDuplicatedChecker.contains(msgId);
    }

    public void setReceiver(String receiver) { mLastReceiver = receiver; }

    public String getReceiver() { return mLastReceiver; }

    public boolean checkChangeReceiver(String newRcv) {
        boolean isChange = false;
        if(mLastReceiver != newRcv)
            isChange = true;

        return isChange;
    }
}
