package com.keystone.voip.service;

public interface MessageListner {
    void onMessageSent(String result);
    void onMessageReceived(String sender, String receiver, String msg);
}
