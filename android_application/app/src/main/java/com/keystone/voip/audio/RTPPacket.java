package com.keystone.voip.audio;


import android.util.Log;

import java.util.Random;
import static android.support.constraint.Constraints.TAG;

public class RTPPacket {
	private static final String LOG_TAG = "VoIPAudioIo";
	final private static int VERSION = 2;
	final private static int PADDING = 0;
	final private static int EXTENSION = 0;
	final private static int MAKER = 0;
	final private static int CC = 0;
	final private static int RTP_HEADER_SIZE  = 12;
	private int mSequenceNumber;
	private int mPayloadType;
	private int mSsrc;
	
	private byte[] header; 
	public RTPPacket(int payloadtype, int mSsrc) {
		mSequenceNumber = (new Random()).nextInt(65536); 
		mPayloadType  = payloadtype;
		header = new byte[RTP_HEADER_SIZE];
		this.mSsrc = mSsrc;
		
		header[0] = (byte)(VERSION << 6 | PADDING << 5 | EXTENSION << 4 | CC);
		header[1] = (byte)(MAKER << 7 | mPayloadType & 0x3f);
		
	}
	
	public byte[] genRTPPacket(byte[] data, long timestamp, int packet_length) {
		byte[] rtpPacket = new byte[RTP_HEADER_SIZE + packet_length];

		rtpPacket[0] = header[0];
		rtpPacket[1] = header[1];
		rtpPacket[2] = (byte)(mSequenceNumber >> 8);
		rtpPacket[3] = (byte)(mSequenceNumber & 0xff);
		rtpPacket[4] = (byte)(timestamp >> 24);
		rtpPacket[5] = (byte)(timestamp >> 16);
		rtpPacket[6] = (byte)(timestamp >> 8);
		rtpPacket[7] = (byte)(timestamp & 0xff);
		rtpPacket[8] = (byte)(mSsrc >> 24);
		rtpPacket[9] = (byte)(mSsrc >> 16);
		rtpPacket[10] = (byte)(mSsrc >> 8);
		rtpPacket[11] = (byte)(mSsrc & 0xff);		
		System.arraycopy(data,  0,  rtpPacket,  RTP_HEADER_SIZE, packet_length);
		
		mSequenceNumber++;
//		Log.d(LOG_TAG, "RPTP"+mSequenceNumber +" " + timestamp + " " + packet_length);
		return rtpPacket; 
		
	}

}
