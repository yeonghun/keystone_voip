package com.keystone.voip.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.keystone.voip.R;
import com.keystone.voip.service.ConferenceCallService;
import com.keystone.voip.state.ConferenceCallState;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ConferenceTestActivity extends Activity implements View.OnClickListener, Observer {
    private static final String TAG = "ConferenceTestActivity";
    private Button mSetupBtn;
    private Button mSaveBtn;
    private Button mStartBtn;
    private Button mEndBtn;

    private EditText et1;
    private EditText et2;
    private EditText et3;
    private EditText et4;
    private EditText et5;
    private EditText et6;
    private EditText et7;
    private EditText et8;
    private EditText et9;
    private EditText et10;
    private EditText et11;
    private EditText et12;

    private EditText at1;
    private EditText at2;
    private EditText at3;
    private EditText at4;

    private TextView tv1;

    private PowerManager.WakeLock proximityWakeLock;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conference_test);

        mSetupBtn = (Button)findViewById(R.id.conf_setup);
        mSetupBtn.setOnClickListener(this);
        mSaveBtn = (Button)findViewById(R.id.conf_save);
        mSaveBtn.setOnClickListener(this);
        mStartBtn = (Button)findViewById(R.id.conf_start);
        mStartBtn.setOnClickListener(this);
        mEndBtn = (Button)findViewById(R.id.conf_end);
        mEndBtn.setOnClickListener(this);

        at1 = (EditText)findViewById(R.id.attendee1);
        at2 = (EditText)findViewById(R.id.attendee2);
        at3 = (EditText)findViewById(R.id.attendee3);
        at4 = (EditText)findViewById(R.id.attendee4);
        at1.setText("0001");
        at1.setSelection(at1.length());
        at2.setText("0002");
        at2.setSelection(at2.length());
        at3.setText("0003");
        at3.setSelection(at3.length());
        at4.setText("0004");
        at4.setSelection(at4.length());

        et1 = (EditText)findViewById(R.id.editText);
        et2 = (EditText)findViewById(R.id.editText2);
        et3 = (EditText)findViewById(R.id.editText3);
        et4 = (EditText)findViewById(R.id.editText4);
        et5 = (EditText)findViewById(R.id.editText5);
        et6 = (EditText)findViewById(R.id.editText6);
        et7 = (EditText)findViewById(R.id.editText7);
        et8 = (EditText)findViewById(R.id.editText8);
        et9 = (EditText)findViewById(R.id.editText9);
        et10 = (EditText)findViewById(R.id.editText10);
        et11 = (EditText)findViewById(R.id.editText11);
        et12 = (EditText)findViewById(R.id.editText12);

        tv1 = (TextView)findViewById(R.id.conf_state);
        tv1.setText(ConferenceCallState.getInstance().dump());
        ConferenceCallState.getInstance().addObserver(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.conf_setup:
                ArrayList<String> numbers = new ArrayList<>();
                numbers.add("503863" + at1.getText().toString());
                numbers.add("503863" + at2.getText().toString());
                numbers.add("503863" + at3.getText().toString());
                numbers.add("503863" + at4.getText().toString());
                Intent i = new Intent(this, ConferenceCallService.class);
                i.putExtra("action", Utils.ACTION.ACT_CC_CREATE);
                i.putExtra(Utils.CC_DATE_TIME, "2018-06-29T10:00:00");
                i.putStringArrayListExtra(Utils.CC_ATTENDANTS, numbers);
                startService(i);
                break;
            case R.id.conf_save:
                tv1.setText(ConferenceCallState.getInstance().dump());
//                ConferenceCallState.getInstance().clearAllState();
//                if (et1.getText().toString().length() > 0) {
//                    StringBuilder sb = new StringBuilder();
//                    sb.append(et1.getText().toString()).append(".");
//                    sb.append(et2.getText().toString()).append(".");
//                    sb.append(et3.getText().toString()).append(".");
//                    sb.append(et4.getText().toString());
//                    Log.w(TAG, "1 -> " + sb.toString());
//                    ConferenceCallState.getInstance().addAttendeeIp(sb.toString());
//                }
//
//                if (et5.getText().toString().length() > 0) {
//                    StringBuilder sb = new StringBuilder();
//                    sb.append(et5.getText().toString()).append(".");
//                    sb.append(et6.getText().toString()).append(".");
//                    sb.append(et7.getText().toString()).append(".");
//                    sb.append(et8.getText().toString());
//                    Log.w(TAG, "2 -> " + sb.toString());
//                    ConferenceCallState.getInstance().addAttendeeIp(sb.toString());
//                }
//
//                if (et9.getText().toString().length() > 0) {
//                    StringBuilder sb = new StringBuilder();
//                    sb.append(et9.getText().toString()).append(".");
//                    sb.append(et10.getText().toString()).append(".");
//                    sb.append(et11.getText().toString()).append(".");
//                    sb.append(et12.getText().toString());
//                    Log.w(TAG, "3 -> " + sb.toString());
//                    ConferenceCallState.getInstance().addAttendeeIp(sb.toString());
//                }
                break;
            case R.id.conf_start:
                enableProximityWakeLock();
                Intent start = new Intent(this, ConferenceCallService.class);
                start.putExtra("action", Utils.ACTION.ACT_CC_START);
                startService(start);
                break;
            case R.id.conf_end:
                disableProximityWakeLock();
                Intent end = new Intent(this, ConferenceCallService.class);
                end.putExtra("action", Utils.ACTION.ACT_CC_END);
                startService(end);
                break;

            default: break;
        }

    }

    public boolean enableProximityWakeLock() {
        if (proximityWakeLock != null) {
            return true;
        }
            PowerManager powerManager = (PowerManager)
                    getApplicationContext().getSystemService(Context.POWER_SERVICE);
            if (powerManager != null) {
                proximityWakeLock = powerManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "keystone:proximityWaklock");
                proximityWakeLock.acquire();
            }
        return false;
    }

    public void disableProximityWakeLock() {
        if (proximityWakeLock != null) {
            proximityWakeLock.release();
            proximityWakeLock = null;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        tv1.setText(ConferenceCallState.getInstance().dump());

        if (ConferenceCallState.getInstance().getCcState() == ConferenceCallState.CcState.END) {
            ConferenceCallState.getInstance().clearAllState();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ConferenceCallState.getInstance().deleteObserver(this);
    }
}
