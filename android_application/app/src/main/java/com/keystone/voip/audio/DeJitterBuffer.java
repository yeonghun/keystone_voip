package com.keystone.voip.audio;


import android.util.Log;

import java.util.Arrays;
import java.lang.Math;
import java.lang.reflect.Array;
import static android.support.constraint.Constraints.TAG;

public class DeJitterBuffer {
	private static final int BUFFER_SIZE = 200;
	private static final int BUFFER_DEPTH = 100;
	private static final int DELAY_FOR_JITTER = 100; // ms
	private static final int SAMPLE_INTERVAL = 20; // ms 

	private static final int TIMESTAMP_INTERVAL = 160; //sampling tick about 20ms
	
	/* Buffer status */
	public static final byte EMPTY = 0;
	public static final byte LOSS = 1;
	public static final byte ACTIVE = 2;
	
	/* JITTER Buffer State Machine */
	private static final byte IDLE = 0;
	private static final byte DELAY_PACKET = 1;
	private static final byte NORMAL = 2;

	private byte[][] mBuffer = new byte[BUFFER_DEPTH][BUFFER_SIZE];
	private byte mBufferState[] = new byte[BUFFER_DEPTH];
	private short mBufferSize[] = new short[BUFFER_DEPTH];
	private long mBufferTimestamp[] = new long[BUFFER_DEPTH];
	
	private int mReadIdx;
	private int mWriteIdx; 
	private int mPacketCount;
	private int mNextSeq;
	private long mNextTimestamp;
	private int mBufferDelay;
	private byte mJitterState;
	private int mDelayCount;
	private int mContinuePacketCount;
	private long currentMillis ;
	private long prevMillis;
	private int lastPacketCount;
    private long mNextTimestampAfterJitter;
	
	private RTPDePacket mRTPDePacket;

	private static final String LOG_TAG = "VoIPAudioIo";

	private void initJitterBuffer(){
		for(int idx = 0 ; idx < BUFFER_DEPTH ; idx++) {
			Arrays.fill(mBuffer[idx], (byte)0x0);
		}
		Arrays.fill(mBufferState,  EMPTY);
		Arrays.fill(mBufferSize, (short)0x0);
		Arrays.fill(mBufferTimestamp,  (long)0x0);
		mBufferDelay = DELAY_FOR_JITTER  / SAMPLE_INTERVAL;
		mReadIdx  = BUFFER_DEPTH/2;
		mWriteIdx  = BUFFER_DEPTH/2 ;
		mPacketCount = 0;
		mNextSeq = 0;
		mNextTimestamp = 0;
		mJitterState = IDLE;
		mDelayCount = 0;
		mContinuePacketCount  = 0;
		prevMillis = -1; /* Not assigned */
        mNextTimestampAfterJitter = -1;
		lastPacketCount = -1;
	}

	
	public DeJitterBuffer() {
		initJitterBuffer();
		mRTPDePacket = new RTPDePacket();
	}
	
	
	public byte read(byte[] voice) /* return buffer state */ 
	{
		byte bufferstate = EMPTY;
	//	System.out.println("Jitter State= "+mJitterState + " " + mPacketCount + " " + mReadIdx + " " + mWriteIdx);
//		Log.i(LOG_TAG, "buffer read"+" "+mJitterState + " "+mPacketCount + " " + mReadIdx);
		
		if ( mJitterState == IDLE) {
			bufferstate = EMPTY;

		} else if (mJitterState == DELAY_PACKET){
			if(mDelayCount < mBufferDelay) {
				bufferstate = EMPTY;
				mDelayCount++;
				if(mDelayCount == (mBufferDelay -1)) {
					updateJitterBufferState(NORMAL);
					mDelayCount = 0;
				}
			}else{
				updateJitterBufferState(NORMAL);
			}
		} else {/* mJitterState is NORMAL  */
			if(isBufferEmpty() == true) {
				bufferstate = EMPTY;
				updateJitterBufferState(IDLE);
				mBufferDelay = 5;
			} else 	{
//				Log.i(LOG_TAG, "buffer copy"+" "+mBufferTimestamp[mReadIdx]+" "+mNextTimestamp);
				if(mBufferState[mReadIdx] == ACTIVE) {

					if(mBufferTimestamp[mReadIdx] == mNextTimestamp) {
//						Log.i(LOG_TAG, "buffer copys");
						System.arraycopy(mBuffer[mReadIdx], 0, voice , 0, 33);
						bufferstate = ACTIVE;
						updateBufferState(mReadIdx, EMPTY);
						updatePacketCount(false);
						updateReadIdx(mReadIdx+1);
					} else {

						bufferstate = EMPTY;
					}
				}else {
	
					bufferstate = LOSS;
					updateBufferState(mReadIdx, EMPTY);
					updateReadIdx(mReadIdx+1);
					updatePacketCount(false);
				}
				mNextTimestamp = (mNextTimestamp + TIMESTAMP_INTERVAL) & 0xffffffff;

			}
		}

		return bufferstate;
	}
	private int calcNewIdx(int curIdx, int gap)	{
		return  (curIdx + gap + BUFFER_DEPTH) % BUFFER_DEPTH;
	}

	public void write(byte[] packet) {

		mRTPDePacket.parseRTPPacket(packet);
//		Log.d(LOG_TAG, "Jitter"+mRTPDePacket.getTimestamp() +" " + mRTPDePacket.getSequenceNumber());
//		System.out.println("Write "+mRTPDePacket.getTimestamp()+" "+ mRTPDePacket.getSequenceNumber());
		monitorJitter(packet, mRTPDePacket.getTimestamp(), mRTPDePacket.getSequenceNumber());
	}

	private int calcPacketSequenceDiff(int nextSeq, int curSeq){
		if((curSeq -  nextSeq) < (-BUFFER_DEPTH)) {
			return curSeq + 0xffff - nextSeq;
		} else {
			return curSeq - nextSeq;
		}
	}

	
	private void monitorJitter(byte[] packet , long  timestamp, int  sequence) {
		int newWriteIdx;
		int packetDistance;
		int timediff = -1;
		int PacketLatency ;

		int skipPacketCount;
		int  mindelay;

		long currentMillis = System.currentTimeMillis();
		if(prevMillis >=  0){
			timediff =  (int)(currentMillis-prevMillis);
			if(lastPacketCount < 5){
				mindelay =100;
			}else{
				mindelay = lastPacketCount * SAMPLE_INTERVAL;
			}
			if(timediff >mindelay){ /* Too big Jitter */
				skipPacketCount = (timediff - SAMPLE_INTERVAL * lastPacketCount ) / SAMPLE_INTERVAL; /* # of packet which can not play packet because big jitter */
				PacketLatency = (int)(timestamp - mNextTimestamp) / TIMESTAMP_INTERVAL+1;
				mNextTimestampAfterJitter = mNextTimestamp + skipPacketCount * 160;

				mBufferDelay = 0;
				Log.i(LOG_TAG, "Packet loss by big delay"+ " " + timediff+" "+skipPacketCount);
				Log.i(LOG_TAG, "Sequence info "+mNextSeq +"  "+mNextTimestampAfterJitter);
			}
		}
		lastPacketCount = mPacketCount;
		prevMillis = currentMillis;
		if(isBufferEmpty() == true) {
			if(mNextTimestampAfterJitter <= timestamp ) {
				updatePacketBuffer(packet, mWriteIdx, timestamp);
				updatePacketCount(true);
				updateWriteIdx();
				mNextSeq = (sequence + 1) & 0xffff;
				mNextTimestamp = timestamp;
				if (mNextTimestampAfterJitter < 0){
					updateJitterBufferState(DELAY_PACKET);
				} else {
					updateJitterBufferState(NORMAL);
					mNextTimestampAfterJitter = -1;
				}
			}
		}else 	{
			if(mNextSeq == sequence) {
				if(mPacketCount == BUFFER_DEPTH){
					updateReadIdx(mReadIdx);
				}else{
					updatePacketCount(true);
				}

				updatePacketBuffer(packet, mWriteIdx, timestamp);
				updateWriteIdx();
				updateSeqNumber();

			} else 	{/* packet sequence is no correct, Packet loss is occurred */ 
				packetDistance = calcPacketSequenceDiff(mNextSeq, sequence);
				if( packetDistance > 0) { // packet loss
					for(int idx = 0 ; idx < packetDistance; idx++) {
						if (mPacketCount == BUFFER_DEPTH){
							updateReadIdx(mReadIdx+1);
							mNextTimestamp = mBufferTimestamp[mReadIdx];

						}else{
							updatePacketCount(true);
						}

						updateBufferState(mWriteIdx, LOSS);
						updateWriteIdx();		
						updateSeqNumber();
					}
					if (mPacketCount == BUFFER_DEPTH){
						updateReadIdx(mReadIdx);
						mNextTimestamp = mBufferTimestamp[mReadIdx];

					}else{
						updatePacketCount(true);
					}

					updatePacketBuffer(packet, mWriteIdx, timestamp);
					//updatePacketCount(true);
					updateWriteIdx();
					updateSeqNumber();
					
				} else { // receive previous packet . reordering 
					newWriteIdx = calcNewIdx(mWriteIdx,  packetDistance);
					packetDistance = -packetDistance;
					if( packetDistance< mPacketCount) { /* Packet index is between Read Idx and write Idx */
						updatePacketBuffer(packet, newWriteIdx, timestamp); 
					}else { /* New packet is older than our first packet */ 
						if(mJitterState == DELAY_PACKET) {
							if(mReadIdx ==((newWriteIdx+1)%BUFFER_DEPTH)){
								if(mBufferDelay > 0){
									mBufferDelay--;
								}
							}
							updatePacketBuffer(packet, newWriteIdx, timestamp);
							updateReadIdx(newWriteIdx);
							mNextTimestamp = timestamp;
							updatePacketCount(true);
							
							for(int idx = 1 ; idx <(packetDistance - mPacketCount); idx++) {
								updateBufferState((mReadIdx+idx) & 0xffff , LOSS);
								updatePacketCount(true);
							}
						}
					}
			    }
			}
		}
	}
	
	public void updatePacketBuffer(byte[] packet, int writeIdx, long timestamp) {
//        Log.d(LOG_TAG, "Jitter Update"+ writeIdx +" " + timestamp);
		updateBufferState(writeIdx, ACTIVE);
		System.arraycopy(packet,  RTPDePacket.RTP_HEADER_SIZE,  mBuffer[writeIdx],  0, 
				packet.length-RTPDePacket.RTP_HEADER_SIZE);
		mBufferSize[writeIdx] = (short)(packet.length -RTPDePacket.RTP_HEADER_SIZE); 
		mBufferTimestamp[writeIdx] = timestamp;
		
//		System.out.println("update Buffer " + writeIdx + " " + mBufferState[writeIdx] + " " + mBufferTimestamp[writeIdx]);
	}
 
	public  void updateJitterBufferState(byte newState)
	{
		mJitterState = newState; 
	}

	public int getPacketCount(){
		return mPacketCount;
	}

	private  void updatePacketCount (boolean isInc) {
		if(isInc == true) {
			mPacketCount++;
		}else {
			mPacketCount--;
		}
	}
	
	private   void updateBufferState(int idx, byte state)
	{
		mBufferState[idx] = state; 
	}
	
	public boolean isBufferEmpty() {
		return mPacketCount == 0;
	}
	
	private  void updateReadIdx(int newReadIdx)	{
		mReadIdx = newReadIdx % BUFFER_DEPTH;
	}
	
	
	private void updateWriteIdx()	{
		mWriteIdx = (mWriteIdx +1) % BUFFER_DEPTH;
	}
		
	private void updateSeqNumber()	{
		mNextSeq = (mNextSeq+1) & 0xffff;
	}

	public void flush(){
		initJitterBuffer();
	}
}


