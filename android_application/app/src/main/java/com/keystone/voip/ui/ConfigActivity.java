package com.keystone.voip.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.keystone.voip.config.ConfigStore;
import com.keystone.voip.R;

public class ConfigActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "ConfigActivity";
    private EditText etServerAddr;
    private Button btApply;
    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_layout);
        mContext = getApplicationContext();

        final Spinner s1 = (Spinner) findViewById(R.id.server_spinner);

        final String [] prefServer = {"",
                "http://45.56.92.131:8080","http://192.168.43.179:8080",
                "http://192.168.43.51:8080",
                "http://10.0.3.101:8080","http://192.168.43.52:8080"};

        ArrayAdapter adapter = new ArrayAdapter(
                getApplicationContext(), // 현재화면의 제어권자
                R.layout.spinner,
                prefServer); // 데이터
        adapter.setDropDownViewResource(
                R.layout.spin_dropdown);

        s1.setAdapter(adapter);


        etServerAddr = (EditText)findViewById(R.id.server_address);
        etServerAddr.setOnClickListener(this);

        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.d(TAG, "onItemSelected - " + String.valueOf(position));
                if (position != 0)
                    etServerAddr.setText(prefServer[position]);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        etServerAddr.setText(ConfigStore.Server.getAddress(getApplicationContext()));


        btApply = (Button)findViewById(R.id.save_button);
        btApply.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_button:
                // update Server Address
                String address = etServerAddr.getText().toString();
                if (!address.isEmpty()) {
                    ConfigStore.Server.updateAddress(mContext, address);
                    finish();
                }
                break;
            case R.id.reset_button:
                // reset Server Address
                ConfigStore.Server.updateAddress(mContext, "");
                break;
            default : break;
        }
    }
}
