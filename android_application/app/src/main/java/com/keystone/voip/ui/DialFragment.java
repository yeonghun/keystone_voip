package com.keystone.voip.ui;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.keystone.voip.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DialFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DialFragment extends BaseFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private final String TAG = "DialFragment";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Button mCall_Btn;
    private EditText mPhoneNumber_etv;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button mbtn1;
    private Button mbtn2;
    private Button mbtn3;
    private Button mbtn4;
    private Button mbtn5;
    private Button mbtn6;
    private Button mbtn7;
    private Button mbtn8;
    private Button mbtn9;
    private Button mbtn0;
    private ImageView mImgCall;
    private ImageView mImgMsg;
    private ImageView mImgDel;




    public DialFragment()  {
        // Required empty public constructor
    }


    public static DialFragment newInstance(String param1, String param2) {
        DialFragment fragment = new DialFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragView =  inflater.inflate(R.layout.fragment_dial, container, false);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        mPhoneNumber_etv = (EditText) fragView.findViewById(R.id.edtPhoneNumber);
        mPhoneNumber_etv.setText("50386300");
        mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
        mPhoneNumber_etv.setShowSoftInputOnFocus(false);


         mbtn1 = (Button) fragView.findViewById(R.id.btnOne);
         mbtn1.setOnClickListener(this);

         mbtn2 = (Button) fragView.findViewById(R.id.btnTwo);
         mbtn2.setOnClickListener(this);

         mbtn3 = (Button) fragView.findViewById(R.id.btnThree);
         mbtn3.setOnClickListener(this);

         mbtn4 = (Button) fragView.findViewById(R.id.btnFour);
         mbtn4.setOnClickListener(this);

         mbtn5 = (Button) fragView.findViewById(R.id.btnFive);
         mbtn5.setOnClickListener(this);

         mbtn6 = (Button) fragView.findViewById(R.id.btnSix);
         mbtn6.setOnClickListener(this);

         mbtn7 = (Button) fragView.findViewById(R.id.btnSeven);
         mbtn7.setOnClickListener(this);

         mbtn8 = (Button) fragView.findViewById(R.id.btnEight);
         mbtn8.setOnClickListener(this);

         mbtn9 = (Button) fragView.findViewById(R.id.btnNine);
         mbtn9.setOnClickListener(this);

         mbtn0 = (Button) fragView.findViewById(R.id.btnZero);
         mbtn0.setOnClickListener(this);

         mImgCall = (ImageView) fragView.findViewById(R.id.img_call);
         mImgCall.setOnClickListener(this);

         mImgMsg = (ImageView) fragView.findViewById(R.id.img_msg);
         mImgMsg.setOnClickListener(this);

        mImgDel = (ImageView) fragView.findViewById(R.id.btndel);
        mImgDel.setOnClickListener(this);


        return fragView;
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            Log.e(TAG, " onClick : v = null");
            return;
        }
        String phoneNo = mPhoneNumber_etv.getText().toString();
        try {
            switch (v.getId()) {
                case R.id.btnZero:
                    phoneNo += "0";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnOne:
                    phoneNo += "1";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnTwo:
                    phoneNo += "2";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnThree:
                    phoneNo += "3";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnFour:
                    phoneNo += "4";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnFive:
                    phoneNo += "5";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnSix:
                    phoneNo += "6";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnSeven:
                    phoneNo += "7";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnEight:
                    phoneNo += "8";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btnNine:
                    phoneNo += "9";
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;
                case R.id.btndel:
                    if (phoneNo != null && phoneNo.length() > 0) {
                        phoneNo = phoneNo.substring(0, phoneNo.length() - 1);
                    }
                    mPhoneNumber_etv.setText(phoneNo);
                    mPhoneNumber_etv.setSelection(mPhoneNumber_etv.length());
                    break;

                case R.id.img_call:
                    if (phoneNo.trim().length() != 10) {
                        Toast.makeText(getContext(),"Please enter 10 digit Phone Number", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        String contactName = null;
                        DBHelper db = new DBHelper(getContext());
                        contactName = db.getNameFromNumber(mPhoneNumber_etv.getText().toString());
                        if (contactName.isEmpty()) {
                         contactName = "Unknown Contact";
                        }

                        Bundle args  = new Bundle();
                        args.putString(Utils.TO_PHONE_NUMBER, mPhoneNumber_etv.getText().toString());
                        args.putString(Utils.CONTACT_NAME, contactName);
                        mListener.onActionPerformed(0,0, args);
                    }
                    break;
                case R.id.img_msg:
                    //TODO need to launch msging
                    TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs);
                    tabhost.getTabAt(2).select(); // 2 is index of chat tab.
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
/*        if(v == mCall_Btn) {
            Bundle args  = new Bundle();
            args.putString(Utils.TO_PHONE_NUMBER, mPhoneNumber_etv.getText().toString());
            args.putString(Utils.CONTACT_NAME, "NAME OF CONTACT");
            mListener.onActionPerformed(0,0, args);
        }*/
    }
}
