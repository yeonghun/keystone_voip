package com.keystone.voip.ui;

import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.keystone.voip.R;

public class ConferenceActivity extends AppCompatActivity implements OnFragmentInteractionListener{
    ConferenceFragment mConfFragment = null;
    JoinConferenceFragment mJoinConfFragment = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conference);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (findViewById(R.id.conference_fragment_container) != null) {

            if (savedInstanceState != null) {
                return;
            }
            if (getIntent().getAction().equals("setUpConference")) {
                mConfFragment = new ConferenceFragment();
                mConfFragment.setArguments(getIntent().getExtras());
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.conference_fragment_container, mConfFragment).commit();
            } else if(getIntent().getAction().equals("joinConference")) {
                mJoinConfFragment = new JoinConferenceFragment();
                mJoinConfFragment.setArguments(getIntent().getExtras());
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.conference_fragment_container, mJoinConfFragment).commit();
            }
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onActionPerformed(int fragmentName, int action, Bundle args) {

    }
}
