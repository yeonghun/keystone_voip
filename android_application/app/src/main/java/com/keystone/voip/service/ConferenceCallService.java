package com.keystone.voip.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.keystone.voip.account.Account;
import com.keystone.voip.audio.ConferenceVoIPAudioIo;
import com.keystone.voip.audio.VoIPAudioListener;
import com.keystone.voip.state.ConferenceCallState;
import com.keystone.voip.ui.Utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import static com.keystone.voip.ui.Utils.CC_ATTENDANTS;
import static com.keystone.voip.ui.Utils.CC_DATE_TIME;
import static com.keystone.voip.ui.Utils.CC_ID;
import static com.keystone.voip.ui.Utils.IP;

public class ConferenceCallService extends Service {
    private static final String TAG = "ConferenceCallService";
    private ConferenceCallSessionHandler mCcHandler;
    private ConferenceVoIPAudioIo mVoIPAudio;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mCcHandler = new ConferenceCallSessionHandler(this);
        mVoIPAudio = new ConferenceVoIPAudioIo(this, new VoIPAudioListener() {
            @Override
            public void onCallTerminated(int result) {
                // TODO: implement here
            }
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction(Utils.ACTION.toString(Utils.ACTION.ACT_CHANGE_AUDIO_MODE_CONFCALL));
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(mBroadcastReceiver, filter);
        ConferenceCallState.getInstance().setCcState(ConferenceCallState.CcState.IDLE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (intent == null) {
            Log.w(TAG, "our process was automatically restarted with null intent");
            return START_NOT_STICKY;
        }
        Bundle extraBundle = intent.getExtras();
        int action = extraBundle.getInt("action");
        Log.i(TAG, "onStartCommand~! action: " + Utils.ACTION.toString(action) +
                " " + ConferenceCallState.getInstance().dump());

        if (Utils.ACTION.ACT_CC_CREATE == action) {
            String datetime = extraBundle.getString(Utils.CC_DATE_TIME);
            List<String> numbers = extraBundle.getStringArrayList(CC_ATTENDANTS);
            mCcHandler.setupConferenceCall(
                    Account.getInstance().getPhoneNumber(),
                    datetime,
                    numbers);
        } else if (Utils.ACTION.ACT_CC_SETUP == action) { // server send cc info via fire bse
            if (ConferenceCallState.CcState.IDLE != ConferenceCallState.getInstance().getCcState()) {
                return START_NOT_STICKY;
            }
            ConferenceCallState.getInstance().setDatetime(extraBundle.getString(CC_DATE_TIME));
            // save id
            ConferenceCallState.getInstance().setCallId(extraBundle.getString(CC_ID));
            // save attendee with foo logic
            String attendants = extraBundle.getString(CC_ATTENDANTS);
            if (attendants.length() > 2) {
                attendants = attendants.substring(1, attendants.length() - 1);
            }
            String[] split = attendants.split(",");
            for (String number : split) {
                ConferenceCallState.getInstance().addAttendeeNumber(number);
            }

            Log.d(TAG, ConferenceCallState.getInstance().dump());
        } else if (Utils.ACTION.ACT_CC_START == action) { // user start cc
            if (ConferenceCallState.CcState.IDLE != ConferenceCallState.getInstance().getCcState()) {
                return START_NOT_STICKY;
            }
            // 1. send ready msg to server
            mCcHandler.readyConferenceCall(
                    Account.getInstance().getPhoneNumber(),
                    ConferenceCallState.getInstance().getCallId()
            );
            // 2. start audio
            if (ConferenceCallState.getInstance().getAttendeeIpList().size() > 0) {
                for (String ip : ConferenceCallState.getInstance().getAttendeeIpList()) {
                    try {
                        InetAddress address = InetAddress.getByName(ip);
                        mVoIPAudio.addRemoteAddress(address);
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
                mVoIPAudio.startAudio(0);
            }

            // 3. update state
            updateAndNotifyCallState(ConferenceCallState.CcState.INCALL);
        } else if (Utils.ACTION.ACT_CC_END == action) {
            if (ConferenceCallState.CcState.INCALL != ConferenceCallState.getInstance().getCcState()) {
                return START_NOT_STICKY;
            }
            // 1. send end msg to server
            mCcHandler.endConferenceCall(
                    Account.getInstance().getPhoneNumber(),
                    ConferenceCallState.getInstance().getCallId()
            );
            // 2. end audio
            mVoIPAudio.endAudio();
            // 3. update state
            updateAndNotifyCallState(ConferenceCallState.CcState.END);
        } else if (Utils.ACTION.ACT_CC_CHANGE == action) {
            String ip = extraBundle.getString(IP);
            // 1. add attendee ip
            ConferenceCallState.getInstance().addAttendeeIp(extraBundle.getString(IP));

            // 2. if cc state is incall, we need to add address to audio
            if (ConferenceCallState.CcState.INCALL == ConferenceCallState.getInstance().getCcState()) {
                try {
                    InetAddress address = InetAddress.getByName(ip);
                    mVoIPAudio.addRemoteAddress(address);
                    if (ConferenceCallState.getInstance().getAttendeeIpList().size() == 1) {
                        mVoIPAudio.startAudio(0);
                    }
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
            // Log.d(TAG, ConferenceCallState.getInstance().dump());
        }
        return START_NOT_STICKY;
    }

    private void updateAndNotifyCallState(ConferenceCallState.CcState state) {
        ConferenceCallState.getInstance().setCcState(state);
        ConferenceCallState.getInstance().notifyUpdate();
    }
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent ) {
            if (intent.hasExtra("OUT")) {
                int out_mode = intent.getIntExtra("OUT", 100);
                mVoIPAudio.changeAudioOut(out_mode);
            }

           /* if (intent.hasExtra("MIC")) {
                boolean micEnable = intent.getBooleanExtra("MIC", true);
                mVoIPAudio.toggleMicIn(micEnable);
            }
*/
        }
    };
}
