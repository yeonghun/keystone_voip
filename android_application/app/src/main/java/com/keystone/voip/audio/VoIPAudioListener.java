package com.keystone.voip.audio;

public interface VoIPAudioListener {
    int CALL_TERMINATED_BY_END_CALL = 0;
    int CALL_TERMINATED_BY_EXCEPTION = 1;
    void onCallTerminated(int result);
}
