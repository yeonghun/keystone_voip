package com.keystone.voip.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DBHelper extends SQLiteOpenHelper  {
    public static final String DATABASE_NAME = "KeyStoneContacts.db";
    public static final String CONTACTS_TABLE_NAME = "contacts";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_FIRST_NAME = "firstname";
    public static final String CONTACTS_COLUMN_LAST_NAME = "lastname";
    public static final String CONTACTS_COLUMN_EMAIL = "email";
    public static final String CONTACTS_COLUMN_ADDRESS = "address";
    public static final String CONTACTS_COLUMN_PHONE = "phone";
    private HashMap mHashMap;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table contacts " +
                        "(id integer primary key, firstname text,lastname text,phone text,email text, address text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertContact (String firstName, String lastName, String phone, String email, String address) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_FIRST_NAME, firstName);
        contentValues.put(CONTACTS_COLUMN_LAST_NAME, lastName);
        contentValues.put(CONTACTS_COLUMN_PHONE, phone);
        contentValues.put(CONTACTS_COLUMN_EMAIL, email);
        contentValues.put(CONTACTS_COLUMN_ADDRESS, address);

        db.insert(CONTACTS_TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from contacts where id="+id+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }

    public boolean updateContact (Integer id, String firstName, String lastName, String phone, String email, String address) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_FIRST_NAME, firstName);
        contentValues.put(CONTACTS_COLUMN_LAST_NAME, lastName);
        contentValues.put(CONTACTS_COLUMN_PHONE, phone);
        contentValues.put(CONTACTS_COLUMN_EMAIL, email);
        contentValues.put(CONTACTS_COLUMN_ADDRESS, address);
        db.update(CONTACTS_TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteContact (Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CONTACTS_TABLE_NAME,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<Contact> getAllContacts() {
        ArrayList<Contact> array_list = new ArrayList<Contact>();

        SQLiteDatabase db = this.getReadableDatabase();
       Cursor res =  db.rawQuery( "select * from contacts", null );
       if (res != null) {
        res.moveToFirst();

        while(res.isAfterLast() == false) {
            Contact c = new Contact(res.getInt(res.getColumnIndex(CONTACTS_COLUMN_ID)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_FIRST_NAME)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_LAST_NAME)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_EMAIL)),
                    res.getString(res.getColumnIndex(CONTACTS_COLUMN_ADDRESS)));

            //array_list.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_FIRST_NAME))+" " + res.getString(res.getColumnIndex(CONTACTS_COLUMN_LAST_NAME)) );
            array_list.add(c);
            res.moveToNext();
        }
        res.close();
        }
        return array_list;
    }
    public String getNameFromNumber (String phoneNumber) {
        String name = "";
        try {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res =  db.rawQuery( "select * from contacts where phone=?", new String[] {phoneNumber + ""} );
        if (res != null & res.getCount() >= 1) {
            res.moveToFirst();
            name = res.getString(res.getColumnIndex(CONTACTS_COLUMN_FIRST_NAME)) + " " + res.getString(res.getColumnIndex(CONTACTS_COLUMN_LAST_NAME));
            res.close();
        }
        } catch (Exception e){
            e.printStackTrace();
        }
        return name;
    }
}
