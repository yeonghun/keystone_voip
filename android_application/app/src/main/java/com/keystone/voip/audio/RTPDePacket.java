package com.keystone.voip.audio;



public class RTPDePacket {
	final public static int RTP_HEADER_SIZE = 12;
	private int mSequenceNumber; 
	private long mTimestamp;
	private int mSsrc;
	private int mPayloadType; 
	
	
	public RTPDePacket() {
		mSequenceNumber = 0;
		mTimestamp = 0;
		mSsrc = 0;
		mPayloadType = 0;
	}
	
	void parseRTPPacket(byte[] rtp_packet)
	{
		mSequenceNumber = (rtp_packet[2]& 0xff) << 8 | rtp_packet[3] & 0xff;
		mTimestamp =  (rtp_packet[4] & 0xff) << 24 | (rtp_packet[5] & 0xff) << 16 | (rtp_packet[6] & 0xff) << 8 | rtp_packet[7] & 0xff;
		mSsrc =  rtp_packet[8] << 24 | rtp_packet[9] << 16 | rtp_packet[10] << 8 | rtp_packet[11]; 
		mPayloadType = rtp_packet[1] & 0x3f;	
	}
	
	public long getTimestamp() {
		return mTimestamp;
	}
	public int getSequenceNumber() {
		return mSequenceNumber;
	}
	
	public int getSSrc() {
		return mSsrc;
	}
	
	public int getPayloadType() {
		return mPayloadType;
	}

}
