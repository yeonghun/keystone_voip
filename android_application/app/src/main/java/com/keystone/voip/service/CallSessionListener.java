package com.keystone.voip.service;

public interface CallSessionListener {
    void onCallRequested(boolean passResult);
}
