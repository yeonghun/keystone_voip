package com.keystone.voip.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.keystone.voip.R;
import com.keystone.voip.account.Account;
import com.keystone.voip.state.CallState;
import com.keystone.voip.state.MessageState;
import com.keystone.voip.ui.CallActivity;
import com.keystone.voip.ui.ConferenceActivity;
import com.keystone.voip.ui.ContentsActivity;
import com.keystone.voip.ui.DBHelper;
import com.keystone.voip.ui.Utils;

import java.util.Map;

import static com.keystone.voip.ui.Utils.ACTION.ACT_PEER_READY;
import static com.keystone.voip.ui.Utils.ACTION.ACT_RECV_MSG;
import static com.keystone.voip.ui.Utils.ACTION.ACT_REJECT_CALL_RECEIVED;
import static com.keystone.voip.ui.Utils.CC_ATTENDANTS;
import static com.keystone.voip.ui.Utils.CC_DATE_TIME;
import static com.keystone.voip.ui.Utils.CC_ID;
import static com.keystone.voip.ui.Utils.IP;
import static com.keystone.voip.ui.Utils.PHONENUMBER;

public class KeystoneFirebaseMessagingService extends FirebaseMessagingService {

    //Firebase message
    private static final String FBM_MSG_TYPE = "msgType";
    private static final String FBM_MSG_REQ_PORT_OPEN = "INCOMING_CALL";
    private static final String FBM_MSG_CALL_READY = "CALL_READY";
    private static final String FBM_MSG_MSG_RECEIVE = "MESSAGE_REQUEST";
    private static final String FBM_MSG_CALL_REJECT = "CALL_REJECT";
    private static final String FBM_MSG_CC_SETUP = "CC_SETUP";
    private static final String FBM_MSG_CC_CHANGE = "CC_CHANGE";

    private static final String TAG = "FirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Map rxedData;
        // Check if message contains a data payload.
        Account.getInstance().retrieveSavedAccount(getApplicationContext());
        CallState.getInstance().setLocalIP(Utils.getLocalIP(getApplicationContext()));
        CallState.getInstance().setLocalPhoneNumber(Account.getInstance().getPhoneNumber());

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String msgtype = remoteMessage.getData().get(FBM_MSG_TYPE);

            if (FBM_MSG_REQ_PORT_OPEN.equals(msgtype)) {
                // callee set call id here
                String callID = remoteMessage.getData().get(Utils.CALL_ID);
                CallState.getInstance().setCallID(callID);
                // that's why we set caller IP here also
                String callerIP = remoteMessage.getData().get(Utils.CALLER_IP);
                CallState.getInstance().setRemoteIP(callerIP);
                String callerNum = remoteMessage.getData().get(Utils.FROM_PHONE_NUMBER);
                CallState.getInstance().setRemotePhoneNumber(callerNum);

                String remotePhoneNumber = remoteMessage.getData().get(Utils.FROM_PHONE_NUMBER);
                CallState.getInstance().setRemotePhoneNumber(remotePhoneNumber);
                Log.d(TAG, "revCallID: " + callID + ", callerIP: " + callerIP);
                Intent callAct = new Intent(this, CallActivity.class);
                callAct.putExtra("action", Utils.ACTION.ACT_REQ_PORT_OPEN);
                callAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callAct);
            } else if (FBM_MSG_CALL_READY.equals(msgtype)) {
                String revCallID = remoteMessage.getData().get(Utils.CALL_ID);
                String actCallID = CallState.getInstance().getCallID();
                Log.d(TAG, "revCallID: " + revCallID + ", actCallID: " + actCallID);
                if (revCallID.equals(actCallID)) {
                    Intent callReadyIntent = new Intent(this, CallService.class);
                    callReadyIntent.putExtra("action", ACT_PEER_READY);
                    startService(callReadyIntent);
                }
            } else if (FBM_MSG_CALL_REJECT.equals(msgtype)) {
                String revCallID = remoteMessage.getData().get(Utils.CALL_ID);
                String actCallID = CallState.getInstance().getCallID();
                Log.d(TAG, "revCallID: " + revCallID + ", actCallID: " + actCallID);
                if (revCallID.equals(actCallID)) {
                    Intent callReadyIntent = new Intent(this, CallService.class);
                    callReadyIntent.putExtra("action", ACT_REJECT_CALL_RECEIVED);
                    startService(callReadyIntent);
                }
            } else if (FBM_MSG_MSG_RECEIVE.equals(msgtype)) {
                try {
                    String phoneNumber = remoteMessage.getData().get(Utils.FROM_PHONE_NUMBER);
                    CallState.getInstance().setRemotePhoneNumber(phoneNumber);

                    String msgId = remoteMessage.getData().get(Utils.MSG_ID);

                    if (!MessageState.getInstance().isDuplicated(msgId)) {
                        MessageState.getInstance().addReceiveMsg(msgId);

                        String fromPN = remoteMessage.getData().get(Utils.MSG_FROM_PHONE_NUM);
                        String toPN = remoteMessage.getData().get(Utils.MSG_TO_PHONE_NUM);
                        String msg = remoteMessage.getData().get(Utils.MESSAGE);

                        Intent ReceiveMsgIntent = new Intent(this, MessageService.class);
                        ReceiveMsgIntent.putExtra("action", ACT_RECV_MSG);
                        ReceiveMsgIntent.putExtra(Utils.MSG_ID, msgId);
                        ReceiveMsgIntent.putExtra(Utils.MSG_FROM_PHONE_NUM, fromPN);
                        ReceiveMsgIntent.putExtra(Utils.MSG_TO_PHONE_NUM, toPN);
                        ReceiveMsgIntent.putExtra(Utils.MESSAGE, msg);
                        startService(ReceiveMsgIntent);
                        DBHelper db = new DBHelper(this);
                        String contactName = db.getNameFromNumber(fromPN);
                        if (contactName.isEmpty()) {
                            contactName = fromPN;
                        }

                        NotificationCompat.Builder builder =
                                new NotificationCompat.Builder(this)
                                        .setSmallIcon(R.drawable.noti_new_msg)
                                        .setContentTitle("New Message")
                                        .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
                                        .setContentText("You received message from:" + contactName);

        /*        Intent notificationIntent = new Intent(this, ContentsActivity.class);
                notificationIntent.setAction("ChatMessage");
                PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);*/

                        // Add as notification
                        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.notify(0, builder.build());
                    } else {
                        Log.d("Message :", "Duplicated" + msgId);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (FBM_MSG_CC_SETUP.equals(msgtype)) {
                Intent ccStart = new Intent(this, ConferenceCallService.class);
                ccStart.putExtra("action", Utils.ACTION.ACT_CC_SETUP);
                ccStart.putExtra(CC_ID, remoteMessage.getData().get(CC_ID));
                ccStart.putExtra(CC_ATTENDANTS, remoteMessage.getData().get(CC_ATTENDANTS));
                ccStart.putExtra(CC_DATE_TIME, remoteMessage.getData().get(CC_DATE_TIME));
                startService(ccStart);
                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.img_incoming)
                                .setContentTitle("Conf Call")
                                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary))
                                .setContentText("You got inivite for Conf call!!!");

               /* Intent notificationIntent = new Intent(this, ConferenceActivity.class);
                notificationIntent.setAction("joinConference");
                PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);*/

                // Add as notification
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(0, builder.build());

            } else if (FBM_MSG_CC_CHANGE.equals(msgtype)) {
                // TODO: implement for parsing msg and then add ip to conference call state
                Intent ccChange = new Intent(this, ConferenceCallService.class);
                ccChange.putExtra("action", Utils.ACTION.ACT_CC_CHANGE);
                ccChange.putExtra(CC_ID, remoteMessage.getData().get(CC_ID));
                ccChange.putExtra(PHONENUMBER, remoteMessage.getData().get(PHONENUMBER));
                ccChange.putExtra(IP, remoteMessage.getData().get(IP));
                startService(ccChange);
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }
}
