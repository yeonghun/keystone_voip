package com.keystone.voip.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;

import com.keystone.voip.R;

public class AccountActivity extends AppCompatActivity implements OnFragmentInteractionListener {
    private final String TAG = "AccountActivity";
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create an instance of ExampleFragment
            LoginFragment firstFragment = new LoginFragment();

            // In case this activity was started with special instructions from an Intent,
            // pass the Intent's extras to the fragment as arguments
            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, ConfigActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
   /* public native String stringFromJNI();*/

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public AccountActivity() {
        super();
    }

    @Override
    public void onActionPerformed(int fragmentName, int action, Bundle args) {
        if (fragmentName == Utils.Fragment.LOGIN) {
            launchContentsActivity();
            return;
        }  else if (fragmentName ==Utils.Fragment.CREATE_ACCOUNT){
            if (action == Utils.ACTION.ACT_LOGIN_SUCCESS) {
             launchContentsActivity();
             return;
            }
            setFragment(fragmentName);
        } else if (fragmentName == Utils.Fragment.FORGOT_PASSWORD) {
            if (action == Utils.ACTION.ACT_RESET_PWD_SUCCESS) {
                try {
                    getSupportFragmentManager().popBackStack();
                } catch (Exception e) {
                    setFragment(Utils.Fragment.LOGIN);
                }
                return;
            }
            setFragment(fragmentName);
        } else {
            setFragment(fragmentName);
        }

    }
    private void setFragment(int fragmentName) {
        BaseFragment newFragment = null;

       switch (fragmentName) {
            case Utils.Fragment.FORGOT_PASSWORD:
                newFragment = (BaseFragment) RecoverPwdFragment.newInstance("","");
                if (newFragment == null) return;
                break;
           case Utils.Fragment.CREATE_ACCOUNT:
               newFragment = (BaseFragment) CreateAccountFragment.newInstance("","");
               if (newFragment == null) return;
               break;
           case Utils.Fragment.LOGIN:
               newFragment = (BaseFragment) LoginFragment.newInstance("","");
               return;

        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (transaction == null) {
            Log.e(TAG, " transaction = null");
            return;
        }
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    }
     private void launchContentsActivity() {
        Intent accActivity = new Intent(this, ContentsActivity.class);
        startActivity(accActivity);
        finish();
    }
}
