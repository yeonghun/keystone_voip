package com.keystone.voip.ui;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;
import android.content.Context;

import com.keystone.voip.R;

import com.keystone.voip.account.Account;
import com.keystone.voip.state.CallState;
import com.keystone.voip.state.ConferenceCallState;

public class ContentsActivity extends AppCompatActivity implements OnFragmentInteractionListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_tab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);// two tab pages are retained on either side.

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        getSupportActionBar().setSubtitle(Account.getInstance().getPhoneNumber());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contents, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_userinfo) {
            Intent userInfoSettings = new Intent(this, SettingsActivity.class);
            userInfoSettings.setAction("userinfo");
            startActivity(userInfoSettings);
            return true;
        } else if (id == R.id.action_serverinfo) {
            startActivity(new Intent(this, ConfigActivity.class));
            return true;
        } else if (id == R.id.action_conference) {
           // startActivity(new Intent(this, ConferenceTestActivity.class));
            Intent setUpConference = new Intent(this, ConferenceActivity.class);
            setUpConference.setAction("setUpConference");
            startActivity(setUpConference);
        }
        else if (id == R.id.action_billinghistory) {
            Intent billingSettings = new Intent(this, SettingsActivity.class);
            billingSettings.setAction("billinghistory");
            startActivity(billingSettings);
            return true;
        } else if (id == R.id.action_callhistory) {
            Intent callHistorySettings = new Intent(this, SettingsActivity.class);
            callHistorySettings.setAction("callhistory");
            startActivity(callHistorySettings);
        } else if (id == R.id.action_settings) {
            Intent settings = new Intent(this, SettingsActivity.class);
            settings.setAction("settings");
            startActivity(settings);
        } else if (id == R.id.action_join_conference) {

            Intent joinConference = new Intent(this, ConferenceActivity.class);
            joinConference.setAction("joinConference");
            startActivity(joinConference);
        } else if (id == R.id.action_logout) {
            CallState.getInstance().clearAllState();
            ConferenceCallState.getInstance().clearAllState();
            Account.getInstance().clearSavedAccount(this);
            Intent accActivity = new Intent(this, AccountActivity.class);
            startActivity(accActivity);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onActionPerformed(int fragmentName, int action, Bundle args) {
        Intent callAct = new Intent(this, CallActivity.class);
        callAct.putExtra("action", Utils.ACTION.ACT_REQUEST_CALL);
        callAct.putExtra(Utils.TO_PHONE_NUMBER, args.getString(Utils.TO_PHONE_NUMBER));
        callAct.putExtra(Utils.CONTACT_NAME, args.getString(Utils.CONTACT_NAME));
        startActivity(callAct);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends BaseFragment  implements View.OnClickListener{
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String TAG = "PlaceholderFragment";
        private static final String ARG_SECTION_NUMBER = "section_number";
        private Button mCall_Btn;
        private EditText mPhoneNumber_etv;
        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_content_tab, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

            mCall_Btn = (Button) rootView.findViewById(R.id.call_btn);
            mCall_Btn.setOnClickListener(this);

            mPhoneNumber_etv = (EditText) rootView.findViewById(R.id.tempNumbereditor);

            return rootView;
        }

        @Override
        public void onClick(View v) {
            if (v == null) {
                Log.e(TAG, " onClick : v = null");
                return;
            }
            if(v == mCall_Btn) {
                Bundle args  = new Bundle();
                args.putString(Utils.TO_PHONE_NUMBER, mPhoneNumber_etv.getText().toString());
                mListener.onActionPerformed(0,0, args);
            }
        }
    }
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
            Fragment fragment = null;
            switch (position) {
                case 0:
                fragment =  new DialFragment();
                break;
                case 1:
                fragment =  new ContactsFragment();
                break;
                case 2:
                fragment =  new ChatFragment();
                break;
                default:
                fragment =  new DialFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
