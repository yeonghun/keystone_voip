package com.keystone.voip.ui;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.keystone.voip.R;
import com.keystone.voip.account.Account;
import com.keystone.voip.config.ConfigStore;
import com.keystone.voip.state.CallState;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RecoverPwdFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RecoverPwdFragment extends BaseFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private final String TAG = "RecoverPwdFragment";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextInputLayout mFirstName_tlayout;
    private TextInputEditText mFirstName_Etv;

    private TextInputLayout mLastName_tlayout;
    private TextInputEditText mLastName_Etv;

    private TextInputLayout mEmail_tlayout;
    private TextInputEditText mEmail_Etv;

    private TextInputLayout mPhoneNumber_tlayout;
    private TextInputEditText mPhoneNumber_Etv;

    private Button mVerifyBtn;


    private TextInputLayout mPassword_tlayout;
    private TextInputEditText mPassword_Etv;

    private TextInputLayout mConfirmPasswordtlayout;
    private TextInputEditText mConfirmPasswordEtv;
    private Button mResetPwdBtn;
    private String mPhonenumber;


    public RecoverPwdFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RecoverPwdFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RecoverPwdFragment newInstance(String param1, String param2) {
        RecoverPwdFragment fragment = new RecoverPwdFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View fragView = inflater.inflate(R.layout.recover_password_fragment, container, false);
        if (fragView == null)
        {
            Log.e("recoverfragement", "fragView = null");
            return null;
        }
        mFirstName_tlayout = (TextInputLayout) fragView.findViewById(R.id.first_tlayout);
        mFirstName_Etv = (TextInputEditText) fragView.findViewById(R.id.first_etv);

        mLastName_tlayout = (TextInputLayout) fragView.findViewById(R.id.last_tlayout);
        mLastName_Etv = (TextInputEditText) fragView.findViewById(R.id.last_etv);

        mEmail_tlayout = (TextInputLayout) fragView.findViewById(R.id.email_id_tlayout);
        mEmail_Etv = (TextInputEditText) fragView.findViewById(R.id.email_id_edittv);

        mPhoneNumber_tlayout = (TextInputLayout) fragView.findViewById(R.id.input_num_tlayout);
        mPhoneNumber_Etv = (TextInputEditText) fragView.findViewById(R.id.input_num_edittv);

        mVerifyBtn = (Button) fragView.findViewById(R.id.verify_btn);
        mVerifyBtn.setOnClickListener(this);

        mPassword_tlayout = (TextInputLayout) fragView.findViewById(R.id.new_pwd_tlayout);
        mPassword_Etv = (TextInputEditText) fragView.findViewById(R.id.new_pwd_edittv);

        mConfirmPasswordtlayout = (TextInputLayout) fragView.findViewById(R.id.conf_pwd_tlayout);
        mConfirmPasswordEtv = (TextInputEditText) fragView.findViewById(R.id.conf_pwd_edittv);

        mResetPwdBtn = (Button) fragView.findViewById(R.id.reset_btn);
        mResetPwdBtn.setOnClickListener(this);

        return fragView;
    }

    @Override
    public void onClick(View v) {
        if (v == null) {
            Log.e(TAG, " onClick : v = null");
            return;
        }
        if (v == mVerifyBtn) {
            if (validateInputFields() == false) {
                return;
            }
           new VerifyAccountTask().execute();
        } else if (v == mResetPwdBtn) {
            if (validatePassword() == false) {
                return;
            }
            new ResetPasswordTask().execute();
        }
    }
    private boolean validatePassword() {
        if ((mPassword_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.password), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mConfirmPasswordEtv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.confirm_password), Toast.LENGTH_LONG).show();
            return false;
        }
        if (mPassword_Etv.getText().toString().equals(mConfirmPasswordEtv.getText().toString()) == false) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_password), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    private boolean validateInputFields() {
        if ((mPhoneNumber_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.phonenumber), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mFirstName_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.first_name), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mLastName_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.last_name), Toast.LENGTH_LONG).show();
            return false;
        }
        if ((mEmail_Etv.getText().toString()).isEmpty()) {
            Toast.makeText(this.getContext(),getResources().getString(R.string.error_input_msg_name) + getResources().getString(R.string.email), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void showResetPwdUI() {

        if (mFirstName_tlayout != null) {
            mFirstName_tlayout.setVisibility(View.GONE);
        }
        if (mLastName_tlayout != null) {
            mLastName_tlayout.setVisibility(View.GONE);
        }
        if (mEmail_tlayout != null) {
            mEmail_tlayout.setVisibility(View.GONE);
        }
        if (mPhoneNumber_tlayout != null) {
            mPhoneNumber_tlayout.setVisibility(View.GONE);
        }
        if (mPassword_tlayout != null) {
            mPassword_tlayout.setVisibility(View.VISIBLE);
        }
        if (mConfirmPasswordtlayout != null) {
            mConfirmPasswordtlayout.setVisibility(View.VISIBLE);
        }
        if (mVerifyBtn != null) {
            mVerifyBtn.setVisibility(View.GONE);
        }
        if (mResetPwdBtn != null) {
            mResetPwdBtn.setVisibility(View.VISIBLE);
        };
    }
    private class VerifyAccountTask extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(RecoverPwdFragment.this.getContext());
        private final String FAIL = "fail";
        private final String PASS = "pass";
        private HttpURLConnection myConnection = null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pdLoading != null) {
                pdLoading.setMessage(getResources().getString(R.string.processing_msg));
                pdLoading.show();
            }
            mPhonenumber = mPhoneNumber_Etv.getText().toString();
        }
        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;
            try {

                URL serverURL = new URL(ConfigStore.Server.getAddress(getContext()) + "/user/verify");
                myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");


                //Build Json for USER data
                JSONObject jsonUserData = new JSONObject();
                jsonUserData.put(Utils.FIRST_NAME, mFirstName_Etv.getText().toString());
                jsonUserData.put(Utils.LAST_NAME, mLastName_Etv.getText().toString());
                jsonUserData.put(Utils.EMAIL, mEmail_Etv.getText().toString());
                jsonUserData.put(Utils.IP, Utils.getLocalIP(RecoverPwdFragment.this.getContext()));
                jsonUserData.put(Utils.REGID, FirebaseInstanceId.getInstance().getToken());
                jsonUserData.put(Utils.PHONENUMBER, mPhoneNumber_Etv.getText().toString());

                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonUserData.toString().getBytes());  // Write the data

                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    result = PASS;
                } else if (responseCode == 400) {
                    result = FAIL;
                }

            } catch (Exception e){
                result = FAIL;
                e.printStackTrace();

            } finally {
                if (myConnection != null) {
                    myConnection.disconnect();
                }
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pdLoading != null) {
                pdLoading.dismiss();
            }
            if (result.equals(FAIL))
            {
                Toast.makeText(RecoverPwdFragment.this.getContext(),getResources().getString(R.string.account_verify_fail), Toast.LENGTH_LONG).show();
                return;
            } else {
                Toast.makeText(RecoverPwdFragment.this.getContext(),getResources().getString(R.string.account_verify_pass), Toast.LENGTH_LONG).show();
                showResetPwdUI();
            }

        }

    }

    private class ResetPasswordTask extends AsyncTask<String, String, String>
    {
        ProgressDialog pdLoading = new ProgressDialog(RecoverPwdFragment.this.getContext());
        private final String FAIL = "fail";
        private final String PASS = "pass";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pdLoading != null) {
                pdLoading.setMessage(getResources().getString(R.string.processing_msg));
                pdLoading.show();
            }
        }
        @Override
        protected String doInBackground(String... params) {
            String result = FAIL;
            try {

                URL serverURL = new URL(ConfigStore.Server.getAddress(getContext()) + "/user/recover");
                HttpURLConnection myConnection = (HttpURLConnection) serverURL.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setRequestProperty("Content-Type", "application/json");
                myConnection.setRequestProperty("User-Agent", "keystone");


                //Build Json for USER data
                JSONObject jsonUserData = new JSONObject();
                jsonUserData.put(Utils.PASSWORD, mPassword_Etv.getText().toString());
                jsonUserData.put(Utils.PHONENUMBER, mPhonenumber);


                myConnection.setDoOutput(true);  // Enable writing
                myConnection.getOutputStream().write(jsonUserData.toString().getBytes());  // Write the data

                int responseCode = myConnection.getResponseCode();
                if (responseCode == 200) {
                    result = PASS;
                } else if (responseCode == 400) {
                    result = FAIL;
                }
            } catch (Exception e){
                result = FAIL;
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pdLoading != null) {
                pdLoading.dismiss();
            }
            if (result.equals(FAIL)) {
                Toast.makeText(RecoverPwdFragment.this.getContext(),getResources().getString(R.string.reset_password_fail), Toast.LENGTH_LONG).show();
                return;
            } else {
                Toast.makeText(RecoverPwdFragment.this.getContext(),getResources().getString(R.string.reset_password_success), Toast.LENGTH_LONG).show();

                Account.getInstance().setPassword(mPassword_Etv.getText().toString());
                Account.getInstance().saveAccount(RecoverPwdFragment.this.getContext());

                if (mListener == null) {
                    Log.e(TAG, " ResetPasswordTask onPostExecute : mListener = null");
                    return;
                }
                mListener.onActionPerformed(Utils.Fragment.FORGOT_PASSWORD, Utils.ACTION.ACT_RESET_PWD_SUCCESS, new Bundle());
            }

        }

    }
}
