package com.keystone.voip.ui;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.keystone.voip.R;

import java.util.List;

public class ContactsRecyclerViewAdapter extends RecyclerView.Adapter<ContactsRecyclerViewAdapter.ViewHolder> {
    private List<Contact> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    public static class CONTACT_ACTION {
        static public final int CALL = 0;
        static public final int EDIT = 1;
        static public final int DELETE = 2;
        static public final int LIST = 3;
    }
        // data is passed into the constructor
    ContactsRecyclerViewAdapter(Context context, List<Contact> data) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.contactlist_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contact contact = mData.get(position);
        holder.nameTv.setText(contact.getFirstName() + " " + contact.getLastName());
        final View v = holder.itemView;
        final int pos = position;

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                PopupMenu popup = new PopupMenu(mContext, v, Gravity.RIGHT);

                popup.inflate(R.menu.menu_contact_contextmenu);
                final View innerview = v;

                final Contact innerContact = mData.get(pos);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.call:
                                mClickListener.onItemClick(innerview, innerContact, pos,CONTACT_ACTION.CALL);
                                return true;
                            case R.id.edit_contact:
                                mClickListener.onItemClick(innerview, innerContact, pos,CONTACT_ACTION.EDIT);
                                return true;
                            case R.id.delete_contact:
                                mClickListener.onItemClick(innerview, innerContact, pos,CONTACT_ACTION.DELETE);
                                return true;
                                default:
                                return true;
                        }
                    }
                });

                popup.show();
                return true;
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        TextView nameTv;
        ImageView callImage;

        ViewHolder(View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.nameView);
            callImage = itemView.findViewById(R.id.imageView);
            callImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mClickListener != null) mClickListener.onItemClick(v, getItem(getAdapterPosition()), getAdapterPosition(),CONTACT_ACTION.CALL);
                }
            });
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getItem(getAdapterPosition()), getAdapterPosition(), CONTACT_ACTION.LIST);
        }

    }

    // convenience method for getting data at click position
    Contact getItem(int index) {
        return mData.get(index);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
    public void removeItem(int position) {
        mData.remove(position);
        notifyDataSetChanged();
    }
    public void updateItem(int position, Contact contact)
    {
        mData.set(position, contact);
        notifyDataSetChanged();

    }
    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, Contact contact, int position, int action);
    }
}
