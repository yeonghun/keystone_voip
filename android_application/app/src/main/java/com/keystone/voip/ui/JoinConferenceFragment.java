package com.keystone.voip.ui;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.keystone.voip.R;
import com.keystone.voip.service.ConferenceCallService;
import com.keystone.voip.state.ConferenceCallState;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * A simple {@link Fragment} subclass.
 */
public class JoinConferenceFragment extends BaseFragment implements View.OnClickListener , Observer {
    private final String TAG = "JoinConferenceFragment";
    private Button mCallButton;
    private Button mEndButton;
    private Button mBluetoothButton;
    private TextView mParti1;
    private TextView mParti2;
    private TextView mParti3;
    private TextView mParti4;
    private TextView mDateTv;
    private TextView mTimeTv;
    private List<String> mAttendeeNumList;

    private PowerManager.WakeLock proximityWakeLock;

    public JoinConferenceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragView = inflater.inflate(R.layout.fragment_join_conference, container, false);;
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
        mCallButton = (Button) fragView.findViewById(R.id.callbtn);
        mCallButton.setOnClickListener(this);

        mEndButton = (Button) fragView.findViewById(R.id.endbtn);
        mEndButton.setOnClickListener(this);
        mEndButton.setVisibility(View.INVISIBLE);


        mBluetoothButton = (Button) fragView.findViewById(R.id.bluetoothbtn);
        mBluetoothButton.setOnClickListener(this);
        mBluetoothButton.setVisibility(View.INVISIBLE);

        mParti1 = (TextView) fragView.findViewById(R.id.parti1);
        mParti1.setVisibility(View.INVISIBLE);

        mParti2 = (TextView) fragView.findViewById(R.id.parti2);
        mParti2.setVisibility(View.INVISIBLE);

        mParti3 = (TextView) fragView.findViewById(R.id.parti3);
        mParti3.setVisibility(View.INVISIBLE);

        mParti4 = (TextView) fragView.findViewById(R.id.parti4);
        mParti4.setVisibility(View.INVISIBLE);

        mDateTv = (TextView) fragView.findViewById(R.id.confDate);
        mTimeTv = (TextView) fragView.findViewById(R.id.confTime);
        ConferenceCallState.getInstance().addObserver(this);

        mAttendeeNumList = ConferenceCallState.getInstance().getAttendeeNumList();
        DBHelper db = new DBHelper(getContext());

        for (int i=0; i < mAttendeeNumList.size(); i++) {
          if (i == 0) {
              mParti1.setText(getNameFromNumber(mAttendeeNumList.get(i),db));
              mParti1.setVisibility(View.VISIBLE);
          } else if (i == 1) {
                mParti2.setText(getNameFromNumber(mAttendeeNumList.get(i),db));
                mParti2.setVisibility(View.VISIBLE);
            } else if (i == 2) {
                mParti3.setText(getNameFromNumber(mAttendeeNumList.get(i),db));
                mParti3.setVisibility(View.VISIBLE);
            } else if (i == 3) {
              mParti4.setText(getNameFromNumber(mAttendeeNumList.get(i),db));
              mParti4.setVisibility(View.VISIBLE);
          }
        }
        // "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        try {
            Date nDate = simpleDateformat.parse(ConferenceCallState.getInstance().getDatetime());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(nDate);
            String timeString = calendar.get(Calendar.HOUR_OF_DAY)+":"+ String.format("%02d",calendar.get(Calendar.MINUTE));
            String dateString = String.format("%02d",calendar.get(Calendar.DAY_OF_MONTH)) + "/" + String.format("%02d",(1+ calendar.get(Calendar.MONTH))) + "/" + calendar.get(Calendar.YEAR);

            mDateTv.setText(dateString);
            mTimeTv.setText(timeString);
        } catch (Exception e) {

        }

        return fragView;
    }

    @Override
    public void onClick(View v) {
          if (mCallButton == v) {
              enableProximityWakeLock();
              Intent start = new Intent(getActivity(), ConferenceCallService.class);
              start.putExtra("action", Utils.ACTION.ACT_CC_START);
              getActivity().startService(start);
              if  (mCallButton !=  null) {
                  mCallButton.setVisibility(View.INVISIBLE);
              }
              if (mEndButton != null) {
                  mEndButton.setVisibility(View.VISIBLE);
              }
              if (mBluetoothButton != null) {
                  mBluetoothButton.setVisibility(View.VISIBLE);
              }

          } else if (mEndButton == v) {
              disableProximityWakeLock();
              Intent end = new Intent(getActivity(), ConferenceCallService.class);
              end.putExtra("action", Utils.ACTION.ACT_CC_END);
              getActivity().startService(end);
              if  (mCallButton !=  null) {
                  mCallButton.setVisibility(View.VISIBLE);
              }
              if (mEndButton != null) {
                  mEndButton.setVisibility(View.INVISIBLE);
              }
              if (mBluetoothButton != null) {
                  mBluetoothButton.setVisibility(View.INVISIBLE);
              }
          } else if (mBluetoothButton == v) {
              mBluetoothButton.setSelected(!mBluetoothButton.isSelected());
              changeAudioOut();
              return;
          }
    }
    public boolean enableProximityWakeLock() {
        if (proximityWakeLock != null) {
            return true;
        }
        PowerManager powerManager = (PowerManager)
                getActivity().getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            proximityWakeLock = powerManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "keystone:proximityWaklock");
            proximityWakeLock.acquire();
        }
        return false;
    }

    public void disableProximityWakeLock() {
        if (proximityWakeLock != null) {
            proximityWakeLock.release();
            proximityWakeLock = null;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (ConferenceCallState.getInstance().getCcState() == ConferenceCallState.CcState.END) {
            ConferenceCallState.getInstance().clearAllState();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ConferenceCallState.getInstance().deleteObserver(this);
    }
    private String getNameFromNumber (String number , DBHelper db) {
        String name = "";
        if (db == null || number ==  null || number.isEmpty())
        {
            return number;
        } else {
            name =  db.getNameFromNumber(number);
            if (name.isEmpty() == false) {
                return name;
            }
        }
        return number;
    }

    private void changeAudioOut() {

        int mode = 0;
        if (mBluetoothButton.isSelected()) {
            mode = Utils.AUDIO_OUT_MODE.BLUETOOTH;
        } else {
            mode = Utils.AUDIO_OUT_MODE.EARPIECE;
        }

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this.getContext());
        Intent localIntent = new Intent(Utils.ACTION.toString(Utils.ACTION.ACT_CHANGE_AUDIO_MODE_CONFCALL));
        localIntent.putExtra("OUT", mode);
        localBroadcastManager.sendBroadcast(localIntent);
    }
}
