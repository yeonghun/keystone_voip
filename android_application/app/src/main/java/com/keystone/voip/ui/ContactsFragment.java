package com.keystone.voip.ui;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.keystone.voip.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends BaseFragment implements View.OnClickListener, ContactsRecyclerViewAdapter.ItemClickListener {
    private final String TAG = "ContactsFragment";
    DBHelper mContactDB = null;
    ArrayList<Contact> mContactList = null;

    ContactsRecyclerViewAdapter mRecycleAdapter;
    RecyclerView mRecyclerView = null;
    LinearLayout mAddContactLayout = null;

    public ContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContactDB = new DBHelper(getActivity());
        mContactList = mContactDB.getAllContacts();
        return;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.fragment_contacts, container, false);
        if (fragView == null)
        {
            Log.e(TAG, "fragView = null");
            return null;
        }
        mAddContactLayout = (LinearLayout) fragView.findViewById(R.id.add_contact_layout);
        mAddContactLayout.setOnClickListener(this);

        mRecyclerView = fragView.findViewById(R.id.contactlist);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mRecycleAdapter = new ContactsRecyclerViewAdapter(this.getContext(), mContactList);
        mRecycleAdapter.setClickListener(this);
        mRecyclerView.setAdapter(mRecycleAdapter);
        registerForContextMenu(mRecyclerView);
      //  mRecycleAdapter.
        return fragView;
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        final TextInputEditText FirstName_Etv;
        final TextInputEditText LastName_Etv;
        final TextInputEditText Email_Etv;
        final TextInputEditText PhoneNumber_Etv;
        final TextInputEditText Address_Etv;

        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.add_contact, null);

        FirstName_Etv = promptsView.findViewById(R.id.ad_first_etv);
        LastName_Etv = promptsView.findViewById(R.id.ad_last_etv);
        Email_Etv = promptsView.findViewById(R.id.ad_email_id_edittv);
        PhoneNumber_Etv = promptsView.findViewById(R.id.ad_phonenumber_edittv);
        Address_Etv = promptsView.findViewById(R.id.ad_address_edittv);
         if (v == mAddContactLayout) {
             AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                     getContext());
             alertDialogBuilder.setView(promptsView);

             alertDialogBuilder
                     .setCancelable(false)
                     .setPositiveButton("Add",null)
                     .setNegativeButton("Cancel",
                             new DialogInterface.OnClickListener() {
                                 public void onClick(DialogInterface dialog,int id) {
                                     dialog.cancel();
                                 }
                             });


             AlertDialog alertDialog = alertDialogBuilder.create();
             alertDialog.setTitle("Add Contact");
             alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                 @Override
                 public void onShow(final DialogInterface dialog) {
                     Button b = ((AlertDialog)dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                     b.setOnClickListener(new View.OnClickListener() {
                         @Override
                         public void onClick(View v) {
                             boolean valid = validateInputFields(FirstName_Etv.getText().toString(), LastName_Etv.getText().toString(),
                                     PhoneNumber_Etv.getText().toString());
                             if (valid == false) {
                                 return;
                             }
                             if (mContactDB != null) {
                                 mContactDB.insertContact(FirstName_Etv.getText().toString(),
                                         LastName_Etv.getText().toString(),
                                         PhoneNumber_Etv.getText().toString(),
                                         Email_Etv.getText().toString(),
                                         Address_Etv.getText().toString());

                                 Contact c = new Contact(0,FirstName_Etv.getText().toString(),
                                         LastName_Etv.getText().toString(),
                                         PhoneNumber_Etv.getText().toString(),
                                         Email_Etv.getText().toString(),
                                         Address_Etv.getText().toString());

                                 mContactList.add(c);
                                 mRecycleAdapter.notifyDataSetChanged();
                             }

                             if (dialog != null) {
                                 dialog.dismiss();
                         }
                         }
                     });
                 }
             });
             // show it
             alertDialog.show();
         }


    }

    @Override
    public void onItemClick(View view, Contact contact, int position, int action) {

        Bundle args  = new Bundle();
        switch (action) {

            case ContactsRecyclerViewAdapter.CONTACT_ACTION.CALL:
                args.putString(Utils.CONTACT_NAME, contact.getFirstName() + " " + contact.getLastName());
                args.putString(Utils.TO_PHONE_NUMBER, contact.getPhoneNumber());
                mListener.onActionPerformed(Utils.Fragment.CONTACTS, 0, args);
                return;

            case ContactsRecyclerViewAdapter.CONTACT_ACTION.EDIT:
                handleEditOrView(view, contact, position, ContactsRecyclerViewAdapter.CONTACT_ACTION.EDIT);
                return;

            case ContactsRecyclerViewAdapter.CONTACT_ACTION.DELETE:
                mContactDB.deleteContact(contact.getId());
                mRecycleAdapter.removeItem(position);
                return;

            case ContactsRecyclerViewAdapter.CONTACT_ACTION.LIST: //list item click
                handleEditOrView(view, contact, position, ContactsRecyclerViewAdapter.CONTACT_ACTION.LIST);
                return;
        }

    }
    private void handleEditOrView(View view, final Contact contact, final int position, final int action) {
        final TextInputEditText FirstName_Etv;
        final TextInputEditText LastName_Etv;
        final TextInputEditText Email_Etv;
        final TextInputEditText PhoneNumber_Etv;
        final TextInputEditText Address_Etv;

        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.add_contact, null);

        FirstName_Etv = promptsView.findViewById(R.id.ad_first_etv);
        FirstName_Etv.setText(contact.getFirstName());

        LastName_Etv = promptsView.findViewById(R.id.ad_last_etv);
        LastName_Etv.setText(contact.getLastName());

        Email_Etv = promptsView.findViewById(R.id.ad_email_id_edittv);
        Email_Etv.setText(contact.getEmail());

        PhoneNumber_Etv = promptsView.findViewById(R.id.ad_phonenumber_edittv);
        PhoneNumber_Etv.setText(contact.getPhoneNumber());

        Address_Etv = promptsView.findViewById(R.id.ad_address_edittv);
        Address_Etv.setText(contact.getAddress());

        //Create dialog
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());
        alertDialogBuilder.setView(promptsView);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Save",null)
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setTitle("Edit Contact");
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button b = ((AlertDialog)dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean valid = validateInputFields(FirstName_Etv.getText().toString(), LastName_Etv.getText().toString(),
                                PhoneNumber_Etv.getText().toString());
                        if (valid == false) {
                            return;
                        }
                        if (mContactDB != null) {
                            mContactDB.updateContact( contact.getId(),
                                    FirstName_Etv.getText().toString(),
                                    LastName_Etv.getText().toString(),
                                    PhoneNumber_Etv.getText().toString(),
                                    Email_Etv.getText().toString(),
                                    Address_Etv.getText().toString());

                    Contact c = new Contact(contact.getId(),FirstName_Etv.getText().toString(),
                                    LastName_Etv.getText().toString(),
                                    PhoneNumber_Etv.getText().toString(),
                                    Email_Etv.getText().toString(),
                                    Address_Etv.getText().toString());

                            mRecycleAdapter.updateItem(position, c);
                            mRecycleAdapter.notifyDataSetChanged();
                        }

                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                });
            }
        });
        // show it
        alertDialog.show();
    }
    private boolean validateInputFields(String firstName, String lastName, String phonenumber) {

        if (firstName.isEmpty() && lastName.isEmpty()) {
            Toast.makeText(this.getContext(),"Please entry atleast one name field", Toast.LENGTH_LONG).show();
            return false;
        }
       if ((phonenumber).isEmpty()) {
            Toast.makeText(this.getContext(),"Please entry Phone number field", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
