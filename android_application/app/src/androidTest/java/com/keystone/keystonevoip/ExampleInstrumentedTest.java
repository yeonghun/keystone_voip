package com.keystone.keystonevoip;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.keystone.voip.audio.VoIPAudioIo;
import com.keystone.voip.service.CallSessionHandler;
import com.keystone.voip.service.CallSessionListener;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        CallSessionHandler handler = new CallSessionHandler(appContext);
        List<String> numbers = new ArrayList<>();
        numbers.add("123");
        numbers.add("456");
        numbers.add("789");
        handler.setupConferenceCall("mynum","2018-06-20 22:00", numbers);
        handler.readyConferenceCall("myIp", "12333333");
        assertEquals("com.keystone.voip", appContext.getPackageName());
    }
}
