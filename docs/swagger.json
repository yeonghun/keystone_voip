{
  "swagger": "2.0",
  "info": {
    "description": "This is a VoIP Communication System, Keystone.\n\n  *Revision History*\n **- 0.1.0** initial version, add User(partial), Call(partial)\n **- 0.2.0** modify User, add Message\n",
    "version": "0.2.0",
    "title": "Keystone VoIP System",
    "termsOfService": "http://swagger.io/terms/",
    "contact": {
      "email": "nalouzi@gmail.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host": "127.0.0.1",
  "basePath": "/keystone",
  "tags": [
    {
      "name": "call",
      "description": "To initiate session"
    },
    {
      "name": "user",
      "description": "Operations about user"
    },
    {
      "name": "message",
      "description": "text message service"
    },
    {
      "name": "conference call",
      "description": "To setup conference call, TBD"
    },
    {
      "name": "bill",
      "description": "Billing Service, TBD"
    }
  ],
  "schemes": [
    "http"
  ],
  "paths": {
    "/message/{phoneNum}": {
      "get": {
        "tags": [
          "message"
        ],
        "summary": "Get text message if exists",
        "operationId": "getMsgByNum",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "path",
            "description": "receiver's phone number ",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password in encoded text",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "$ref": "#/definitions/Messages"
            }
          },
          "400": {
            "description": "Invalid phoneNum supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "post": {
        "tags": [
          "message"
        ],
        "summary": "request sending message",
        "operationId": "postMsgByNum",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "path",
            "description": "sender's phone number ",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password in encoded text",
            "required": true,
            "type": "string"
          },
          {
            "name": "msgbody",
            "in": "body",
            "schema": {
              "$ref": "#/definitions/Msg"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Invalid phoneNum supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/call/request": {
      "post": {
        "tags": [
          "call"
        ],
        "summary": "request a call",
        "operationId": "invite",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "query",
            "description": "The user phone number",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password in encoded text",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "callee info",
            "description": "initiates a VoIP dialog, destination phone number",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Call"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "object",
              "properties": {
                "callId": {
                  "type": "string",
                  "description": "callId"
                },
                "calleeIp": {
                  "type": "string",
                  "description": "IPaddress of callee"
                }
              }
            }
          }
        }
      }
    },
    "/call/accept": {
      "post": {
        "tags": [
          "call"
        ],
        "summary": "accept a call",
        "operationId": "accept",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "query",
            "description": "The user phone number",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password in encoded text",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "call id",
            "description": "accept call id",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CallId"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "404": {
            "description": "invalid call id"
          }
        }
      }
    },
    "/call/reject": {
      "post": {
        "tags": [
          "call"
        ],
        "summary": "reject a call",
        "operationId": "cancel",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "query",
            "description": "The user phone number",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password in encoded text",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "call info",
            "description": "reject call id",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CallId"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK"
          },
          "404": {
            "description": "invalid call id"
          }
        }
      }
    },
    "/user/create": {
      "post": {
        "tags": [
          "user"
        ],
        "summary": "Create user",
        "description": "User registration.",
        "operationId": "createUser",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "user object",
            "description": "Create user info",
            "required": true,
            "schema": {
              "$ref": "#/definitions/UserCreate"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "object",
              "properties": {
                "phoneNum": {
                  "type": "string",
                  "description": "phoneNumber"
                }
              }
            }
          }
        }
      }
    },
    "/user/login": {
      "post": {
        "tags": [
          "user"
        ],
        "summary": "Logs user into the system",
        "description": "",
        "operationId": "loginUser",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "query",
            "description": "The user phone number for login",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password for login in encoded text",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Invalid phoneNum/password supplied"
          }
        }
      }
    },
    "/user/logout": {
      "post": {
        "tags": [
          "user"
        ],
        "summary": "Logs out current logged in user session",
        "description": "",
        "operationId": "logoutUser",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "query",
            "description": "The user phone number for login",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "default": {
            "description": "successful operation"
          }
        }
      }
    },
    "/user/{phoneNum}": {
      "get": {
        "tags": [
          "user"
        ],
        "summary": "Get user by phoneNum",
        "description": "",
        "operationId": "getUserByNum",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "path",
            "description": "The name that needs to be fetched. ",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password in encoded text",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "$ref": "#/definitions/Buddy"
            }
          },
          "400": {
            "description": "Invalid phoneNum supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "put": {
        "tags": [
          "user"
        ],
        "summary": "Update user info",
        "description": "This can only be done by the logged in user.",
        "operationId": "updateUser",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "path",
            "description": "user's phone number that need to be updated",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password in encoded text",
            "required": true,
            "type": "string"
          },
          {
            "in": "body",
            "name": "body",
            "description": "send only information to update",
            "required": true,
            "schema": {
              "$ref": "#/definitions/User"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation, return updated user object",
            "schema": {
              "$ref": "#/definitions/User"
            }
          },
          "400": {
            "description": "Invalid user supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "delete": {
        "tags": [
          "user"
        ],
        "summary": "Delete user",
        "description": "This can only be done by the logged in user.",
        "operationId": "deleteUser",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "phoneNum",
            "in": "path",
            "description": "The name that needs to be deleted",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "query",
            "description": "The password in encoded text",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation"
          },
          "400": {
            "description": "Invalid username supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    }
  },
  "definitions": {
    "Call": {
      "type": "object",
      "properties": {
        "phoneNum": {
          "type": "string"
        }
      }
    },
    "CallId": {
      "type": "object",
      "properties": {
        "callId": {
          "type": "string"
        }
      }
    },
    "UserCreate": {
      "type": "object",
      "properties": {
        "firstName": {
          "type": "string"
        },
        "lastName": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "password": {
          "type": "string"
        }
      },
      "xml": {
        "name": "UserCreation"
      }
    },
    "User": {
      "type": "object",
      "properties": {
        "firstName": {
          "type": "string"
        },
        "lastName": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "password": {
          "type": "string"
        },
        "phoneNum": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        }
      },
      "xml": {
        "name": "User"
      }
    },
    "Buddy": {
      "type": "object",
      "properties": {
        "phoneNum": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        }
      }
    },
    "Messages": {
      "type": "object",
      "properties": {
        "totalNumOf": {
          "type": "string"
        },
        "msgs": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Msg"
          }
        }
      }
    },
    "Msg": {
      "type": "object",
      "properties": {
        "phoneNum": {
          "type": "string"
        },
        "msg": {
          "type": "string"
        }
      }
    },
    "ApiResponse": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "type": {
          "type": "string"
        },
        "message": {
          "type": "string"
        }
      }
    }
  },
  "externalDocs": {
    "description": "Find out more about Swagger",
    "url": "http://swagger.io"
  }
}