package com.example.airdrive.soundpoolexample;

import android.app.Activity;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

    Button btnPlay;
    SoundPool soundPool;
    AudioManager audioManager;
    int[] soundId;
    int lastLoaded = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPlay = (Button)findViewById(R.id.play);
        btnPlay.setEnabled(false);
        btnPlay.setOnClickListener(btnPlayOnClickListener);

        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        //the maximum number of simultaneous streams for this SoundPool object
        int maxStreams = 8;
        //the audio stream type as described in AudioManager
        int streamType = AudioManager.STREAM_MUSIC;
        //the sample-rate converter quality. Currently has no effect. Use 0 for the default.
        int srcQuality = 0;

        //soundPool = new SoundPool(maxStreams, streamType, srcQuality);
        AudioAttributes audioAttrib = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();

        SoundPool.Builder builder= new SoundPool.Builder();
        builder.setAudioAttributes(audioAttrib).setMaxStreams(maxStreams);

        soundPool = builder.build();

        soundPool.setOnLoadCompleteListener(soundPoolOnLoadCompleteListener);
        soundId = new int[5];
        soundId[0] = soundPool.load(this, R.raw.piano11, 1);
        soundId[1] = soundPool.load(this, R.raw.piano15, 2);
        soundId[2] = soundPool.load(this, R.raw.piano18, 3);
        soundId[3] = soundPool.load(this, R.raw.piano123, 4);
        soundId[4] = soundPool.load(this, R.raw.equinox, 5);

    }

    OnLoadCompleteListener soundPoolOnLoadCompleteListener =
            new OnLoadCompleteListener(){

                @Override
                public void onLoadComplete(SoundPool soundPool,
                                           int sampleId, int status) {
                    if(status==0){
                        btnPlay.setEnabled(true);
                    }else{
                        Toast.makeText(MainActivity.this,
                                "SoundPool.load() fail",
                                Toast.LENGTH_LONG).show();
                    }

                }
            };

    OnClickListener btnPlayOnClickListener =
            new OnClickListener(){

                @Override
                public void onClick(View v) {
                    if (lastLoaded < soundId.length) {
                        float vol = audioManager.getStreamVolume(
                                AudioManager.STREAM_MUSIC);
                        float maxVol = audioManager.getStreamMaxVolume(
                                AudioManager.STREAM_MUSIC);
                        float leftVolume = vol / maxVol;
                        float rightVolume = vol / maxVol;
                        int priority = 1;
                        int no_loop = -1;
                        float normal_playback_rate = 1f;
                        soundPool.play(soundId[lastLoaded],
                                leftVolume,
                                rightVolume,
                                priority,
                                no_loop,
                                normal_playback_rate);
                        lastLoaded++;
                    } else {
                        int idx = 0;
                        while (idx < soundId.length) {
                            lastLoaded--;
                            soundPool.stop(soundId[lastLoaded]);
                            idx++;
                        }
                    }
                }
            };

}