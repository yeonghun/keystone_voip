package hello.keystone.conference;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface AttendantRepository extends CrudRepository<Attendant, AttendantIdentity> {

	
	List<Attendant> findByConferenceId(Long conferenceId);
	List<Attendant> findByPhoneNum(String phoneNum);

	@Query(value = "select * from attendant a where a.phone_num = ?1 and DATE_FORMAT(a.create_time, '%Y%m') = ?2 and end_time is not null  ", nativeQuery = true)
	List<Attendant> findConferenceBillHistory(String phoneNum, String month);
	
}
