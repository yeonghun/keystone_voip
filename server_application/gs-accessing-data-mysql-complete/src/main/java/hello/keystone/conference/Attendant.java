package hello.keystone.conference;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity(name="Attendant")
@IdClass(AttendantIdentity.class)
public class Attendant {

	@Id
	private Long conferenceId;
	@Id
	private String phoneNum;
	@CreationTimestamp
   	private Timestamp createTime;
   	@UpdateTimestamp
   	private Timestamp lastUpdateTime;
   	private Timestamp startTime;
   	private Timestamp endTime;
   	private int talktime;
   	private double unitCost;
   	private double cost = 0.00;
	
	public Long getConferenceId() {
		return conferenceId;
	}
	public void setConferenceId(Long conferenceId) {
		this.conferenceId = conferenceId;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public Timestamp getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Timestamp lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public int getTalktime() {
		return talktime;
	}
	public void setTalktime(int talktime) {
		this.talktime = talktime;
	}
	public void calculateTalktime() {
		long milliseconds = getEndTime().getTime() - getStartTime().getTime();
		this.talktime = (int) milliseconds / 1000;
		this.cost = ((double)this.talktime / (double)60) * this.unitCost;
	}
	
	public double getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	@Override
	public String toString() {
		return "Attendant [conferenceId=" + conferenceId + ", phoneNum=" + phoneNum + ", createTime=" + createTime
				+ ", lastUpdateTime=" + lastUpdateTime + ", startTime=" + startTime + ", endTime=" + endTime
				+ ", talktime=" + talktime + ", unitCost=" + unitCost + ", cost=" + cost + "]";
	}

	
}
