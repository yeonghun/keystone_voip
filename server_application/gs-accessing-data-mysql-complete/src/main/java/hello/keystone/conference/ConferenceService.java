package hello.keystone.conference;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hello.keystone.bill.BillConfigRepository;
import hello.keystone.push.PushManager;
import hello.keystone.user.UserRepository;

@Service
@Transactional(readOnly = true)
public class ConferenceService {

	@Autowired 
	private ConferenceRepository conferenceRepository;
	@Autowired 
	private AttendantRepository attendantRepository;
	@Autowired 
	private UserRepository userRepository;
	@Autowired
	private BillConfigRepository billConfigRepository;
	
	/*---------------------------------------------------------------------
	 * Conference Setup
	 *--------------------------------------------------------------------- */
    @Transactional
    public ConferenceInfo setup(ConferenceInfo conferenceInfo) {
    	
    	/*----------------------------------------------------------
    	 * Validation of Parameter
	    ----------------------------------------------------------*/
    	if(!validatePhoneNumber(conferenceInfo)) {
    		return null;
    	}
    	
    	/*----------------------------------------------------------
    	 * Creating Conference ID
	    ----------------------------------------------------------*/
    	conferenceRepository.save(conferenceInfo.getConference());
		   
    	/*----------------------------------------------------------
    	 * 1. Allocating ConferenceID to Attendants
    	 * 2. Creating Attendants
	    ----------------------------------------------------------*/
    	if(conferenceInfo.allocateConferenceId()) {
    		attendantRepository.saveAll(conferenceInfo.getAttendants());
    	}else {
    		return null;
    	}
    	
    	/*----------------------------------------------------------
    	 * Push Msg
	    ----------------------------------------------------------*/
    	/*---------------------------------------------------------------------
		 * PUSH 
		 *--------------------------------------------------------------------- */
    	
    	//System.out.println(conferenceInfo.makePushMsg("ffff", "dddd"));
    	
    	PushManager pushManager = new PushManager();
    	for(Attendant attendant : conferenceInfo.getAttendants()) {
    		
    		pushManager.sendPushMsg("CC_SETUP", conferenceInfo, userRepository.findById(attendant.getPhoneNum()).get().getRegId());

    	}
	   
	   
    	return conferenceInfo;
		   
    	//Optional<Attendant> temp = attendantRepository.findById(new AttendantIdentity((long) 3.0, "5038630006"));
    	//System.out.println("fff"+ temp.get().toString());
		   
    	//List<Attendant> temp2 = attendantRepository.findByConferenceId((long) 3.0);
    	//System.out.println("ddd" + temp2.toString());
		   
    	//List<Attendant> temp3 = attendantRepository.findByPhoneNum("5038630006");
    	//System.out.println("eee" + temp3.toString());
		   
    }
    
    /*---------------------------------------------------------------------
	 * Conference Ready
	 *--------------------------------------------------------------------- */
    @Transactional
    public Conference ready(Conference conference) {
    	
    	if(conference.getConferenceId() == null) {
    		return null;
    	}
    	/*----------------------------------------------------------
    	 * Validation of Parameter
	    ----------------------------------------------------------*/
    	if(!userRepository.existsById(conference.getPhoneNum())) {
    		return null;
    	}
    	if(!conferenceRepository.existsById(conference.getConferenceId())) {
    		return null;
    	}
    	
    	conference.setIp(userRepository.findById(conference.getPhoneNum()).get().getIp());
  
    	PushManager pushManager = new PushManager();

    	List<Attendant> attendants = attendantRepository.findByConferenceId(conference.getConferenceId());
    	
    	
    	for(Attendant attendant : attendants) {
    		if(conference.getPhoneNum().equals(attendant.getPhoneNum())) {
    			attendant.setStartTime(new Timestamp(System.currentTimeMillis()));
    			attendantRepository.save(attendant);
    		}else {
    			pushManager.sendPushMsg("CC_CHANGE", conference, userRepository.findById(attendant.getPhoneNum()).get().getRegId());
    		}
    	}
    	
    	return conference;
		   
    	//Optional<Attendant> temp = attendantRepository.findById(new AttendantIdentity((long) 3.0, "5038630006"));
    	//System.out.println("fff"+ temp.get().toString());
		   
    	//List<Attendant> temp2 = attendantRepository.findByConferenceId((long) 3.0);
    	//System.out.println("ddd" + temp2.toString());
		   
    	//List<Attendant> temp3 = attendantRepository.findByPhoneNum("5038630006");
    	//System.out.println("eee" + temp3.toString());
		   
    }
    
    /*---------------------------------------------------------------------
	 * Conference End
	 *--------------------------------------------------------------------- */
    @Transactional
    public Conference end(Conference conference) {
    	
    	/*----------------------------------------------------------
    	 * Validation of Parameter
	    ----------------------------------------------------------*/
    	if(!userRepository.existsById(conference.getPhoneNum())) {
    		return null;
    	}
    	if(!conferenceRepository.existsById(conference.getConferenceId())) {
    		return null;
    	}
    	if(!attendantRepository.existsById(new AttendantIdentity(conference.getConferenceId(), conference.getPhoneNum()))) {
    		return null;
    	}

    	Attendant attendant = attendantRepository.findById(new AttendantIdentity(conference.getConferenceId(), conference.getPhoneNum())).get();
  
    	attendant.setEndTime(new Timestamp(System.currentTimeMillis()));
    	attendant.setUnitCost(billConfigRepository.findCurrentBillConfig().getCallPrice());
    	attendant.calculateTalktime();
    	
    	attendantRepository.save(attendant);
    	
    	return conference;
		   
		   
    }
    
    private boolean validatePhoneNumber(ConferenceInfo conferenceInfo) {
    	
    	for(Attendant attendant : conferenceInfo.getAttendants()) {
    		
    		if(attendant.getPhoneNum() == null) {
    			System.out.println("PhoneNum is null!");
    			return false;
    		}
    		
    		if(!userRepository.existsById(attendant.getPhoneNum())) {
    			System.out.println("PhoneNum dose not exist!");
    			return false;
    		}
    		
    	}
    	
    	return true;
    }
   
}
