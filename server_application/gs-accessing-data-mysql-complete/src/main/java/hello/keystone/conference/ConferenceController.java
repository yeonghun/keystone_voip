package hello.keystone.conference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/conference")
public class ConferenceController {

   @Autowired
   private ConferenceService conferenceService;

   /*----------------------------------------------------------*/
   /*---Setup Conference                                       */
   /*----------------------------------------------------------*/
   @PostMapping("/setup")
   public ResponseEntity<?> setup(@RequestBody ConferenceInfo conferenceInfo) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Conference/Setup/Input] : " + conferenceInfo.toString());
	   ConferenceInfo returnConference = conferenceService.setup(conferenceInfo);
	   if(returnConference == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("phonenumber does not exist");
	   }
	   System.out.println("[Response] :" + conferenceInfo.toString());
	   
	   return ResponseEntity.ok().body(conferenceInfo);
   }
   
   /*----------------------------------------------------------*/
   /*---Ready Conference                                           */
   /*----------------------------------------------------------*/
   @PostMapping("/ready")
   public ResponseEntity<?> ready(@RequestBody Conference conference) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Conference/Ready/Input] : " + conference.toString());
	   Conference returnConference = conferenceService.ready(conference);
	   //if(returnConference == null) {
		//   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("phonenumber does not exist");
	   //}
	   //System.out.println("[Response] :" + conferenceInfo.toString());
	   return ResponseEntity.ok().body(returnConference);
   }
   
   /*----------------------------------------------------------*/
   /*---Ready Conference                                           */
   /*----------------------------------------------------------*/
   @PostMapping("/end")
   public ResponseEntity<?> end(@RequestBody Conference conference) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Conference/End/Input] : " + conference.toString());
	   Conference returnConference = conferenceService.end(conference);
	   //if(returnConference == null) {
		//   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("phonenumber does not exist");
	   //}
	   //System.out.println("[Response] :" + conferenceInfo.toString());
	   return ResponseEntity.ok().body(returnConference);
   }

   
   
   
}