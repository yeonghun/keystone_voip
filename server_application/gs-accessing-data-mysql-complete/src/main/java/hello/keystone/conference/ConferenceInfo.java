package hello.keystone.conference;

import java.util.ArrayList;
import java.util.List;

import hello.keystone.push.Push;
public class ConferenceInfo extends Push {

	private Conference conference;

	public List<Attendant> attendants = new ArrayList<Attendant>();
	 
    public List<Attendant> addAttendant(Attendant attendant) {
    	attendants.add(attendant);
        return attendants;
    }

	public Conference getConference() {
		return conference;
	}

	public void setConference(Conference conference) {
		this.conference = conference;
	}

	public List<Attendant> getAttendants() {
		return attendants;
	}

	public void setAttendants(List<Attendant> attendants) {
		this.attendants = attendants;
	}
	

	@Override
	public String toString() {
		return "ConferenceInfo [conference=" + conference + ", attendants=" + attendants + "]";
	}

	public boolean allocateConferenceId() {
		
		if(this.conference.getConferenceId() == null) {
			return false;
		}
		
		for(Attendant attendant : this.attendants) {
			attendant.setConferenceId(this.conference.getConferenceId());
		}
		
		return true;
	}

	@Override
	public String makePushMsg(String msgType, String regId) {
		
		int lastCnt = 0;
		int cnt = this.attendants.size();
		
		String result = "{ \"data\": {\n" +
				"	    \"msgType\": \"" + msgType  + "\",\n" + 
				//"       \"conference\": {\n" + 
				"        				\"conferenceId\": " + this.conference.getConferenceId()  + " ,\n" + 
				"        				\"planTime\": \""+ this.conference.getPlanTime()     +"\",\n" + 
				//"    				  },\n" + 
				"	    \"attendants\": [\n";
				
				for(Attendant attendant : this.attendants) {
					lastCnt ++;
					result = result + 
							attendant.getPhoneNum() ;
					if(lastCnt < cnt) {
						result = result + ",";
					}else {
						result = result + "\n";
					}
				}
				result = result + 
			    "					  ]" ;
				result = result +
				"	  },\n" + 
			    "	  \"to\" : \""+ regId +"\"\n" + 
				"}";

				return result;
		
	}

}
