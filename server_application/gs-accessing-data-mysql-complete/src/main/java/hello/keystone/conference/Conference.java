package hello.keystone.conference;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import hello.keystone.push.Push;

@Entity(name="Conference")
public class Conference extends Push {

	@Id
   	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long conferenceId;
	private Timestamp planTime;
	private String phoneNum; 
	private String ip;
	
	public Long getConferenceId() {
		return conferenceId;
	}
	public void setConferenceId(Long conferenceId) {
		this.conferenceId = conferenceId;
	}
	public Timestamp getPlanTime() {
		return planTime;
	}
	public void setPlanTime(Timestamp planTime) {
		this.planTime = planTime;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	@Override
	public String toString() {
		return "Conference [conferenceId=" + conferenceId + ", planTime=" + planTime + ", phoneNum=" + phoneNum
				+ ", ip=" + ip + "]";
	}
	
	@Override
	public String makePushMsg(String msgType, String regId) {

		String result = "{ \"data\": {\n" +
				"	    \"msgType\": \"" + msgType  + "\",\n" + 
				"	    \"conferenceId\": \"" + this.getConferenceId()  + "\",\n" +
				"	    \"phoneNum\": \""+ this.getPhoneNum() +  "\",\n" +
				"	    \"ip\": \""+ this.getIp() +  "\"\n" +
				"	  },\n" + 
			    "	  \"to\" : \""+ regId +"\"\n" + 
				"}";
		
		return result;
		
	}
	
	
	
}
