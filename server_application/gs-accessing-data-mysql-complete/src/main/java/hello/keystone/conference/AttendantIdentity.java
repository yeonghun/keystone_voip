package hello.keystone.conference;

import java.io.Serializable;
import java.util.Objects;


public class AttendantIdentity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1233926765802253347L;
	
	private Long conferenceId;
	private String phoneNum;
	
	public AttendantIdentity() {
		
	}

	public AttendantIdentity(Long conferenceId, String phoneNum) {
		super();
		this.conferenceId = conferenceId;
		this.phoneNum = phoneNum;
	}

	@Override
	public int hashCode() {
		return Objects.hash(conferenceId, phoneNum);
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		AttendantIdentity taskId1 = (AttendantIdentity) obj;
		if (conferenceId != taskId1.conferenceId) return false;
		return phoneNum == taskId1.phoneNum;
	}
	
	
	
	
}
