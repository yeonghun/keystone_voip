package hello.keystone.call;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/call")
public class CallController {

   @Autowired
   private CallService callService;

   /*----------------------------------------------------------*/
   /*---Request Call                                           */
   /*----------------------------------------------------------*/
   @PostMapping("/request")
   public ResponseEntity<?> request(@RequestBody Call call) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Call/Request/Input] : " + call.toString());
	   Call returnCall = callService.request(call);
	   if(returnCall == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Caller's or Callee's phonenumber does not exist");
	   }
	   System.out.println("[Response] :" + returnCall.toString());
	   return ResponseEntity.ok().body(returnCall);
   }
   
   /*----------------------------------------------------------*/
   /*---Accept Call                                           */
   /*----------------------------------------------------------*/
   @PostMapping("/accept")
   public ResponseEntity<?> accept(@RequestBody Call call) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Call/accept/Input] : " + call.toString());
	   Call returnCall = callService.accept(call);
	   if(returnCall == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Call ID does not exist");
	   }
	   System.out.println("[Response] :" + returnCall.toString());
	   return ResponseEntity.ok().body(returnCall);
   }
   
   /*----------------------------------------------------------*/
   /*---Reject Call                                           */
   /*----------------------------------------------------------*/
   @PostMapping("/reject")
   public ResponseEntity<?> reject(@RequestBody Call call) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Call/reject/Input] : " + call.toString());
	   Call returnCall = callService.reject(call);
	   if(returnCall == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Call ID does not exist");
	   }
	   System.out.println("[Response] :" + returnCall.toString());
	   return ResponseEntity.ok().body(returnCall);
   }
   
   /*----------------------------------------------------------*/
   /*---End Call                                               */
   /*----------------------------------------------------------*/
   @PostMapping("/end")
   public ResponseEntity<?> end(@RequestBody Call call) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Call/End/Input] : " + call.toString());
	   Call returnCall = callService.end(call);
	   if(returnCall == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Call ID does not exist");
	   }
	   System.out.println("[Response] :" + returnCall.toString());
	   return ResponseEntity.ok().body(returnCall);
   }
   
   /*----------------------------------------------------------*/
   /*---Timeout Call                                               */
   /*----------------------------------------------------------*/
   @PostMapping("/timeout")
   public ResponseEntity<?> timeout(@RequestBody Call call) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Call/Timeout/Input] : " + call.toString());
	   Call returnCall = callService.timeout(call);
	   if(returnCall == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Call ID does not exist");
	   }
	   System.out.println("[Response] :" + returnCall.toString());
	   return ResponseEntity.ok().body(returnCall);
   }
   
   /*----------------------------------------------------------*/
   /*---Hearbeat Call                                               */
   /*----------------------------------------------------------*/
   @PostMapping("/heartbeat")
   public ResponseEntity<?> heartbeat(@RequestBody Call call) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Call/heartbeat/Input] : " + call.toString());
	   Call returnCall = callService.heartbeat(call);
	   if(returnCall == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Hearbeat Failed. Call ID does not exist");
	   }
	   System.out.println("[Response] :" + returnCall.toString());
	   return ResponseEntity.ok().body(returnCall);
   }
   
}