package hello.keystone.call;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface CallRepository extends CrudRepository<Call, Long> {

	List<Call> findByFromPhoneNum(String phoneNum);
	List<Call> findByToPhoneNum(String phoneNum);
	
	@Query(value = "select * from call1 a where a.received_yn = ?1 and a.end_yn = ?2 and a.cost is not null and a.date like ?3 and a.from_phone_num = ?4 ", nativeQuery = true)
	List<Call> findCallBillHistory(String receivedYn, String endYn, String month, String fromPhoneNum);
	
}
