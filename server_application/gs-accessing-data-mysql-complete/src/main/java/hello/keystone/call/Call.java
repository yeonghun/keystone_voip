package hello.keystone.call;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import hello.keystone.push.Push;

@Entity(name="Call1")
public class Call extends Push {

	@Id
   	@GeneratedValue(strategy = GenerationType.IDENTITY)
   	private Long callId;
   	private String fromPhoneNum;
   	private String toPhoneNum;
   	private String fromIp;
   	private String toIp;
   	private String fromRegId;
   	private String toRegId;
   	private String ReceivedYn;
   	private String endYn;
   	private String date;  
   	@CreationTimestamp
   	private Timestamp createTime;
   	@UpdateTimestamp
   	private Timestamp lastUpdateTime;
   	private Timestamp acceptedTime;
   	private Timestamp endTime;
   	private int talktime;
   	private String timeoutYn = "N";
   	private int heartbeat = 0;
   	private double unitCost;
   	private double cost = 0.00;
   	private String status;

	public Long getCallId() {
		return callId;
	}

	public void setCallId(Long callId) {
		this.callId = callId;
	}

	public String getFromPhoneNum() {
		return fromPhoneNum;
	}

	public void setFromPhoneNum(String fromPhoneNum) {
		this.fromPhoneNum = fromPhoneNum;
	}

	public String getToPhoneNum() {
		return toPhoneNum;
	}

	public void setToPhoneNum(String toPhoneNum) {
		this.toPhoneNum = toPhoneNum;
	}

	public String getFromIp() {
		return fromIp;
	}

	public void setFromIp(String fromIp) {
		this.fromIp = fromIp;
	}

	public String getToIp() {
		return toIp;
	}

	public void setToIp(String toIp) {
		this.toIp = toIp;
	}

	public String getFromRegId() {
		return fromRegId;
	}

	public void setFromRegId(String fromRegId) {
		this.fromRegId = fromRegId;
	}

	public String getToRegId() {
		return toRegId;
	}

	public void setToRegId(String toRegId) {
		this.toRegId = toRegId;
	}

	public String getReceivedYn() {
		return ReceivedYn;
	}

	public void setReceivedYn(String receivedYn) {
		ReceivedYn = receivedYn;
	}
	
	public String getEndYn() {
		return endYn;
	}

	public void setEndYn(String endYn) {
		this.endYn = endYn;
	}
	
	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Timestamp lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Timestamp getAcceptedTime() {
		return acceptedTime;
	}

	public void setAcceptedTime(Timestamp acceptedTime) {		
		this.acceptedTime = acceptedTime;
	}
	
	public void setDateFromTime() {
		this.date = new SimpleDateFormat("yyyyMMdd").format(getCreateTime());;
	}
	public int getTalktime() {
		return talktime;
	}

	public void setTalktime(int talktime) {
		this.talktime = talktime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public void calculateTalktime() {
		long milliseconds = getEndTime().getTime() - getAcceptedTime().getTime();
		this.talktime = (int) milliseconds / 1000;
		this.cost = ((double)this.talktime / (double)60) * this.unitCost;
	}
	public String getTimeoutYn() {
		return timeoutYn;
	}

	public void setTimeoutYn(String timeoutYn) {
		this.timeoutYn = timeoutYn;
	}
	
	public int getHeartbeat() {
		return heartbeat;
	}

	public void setHeartbeat(int heartbeat) {
		this.heartbeat = heartbeat;
	}

	

	public double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	@Override
	public String toString() {
		return "Call [callId=" + callId + ", fromPhoneNum=" + fromPhoneNum + ", toPhoneNum=" + toPhoneNum + ", fromIp="
				+ fromIp + ", toIp=" + toIp + ", fromRegId=" + fromRegId + ", toRegId=" + toRegId + ", ReceivedYn="
				+ ReceivedYn + ", endYn=" + endYn + ", date=" + date + ", createTime=" + createTime
				+ ", lastUpdateTime=" + lastUpdateTime + ", acceptedTime=" + acceptedTime + ", endTime=" + endTime
				+ ", talktime=" + talktime + ", timeoutYn=" + timeoutYn + ", heartbeat=" + heartbeat + ", unitCost="
				+ unitCost + ", cost=" + cost + ", status=" + status + "]";
	}

	@Override
	public String makePushMsg(String msgType, String regId) {
		String result = "{ \"data\": {\n" +
						"	    \"msgType\": \"" + msgType  + "\",\n" + 
						"	    \"callId\": \"" + this.callId  + "\",\n" +
						"	    \"fromPhoneNum\": \""+ this.getFromPhoneNum() +  "\",\n" +
						"	    \"toPhoneNum\": \""+ this.getToPhoneNum() +  "\",\n" +
						"	    \"fromIp\": \""+ this.getFromIp() +  "\"\n" +
						"	  },\n" + 
						"    \"time_to_live\" : 40,\n" +
					    "	  \"to\" : \""+ regId +"\"\n" + 
						"}";
		return result;
	}
   
	
}
