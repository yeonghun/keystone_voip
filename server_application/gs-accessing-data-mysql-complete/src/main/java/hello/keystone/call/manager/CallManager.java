package hello.keystone.call.manager;

import java.util.Timer;
import java.util.TimerTask;

import hello.TimeTask;

public class CallManager {

	TimerTask timerTask = new TimeTask();

	
	public void run() {
		//running timer task as daemon thread
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, 0, 10*1000);
		System.out.println("TimerTask started");
		//cancel after sometime
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		timer.cancel();
		System.out.println("TimerTask cancelled");
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	   
	
}
