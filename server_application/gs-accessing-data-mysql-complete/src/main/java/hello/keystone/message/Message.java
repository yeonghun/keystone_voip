package hello.keystone.message;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import hello.keystone.push.Push;

@Entity(name="Message")
public class Message extends Push {

	@Id
   	@GeneratedValue(strategy = GenerationType.IDENTITY)
   	private Long msgId;
   	private String fromPhoneNum;
   	private String toPhoneNum;
   	private String fromRegId;
   	private String toRegId;
   	private String msg;
   	private String ReceivedYn;
   	private double unitCost;
   	private double cost = 0.00;
   	private String date; 
   	@CreationTimestamp
   	private Timestamp createTime;
   	@UpdateTimestamp
   	private Timestamp lastUpdateTime;
   	
	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public String getFromPhoneNum() {
		return fromPhoneNum;
	}

	public void setFromPhoneNum(String fromPhoneNum) {
		this.fromPhoneNum = fromPhoneNum;
	}

	public String getToPhoneNum() {
		return toPhoneNum;
	}

	public void setToPhoneNum(String toPhoneNum) {
		this.toPhoneNum = toPhoneNum;
	}

	public String getFromRegId() {
		return fromRegId;
	}

	public void setFromRegId(String fromRegId) {
		this.fromRegId = fromRegId;
	}

	public String getToRegId() {
		return toRegId;
	}

	public void setToRegId(String toRegId) {
		this.toRegId = toRegId;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getReceivedYn() {
		return ReceivedYn;
	}

	public void setReceivedYn(String receivedYn) {
		ReceivedYn = receivedYn;
	}
	
	
	
	public double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Timestamp lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
	public void setDateFromTime() {
		this.date = new SimpleDateFormat("yyyyMMdd").format(getCreateTime());;
	}


	@Override
	public String toString() {
		return "Message [msgId=" + msgId + ", fromPhoneNum=" + fromPhoneNum + ", toPhoneNum=" + toPhoneNum
				+ ", fromRegId=" + fromRegId + ", toRegId=" + toRegId + ", msg=" + msg + ", ReceivedYn=" + ReceivedYn
				+ ", unitCost=" + unitCost + ", cost=" + cost + ", date=" + date + ", createTime=" + createTime
				+ ", lastUpdateTime=" + lastUpdateTime + "]";
	}

	@Override
	public String makePushMsg(String msgType, String regId) {
		// TODO Auto-generated method stub
		String result = "{ \"data\": {\n" +
						"	    \"msgType\": \"" + msgType  + "\",\n" + 
						"	    \"msgId\": \"" + this.getMsgId()  + "\",\n" +
						"	    \"fromPhoneNum\": \""+ this.getFromPhoneNum() +  "\",\n" +
						"	    \"toPhoneNum\": \""+ this.getToPhoneNum() +  "\",\n" +
						"	    \"msg\": \"" + this.getMsg()  + "\"\n" +
						"	  },\n" + 
					    "	  \"to\" : \""+ regId +"\"\n" + 
						"}";
		return result;
	}
   
	
}
