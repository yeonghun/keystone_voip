package hello.keystone.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/message")
public class MessageController {

	@Autowired
   	private MessageService msgService;

   	/*----------------------------------------------------------*/
   	/*---Request Message                                        */
   	/*----------------------------------------------------------*/
   	@PostMapping("/request")
   	public ResponseEntity<?> request(@RequestBody Message msg) {
   		System.out.println("--------------------------------------------------");
   		System.out.println("[/Message/Request/Input] : " + msg.toString());
   		Message returnMsg = msgService.request(msg);
   		if(returnMsg == null) {
   			return ResponseEntity.ok().body("Message Reciever's phonenumber does not exist");
   		}
   		System.out.println("[Response] :" + returnMsg.toString());
   		return ResponseEntity.ok().body(returnMsg);
   	}
   
   	/*----------------------------------------------------------*/
   	/*---Accept Message                                         */
   	/*----------------------------------------------------------*/
   	@PostMapping("/accept")
   	public ResponseEntity<?> accept(@RequestBody Message msg) {
   		System.out.println("--------------------------------------------------");
   		System.out.println("[/Message/accept/Input] : " + msg.toString());
   		Message returnMsg = msgService.accept(msg);
   		if(returnMsg == null) {
   			return ResponseEntity.ok().body("Message ID does not exist");
   		}
   		System.out.println("[Response] :" + returnMsg.toString());
   		return ResponseEntity.ok().body(returnMsg);
   	}
   
}