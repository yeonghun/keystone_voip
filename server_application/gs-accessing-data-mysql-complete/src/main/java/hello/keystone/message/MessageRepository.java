package hello.keystone.message;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface MessageRepository extends CrudRepository<Message, Long> {

	@Query(value = "select * from message a where a.date like ?1 and a.from_phone_num = ?2 and a.cost is not null   ", nativeQuery = true)
	List<Message> findMessageBillHistory(String month, String fromPhoneNum);
	
	
}
