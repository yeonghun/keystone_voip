package hello.keystone.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/user")
public class UserController {

   @Autowired
   private UserService userService;

   	/*----------------------------------------------------------*/
   	/*---Create User                                            */
   	/*----------------------------------------------------------*/
   	@PostMapping("/create")
   	public ResponseEntity<?> save(@RequestBody User user) {
   		User reUser = userService.save(user);
   		System.out.println("[Input User] :" + user.toString());
   		return ResponseEntity.ok().body(reUser);
   		//return ResponseEntity.ok().body("Hi kaosick ID:" + id);
   	}
   
   	/*----------------------------------------------------------*/
   	/*---Login User                                             */
   	/*----------------------------------------------------------*/
   	@PostMapping("/login")
   	public ResponseEntity<?> login(@RequestBody User user) {
      
   		User reUser = userService.login(user);
	   
   		if(reUser == null) {
   			System.out.println("[ Login Failed ]");
   			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Login Failed.");
   		}else {
   			System.out.println("[ Login Success ] " + user.getPhoneNum());
   			return ResponseEntity.ok().body(reUser);
   		}
   	}
   
   	/*----------------------------------------------------------*/
   	/*---Update User                                             */
   	/*----------------------------------------------------------*/
   	@PostMapping("/update")
   	public ResponseEntity<?> update(@RequestParam("phoneNum") String phoneNum, @RequestParam("password") String password, @RequestBody User user) {
   		System.out.println("[ /User/update ] : [" +phoneNum + " , " + password +"]" + user.toString());
   		if(!userService.verify(phoneNum, password)) {
   			System.out.println("[ Update Failed : ID/PW is wrong!]");
   			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ID/PW is wrong!");
   		}
   		User reUser = userService.update(user);
   		
   		System.out.println("[ Update Success ] " + user.getPhoneNum());
   		return ResponseEntity.ok().body(reUser);
   	}
   
   	/*----------------------------------------------------------*/
   	/*---Verify User                                             */
   	/*----------------------------------------------------------*/
   	@PostMapping("/verify")
   	public ResponseEntity<?> verify(@RequestBody User user) {
   		System.out.println("[ /User/verify ] : " + user.toString());
   		
   		User reUser = userService.verify(user);
	   
   		if(reUser == null) {
   			System.out.println("[ Verify Failed ]");
   			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Verify Failed!");
   		}else {
   			System.out.println("[ Verify Success ] " + user.getPhoneNum());
   			return ResponseEntity.ok().body("Verify Succeced!");
   		}
   	}
   
   	/*----------------------------------------------------------*/
   	/*---Recover User P/W                                             */
   	/*----------------------------------------------------------*/
   	@PostMapping("/recover")
   	public ResponseEntity<?> recover(@RequestBody User user) {
   		System.out.println("[ /User/recover ] : " + user.toString());
	   
   		User reUser = userService.recover(user);
	   
   		if(reUser == null) {
   			System.out.println("[ Recover Failed ]");
   			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recover Failed!");
   		}else {
   			System.out.println("[ Recover Success ] " + user.getPhoneNum());
   			return ResponseEntity.ok().body(reUser);
   		}
   	}

   /*@GetMapping("/book/{id}")
   public ResponseEntity<User> get(@PathVariable("id") long id) {
      User book = bookService.get(id);
      return ResponseEntity.ok().body(book);
   }

   @GetMapping("/book")
   public @ResponseBody Iterable<User> list() {
	   System.out.println("aaaaaaaaaaaaa");
      //Iterable<User> books = bookService.listAll();
      return null;
   }

   @PutMapping("/book/{id}")
   public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody User book) {
      bookService.update(id, book);
      return ResponseEntity.ok().body("Book has been updated successfully.");
   }

   @DeleteMapping("/book/{id}")
   public ResponseEntity<?> delete(@PathVariable("id") long id) {
	   userService.delete(id);
      return ResponseEntity.ok().body("Book has been deleted successfully.");
   }*/
}