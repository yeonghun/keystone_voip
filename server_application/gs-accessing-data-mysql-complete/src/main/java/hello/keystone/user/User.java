package hello.keystone.user;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import hello.keystone.push.Push;

@Entity(name = "User")
public class User extends Push {

	@Id
	@GenericGenerator(name = "sequence_phone_num", strategy = "hello.PhoneNumGenerator")
	@GeneratedValue(generator = "sequence_phone_num")  
   	//@GeneratedValue(strategy = GenerationType.IDENTITY)
   	private String phoneNum;
   	private String firstName;
   	private String lastName;
   	private String email;
   	private String password;
   	private String ip;
   	private String regId;
   	private boolean activeStatus = true;
   	
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public boolean isActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}
	@Override
	public String toString() {
		return "User [phoneNum=" + phoneNum + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", password=" + password + ", ip=" + ip + ", regId=" + regId + ", activeStatus=" + activeStatus + "]";
	}
}
