package hello.keystone.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, String> {

	
	@Query(value = "select * from user a where a.phone_num = ?1 and a.password = ?2 and a.active_status = 1 ", nativeQuery = true)
	User findUserWithActiveAndPassword(String phoneNum, String password);

	@Query(value = "select * from user a where a.phone_num = ?1 and a.active_status = 1 ", nativeQuery = true)
	User findUserWithActive(String phoneNum);

}
