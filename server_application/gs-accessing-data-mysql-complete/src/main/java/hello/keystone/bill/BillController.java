package hello.keystone.bill;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hello.keystone.call.Call;

@RestController
@RequestMapping(path="/bill")
public class BillController {

   @Autowired
   private BillService billService;

   /*----------------------------------------------------------*/
   /*--- Bill callhistory                                      */
   /*----------------------------------------------------------*/
   @PostMapping("/callhistory")
   public ResponseEntity<?> heartbeat(@RequestBody Bill bill) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Call/Callhistory/Input] : " + bill.toString());
	   List<Call> returnCall = billService.callhistory(bill);
	   if(returnCall == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("PhoneNum dose not exist");
	   }
	   System.out.println("[Response] :" + returnCall.toString());
	   return ResponseEntity.ok().body(returnCall);
   }
   
   /*----------------------------------------------------------*/
   /*--- Bill Summary                                          */
   /*----------------------------------------------------------*/
   @PostMapping("/summary")
   public ResponseEntity<?> summary(@RequestBody Bill bill) {
	   System.out.println("--------------------------------------------------");
	   System.out.println("[/Call/Summary/Input] : " + bill.toString());
	   List<BillSummary> billSummary = billService.summary(bill);
	   if(billSummary == null) {
		   return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("PhoneNum dose not exist");
	   }
	   System.out.println("[Response] :" + billSummary.toString());
	   return ResponseEntity.ok().body(billSummary);
   }
   
}