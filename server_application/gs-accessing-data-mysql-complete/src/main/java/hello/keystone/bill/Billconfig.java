package hello.keystone.bill;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="Billconfig")
public class Billconfig {

	@Id
	private Timestamp applyTime;
	private double callPrice;
	private double textPrice;
	private int useYn;
	public Timestamp getApplyTime() {
		return applyTime;
	}
	public void setApplyTime(Timestamp applyTime) {
		this.applyTime = applyTime;
	}
	public double getCallPrice() {
		return callPrice;
	}
	public void setCallPrice(double callPrice) {
		this.callPrice = callPrice;
	}
	public double getTextPrice() {
		return textPrice;
	}
	public void setTextPrice(double textPrice) {
		this.textPrice = textPrice;
	}
	public int getUseYn() {
		return useYn;
	}
	public void setUseYn(int useYn) {
		this.useYn = useYn;
	}
	@Override
	public String toString() {
		return "Billconfig [applyTime=" + applyTime + ", callPrice=" + callPrice + ", textPrice=" + textPrice
				+ ", useYn=" + useYn + "]";
	}
	
	
}
