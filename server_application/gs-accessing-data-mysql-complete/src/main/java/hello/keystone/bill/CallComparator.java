package hello.keystone.bill;

import java.util.Comparator;

import hello.keystone.call.Call;

public class CallComparator implements Comparator<Call> {

	@Override
	public int compare(Call first, Call second) {

		if(first.getCallId() > second.getCallId()) {
			return -1;
		}else if (first.getCallId() < second.getCallId()) {
			return 1;
		}else {
			return 0;
		}
	}
	
}
