package hello.keystone.bill;

public class BillSummary {
	private String month;
	private String category;
	private double cost = 0 ;
	private int duration = 0;
	
	public BillSummary(String month, String category) {
		super();
		this.month = month;
		this.category = category;
	}
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public void increaseCostAndDuration( double cost, int duration) {
		this.cost = this.cost + cost;
		this.duration = this.duration + duration;
	}

	@Override
	public String toString() {
		return "BillSummary [month=" + month + ", category=" + category + ", cost=" + cost + ", duration=" + duration
				+ "]";
	}
	
	
}

