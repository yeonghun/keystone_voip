package hello.keystone.bill;

import java.sql.Timestamp;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface BillConfigRepository extends CrudRepository<Billconfig, Timestamp> {

	@Query("select a from Billconfig a where use_yn = 1")
	Billconfig findCurrentBillConfig();
	
}
