package hello.keystone.bill;

public class Bill {

   	private String phoneNum;
   	private String month;
   	
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String date) {
		this.month = date;
	}
	@Override
	public String toString() {
		return "Bill [phoneNum=" + phoneNum + ", month=" + month + "]";
	}  
	
   	
   	
}
